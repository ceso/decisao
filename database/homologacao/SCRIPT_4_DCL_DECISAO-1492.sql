
GRANT SELECT ON judiciario.adendo_textual TO rs_decisao_gerente, rs_decisao_consulta;

GRANT SELECT ON judiciario.rotulo TO rs_decisao_gerente, rs_decisao_consulta;

GRANT SELECT ON judiciario.rotulo_objeto_incidente TO rs_decisao_gerente, rs_decisao_consulta;
