package br.jus.stf.decisao.scenarios;

// JUnit Assert framework can be used for verification

import org.openqa.selenium.WebDriver;

import org.springframework.beans.factory.annotation.Autowired;

import com.thoughtworks.twist.core.execution.TwistScenarioDataStore;

public class PerfisEAcessosAosTextos {

	private WebDriver browser;

	@Autowired
	private TwistScenarioDataStore scenarioStore;

	public PerfisEAcessosAosTextos(WebDriver browser) {
		this.browser = browser;
	}

	public void oPerfilMinistroDeveTerAcessoAQualquerTextoRestritoOuNãoEEmQualquerFaseQueEstejaSobSuaRelatoria()
			throws Exception {
	
	}

	public void oPerfilAdministradorDeveTerAcessoAQualquerTextoRestritoOuNãoEEmQualquerFaseQueEstejaSobSuaRelatoria()
			throws Exception {
	
	}

	public void nãoDeveSerPossívelOAcessoATextosRestritosDeOutrasRelatoriasIndependenteDoPerfil()
			throws Exception {
	
	}

}
