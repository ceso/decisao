package br.jus.stf.estf.decisao;

public interface DecisaoActions {
	public static final String ACAO_ABRIR_DOCUMENTO = "abrirDocumento";
	public static final String ACAO_NOVO_DOCUMENTO = "novoDocumento";
	public static final String ACAO_SALVAR_DOCUMENTO = "salvarDocumento";
	public static final String ACAO_GERAR_PDF = "gerarPDF";
	public static final String ACAO_FECHAR_DOCUMENTO = "fecharDocumento";
	public static final String ACAO_RECUPERAR_VERSOES_DOCUMENTO = "recuperarVersoesDocumento";
	public static final String ACAO_MANTER_SESSAO_USUARIO = "manterSessaoUsuario";
}
