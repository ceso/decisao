package br.jus.stf.estf.decisao.uri;

import java.net.URI;

public interface URIBuilder {
	URI buildURI() throws URIEncodingException;
}
