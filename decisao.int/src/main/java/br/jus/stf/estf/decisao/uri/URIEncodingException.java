package br.jus.stf.estf.decisao.uri;

public class URIEncodingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 207639728299046416L;

	public URIEncodingException() {
		// TODO Auto-generated constructor stub
	}

	public URIEncodingException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public URIEncodingException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public URIEncodingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
