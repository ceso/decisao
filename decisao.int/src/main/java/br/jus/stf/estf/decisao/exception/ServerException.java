package br.jus.stf.estf.decisao.exception;

public class ServerException extends Exception {

	public ServerException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ServerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ServerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ServerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2971206335564402103L;

}
