package br.jus.stf.estf.decisao.handlers;

import org.apache.log4j.Logger;

import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.comp.beans.NoConnectionException;
import com.sun.star.comp.beans.OfficeDocument;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.text.XText;
import com.sun.star.text.XTextDocument;
import com.sun.star.uno.UnoRuntime;

/**
 * Classe que permite que uma determinada a��o seja realizada para cada par�grafo desbloqueado do texto.
 * @author Demetrius.Jube
 *
 */
abstract class ParagraphScanner extends Scanner {
	
	private static final Logger logger = Logger.getLogger(ParagraphScanner.class);

	public ParagraphScanner(OfficeDocument document) {
		super(document);
	}

	/**
	 * Recupera um objeto que implemente a interface XServiceInfo, que indica quais servi�os s�o suportados
	 * por aquele objeto
	 * @param objeto
	 * @return
	 */
	private XServiceInfo recuperaServiceInfo(Object objeto) {
		return (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, objeto);
	}

	/**
	 * Recupera os elementos que representam os par�grafos do texto.
	 * @return
	 * @throws NoConnectionException
	 */
	private XEnumeration recuperaParagrafosDoTexto() throws NoConnectionException {
		XTextDocument xTextDocument = (XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, document);
		XText xText = xTextDocument.getText();

		// Recupera a enumera��o de par�grafos do texto
		XEnumerationAccess xParaAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class,
				xText);
		XEnumeration xParaEnum = xParaAccess.createEnumeration();
		return xParaEnum;
	}
	
	/**
	 * Verifica se o objeto � realmente um par�grafo. Para isso, testa se o XServiceInfo
	 * suporta o servi�o com.sun.star.text.TextTable. Caso n�o seja, segundo a API, � seguro
	 * assumir que � um par�grafo. 
	 * @param xInfo
	 * @return
	 */
	private boolean isParagrafo(XServiceInfo xInfo) {
		return !xInfo.supportsService("com.sun.star.text.TextTable");
	}

	public void scan() throws ValidacaoEstiloException {
		try {

			XEnumeration xParaEnum = recuperaParagrafosDoTexto();
			// Varre os par�grafos
			while (xParaEnum.hasMoreElements()) {
				XServiceInfo xInfo = recuperaServiceInfo(xParaEnum.nextElement());
				if (isParagrafo(xInfo)) {
					// S� faz alguma altera��o caso a se��o n�o seja
					// protegida.
					// Isso evita que os dados do cabe�alho do template
					// sejam
					// alterados.
					if (!isSecaoProtegida(xInfo)) {
						executeAction(xInfo);
					}
				}
			}
		} catch (NoConnectionException e) {
			logger.error(e.getMessage(), e);
			throw new ValidacaoEstiloException(e);
		} catch (NoSuchElementException e) {
			logger.error(e.getMessage(), e);
			throw new ValidacaoEstiloException(e);
		} catch (WrappedTargetException e) {
			logger.error(e.getMessage(), e);
			throw new ValidacaoEstiloException(e);
		} catch (UnknownPropertyException e) {
			logger.error(e.getMessage(), e);
			throw new ValidacaoEstiloException(e);
		}
	}

	public abstract void executeAction(XServiceInfo xInfo) throws ValidacaoEstiloException;
}
