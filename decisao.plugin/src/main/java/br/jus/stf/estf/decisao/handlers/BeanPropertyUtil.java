package br.jus.stf.estf.decisao.handlers;

import org.apache.log4j.Logger;

import br.jus.stf.stfoffice.client.uno.*;

import com.sun.star.beans.*;

public class BeanPropertyUtil {

	private static final Logger logger = Logger.getLogger(BeanPropertyUtil.class);
	
	/**
	 * Verifica se a propriedade apresentada est� no seu valor default
	 * @param propriedade O nome da propriedade
	 * @param estadosDasPropriedades O objeto que cont�m os estados das propriedades
	 * @param pespectivaDaPropriedade indica se a propriedade � referente a um par�grafo ou a uma palavra.
	 * @param changeToDefault Indica se o m�todo deve modificar o estado para o valor default ou apenas lan�ar a exce��o.
	 * @throws EstiloDespadronizadoException
	 * @throws BeanUnoWrapperException
	 */
	public static void verificaPropriedadeComValorDefault(String propriedade, XPropertyState estadosDasPropriedades,
			int pespectivaDaPropriedade, boolean changeToDefault) throws EstiloDespadronizadoException, ValidacaoEstiloException {
		try {
			PropertyState propertyState = estadosDasPropriedades.getPropertyState(propriedade);
			if ( pespectivaDaPropriedade != propertyState.getValue() ) {
				if (changeToDefault) {
					modificaPropriedadeParaValorDefault(propriedade, estadosDasPropriedades);
				} else {
					throw new EstiloDespadronizadoException("A propriedade " + propriedade + " est� despadronizada!");
				}
			}
		} catch (UnknownPropertyException e) {
			throw new ValidacaoEstiloException(e);
		}
	}

	private static void modificaPropriedadeParaValorDefault(String propriedade, XPropertyState estadosDasPropriedades)
			throws ValidacaoEstiloException {
		try {
			estadosDasPropriedades.setPropertyToDefault(propriedade);
		} catch (UnknownPropertyException e) {
			throw new ValidacaoEstiloException(e);
		}
	}

}
