package br.jus.stf.estf.decisao.handlers;

 class EstiloDespadronizadoException extends ValidacaoEstiloException {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3977753969416175820L;

		public EstiloDespadronizadoException(String message) {
			super(message);
		}

	}