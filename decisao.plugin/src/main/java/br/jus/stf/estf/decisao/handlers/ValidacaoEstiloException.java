package br.jus.stf.estf.decisao.handlers;

public class ValidacaoEstiloException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValidacaoEstiloException() {
	}

	public ValidacaoEstiloException(String message) {
		super(message);
	}

	public ValidacaoEstiloException(Throwable cause) {
		super(cause);
	}

	public ValidacaoEstiloException(String message, Throwable cause) {
		super(message, cause);
	}

}
