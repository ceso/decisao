package br.jus.stf.estf.decisao.mocker;

public interface ExecucaoMock<T> {

	void executar(T p);

}
