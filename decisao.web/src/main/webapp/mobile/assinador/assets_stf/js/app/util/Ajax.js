'use strict';

StfDecisao.util('AjaxUtil', function(win, nav) {
	var self = this;
	
	var estaOffline = function(xhr) {
		return !nav.onLine;
	};
	
	this.ajax = function(config) {
		var deferred = $.Deferred();
		$.ajax(config).done(function(data) {
			deferred.resolve(data);
		}).fail(function(xhr) {
			if (estaOffline(xhr)) {
				deferred.reject({'errors': ['Erro ao conectar � Internet. Tente novamente mais tarde.'], 'erroImpeditivo': true, 'erroFatal': true});
			} else {
				try {
					var data = $.parseJSON(xhr.responseText);
					deferred.reject(data);
				} catch (err) {
					if (xhr.responseText.indexOf('action="/cas/login?service=') > -1) {
						console.log('redirect do cas');
						win.location = 'index.jsp';
					} else {
						console.log('n�o � redirect do cas');
					}
				}
			}
		});
		return deferred.promise();
	};
	
});