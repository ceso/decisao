'use strict';

/**
 * Servi�o utilit�rio para recuperar informa��es relativas ao browser.
 * 
 * @param win Objeto window do browser
 */
StfDecisao.util('BrowserUtil', function(win) {
	var self = this;
	/**
	 * Recupera a Url base do servidor onde est� rodando o sistema.
	 * 
	 */
	this.getUrlBase = function() {
		return win.document.location.origin;
	};

	/**
	 * Verifica se o sistema est� rodando em ambiente mobile. Sup�e-se que o ambiente mobile ocorrer� sempre que est� rodando em um iframe.
	 * 
	 */
	this.isMobile = function() {
		return win.location != win.parent.location;
	};

	/**
	 * Recupera a Url base do servidor que ser� utilizado para autenticar no sistema.
	 */
	this.getUrlBaseServidorAutenticacao = function() {
		var urlBase = self.getUrlBase();
		if (urlBase.indexOf("localhost") > -1) {
			return "https://sistemast.stf.jus.br";
		} else {
			return urlBase;
		}
	};

	this.hello = function() {
		return "hello";
	};
});