'use strict';

StfDecisao.service('CertificadoService', function(mni) {
	var self = this;
	
	this.getCertificado = function() {
		return mni.recuperarDadosCertificado();
	};
	
	this.excluirCertificado = function() {
		return mni.excluirCertificado();
	};
	
});