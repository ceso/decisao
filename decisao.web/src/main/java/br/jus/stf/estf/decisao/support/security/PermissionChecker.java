package br.jus.stf.estf.decisao.support.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.GrantedAuthority;
import org.springframework.stereotype.Component;

import br.gov.stf.estf.entidade.usuario.Perfil;
import br.gov.stf.estf.entidade.usuario.Transacao;
import br.gov.stf.estf.usuario.model.service.TransacaoService;
import br.gov.stf.framework.model.service.ServiceException;
import br.jus.stf.estf.decisao.support.action.support.ActionIdentification;
import br.jus.stf.estf.decisao.support.util.NestedRuntimeException;
/**
 * Classe que permite checar se um determinado usu�rio tem permiss�o para acessar uma a��o.
 * @author Demetrius.Jube
 *
 */
@Component("permissionChecker")
public class PermissionChecker {

	public static final String SIGLA_SISTEMA_ESTF_DECISAO = "ESTFDECISAO";
	public static final String PERFIL_PREFIXO = "RS_ESTFDECISAO-";
	
	private Map<Integer, List<String>> roles = new HashMap<Integer, List<String>>();

	@Autowired
	private TransacaoService transacaoService;
	
	/**
	 * Verifica se uma autentica��o possui permiss�o para acessar uma determinada a��o.
	 * @param principal
	 * @param actionIdentification
	 * @return
	 */
	public boolean hasPermission(Principal principal, ActionIdentification actionIdentification) {
		// Utiliza as permiss�es do Principal ao inv�s das permiss�es do Authentication, pois 
		// � realizado um filtro dos perfis no momento da autentica��o (vide AuthenticationProvider)
    	List<GrantedAuthority> authorities = principal.getAuthorities();
    	
    	// Se o usu�rio possui algum perfil necess�rio � a��o, liberar! Caso contr�rio, negar.
    	for (GrantedAuthority authority : authorities) {
    		// Recuperando role, descartando prefixo...
    		String role = authority.getAuthority().substring(PERFIL_PREFIXO.length());
    		if (getRoles(actionIdentification.getCodigo()).contains(role)) {
				return true;
			}
		}
		return false;
    }
    
    /**
     * Recupera os perfis necess�rios a execu��o de uma transa��o, que para o caso em quest�o �
     * o mesmo que uma a��o.
     * 
     * <p>Os perfis de cada transa��o (a��o) ser� armazenado em um mapa de perfils (a��o:perfis).
     * 
     * @param actionNumber o identificador da transa��o
     * @return a lista de perfis
     */
    private List<String> getRoles(Integer actionNumber) {
    	if (!roles.containsKey(actionNumber)) {
    		try {
		    	Transacao transacao = transacaoService.pesquisarTransacao(SIGLA_SISTEMA_ESTF_DECISAO, actionNumber);
		    	List<String> perfis = new ArrayList<String>();
		    	if (transacao != null && transacao.getPerfis() != null) {
			    	for (Perfil perfil : transacao.getPerfis()) {
			    		perfis.add(perfil.getDescricao());
					}
		    	}
		    	roles.put(actionNumber, perfis);
			} catch (ServiceException e) {
				throw new NestedRuntimeException(e);
			}
    	}
    	return roles.get(actionNumber);
    }

}
