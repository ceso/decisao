package br.jus.stf.estf.decisao.support.action.handlers;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indica que a a��o s� deve ser apresentada se o 
 * texto pertencer � lista de textos iguais.
 * 
 * @author Rodrigo Barreiros
 * @see 22.07.2010
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CheckTextosIguais {
	
}
