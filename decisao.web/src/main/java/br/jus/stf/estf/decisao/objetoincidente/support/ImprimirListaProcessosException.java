package br.jus.stf.estf.decisao.objetoincidente.support;

public class ImprimirListaProcessosException extends Exception {

	public ImprimirListaProcessosException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ImprimirListaProcessosException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ImprimirListaProcessosException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ImprimirListaProcessosException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
