package br.jus.stf.estf.decisao.documento.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CRL;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;
import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.lowagie.text.DocumentException;

import br.gov.stf.eprocesso.servidorpdf.servico.modelo.ExtensaoEnum;
import br.gov.stf.estf.documento.model.service.impl.AssinaturaDigitalServiceImpl;
import br.gov.stf.estf.entidade.documento.DocumentoComunicacao;
import br.gov.stf.estf.entidade.documento.DocumentoEletronico;
import br.gov.stf.estf.entidade.documento.FlagTipoAssinatura;
import br.gov.stf.estf.entidade.documento.Texto;
import br.gov.stf.estf.entidade.documento.TipoDocumentoTexto;
import br.gov.stf.estf.entidade.documento.TipoTexto;
import br.gov.stf.framework.model.service.ServiceException;
import br.jus.stf.assinadorweb.api.exception.CarimbadorException;
import br.jus.stf.assinadorweb.api.negocio.ResultadoAssinatura;
import br.jus.stf.assinadorweb.api.negocio.ResultadoAssinaturaImpl;
import br.jus.stf.assinadorweb.api.resposta.exception.RespostaException;
import br.jus.stf.estf.decisao.comunicacao.support.AssinarComunicacaoHandler;
import br.jus.stf.estf.decisao.comunicacao.support.ComunicacaoWrapper;
import br.jus.stf.estf.decisao.documento.support.assinador.AssinadorPorPartes;
import br.jus.stf.estf.decisao.documento.support.assinador.ContextoAssinatura;
import br.jus.stf.estf.decisao.documento.support.assinador.DocumentoParaAssinaturaAdapter;
import br.jus.stf.estf.decisao.documento.support.assinador.RefAssinaturaExterna;
import br.jus.stf.estf.decisao.documento.support.assinador.ValidadorCertificado;
import br.jus.stf.estf.decisao.documento.support.assinador.algs.SHA256DetachedAssinadorPorPartes;
import br.jus.stf.estf.decisao.documento.support.assinador.exception.AssinaturaExternaException;
import br.jus.stf.estf.decisao.documento.support.assinador.exception.ValidacaoCertificadoException;
import br.jus.stf.estf.decisao.pesquisa.domain.ComunicacaoDto;
import br.jus.stf.estf.decisao.pesquisa.domain.TextoDto;
import br.jus.stf.estf.decisao.support.service.ConfiguracaoSistemaService;
import br.jus.stf.estf.decisao.texto.support.AssinarTextoHandler;
import br.jus.stf.estf.decisao.texto.support.DadosMontagemTextoBuilder;
import br.jus.stf.estf.decisao.texto.support.TextoWrapper;
import br.jus.stf.estf.montadortexto.ByteArrayOutputStrategy;
import br.jus.stf.estf.montadortexto.DadosMontagemTexto;
import br.jus.stf.estf.montadortexto.MontadorTextoServiceException;
import br.jus.stf.estf.montadortexto.OpenOfficeMontadorTextoService;
import br.jus.stf.estf.montadortexto.TextoOutputException;
import br.jus.stf.estf.montadortexto.TextoOutputStrategy;
import br.jus.stf.estf.montadortexto.TextoSource;
import br.jus.stf.estf.montadortexto.tools.ByteArrayPersister;
import br.jus.stf.estf.montadortexto.tools.PDFUtil;

@Service("assinaturaExternaDocumentoService")
public class AssinaturaExternaDocumentoServiceImpl extends AbstractAssinaturaDocumentoService {

	private static final String SEQUENCIAL_DOCUMENTO = "SEQUENCIAL_DOCUMENTO_ELETRONICO";
	private static final String HASH_VALIDACAO = "HASH_VALIDACAO";
	protected Log log = Logging.getLog(getClass());

	@Autowired
	private OpenOfficeMontadorTextoService openOfficeMontadorTextoService;

	@Autowired
	private DadosMontagemTextoBuilder dadosMontagemTextoBuilder;

	@Autowired
	private ApplicationContext appContext;
	
	@Autowired	
	protected ConfiguracaoSistemaService configuracaoSistemaService;
	
	@Override
	public void assinarTextosAutomaticamente(List<TextoDto> textos) throws ServiceException {
		throw new RuntimeException("Opera��o n�o permitida!");
	}

	@Override
	public void assinarComunicacoesAutomaticamente(List<ComunicacaoDto> comunicacoes) throws ServiceException {
		throw new RuntimeException("Opera��o n�o permitida!");
	}

	@Override
	public void assinarDocumentosAutomaticamente(List<TextoDto> textos, List<ComunicacaoDto> comunicacoes) throws ServiceException {
		throw new RuntimeException("Opera��o n�o permitida!");
	}

	public RefAssinaturaExterna preAssinarTexto(TextoDto texto, String[] cadeia) throws ServiceException {
		Long sequencialDoDocumento = textoService.recuperarSequencialDoDocumentoEletronico(texto);
		String hashValidacao = AssinaturaDigitalServiceImpl.gerarHashValidacao();
		try {
			byte[] pdf = AssinaturaDigitalServiceImpl.adicionarRodapePdf(recuperarPDF(texto), AssinaturaDigitalServiceImpl.getRodapeAssinaturaDigital(hashValidacao));
			return preCalcularHashPdfParaAssinatura(DocumentoParaAssinaturaAdapter.adapt(texto), sequencialDoDocumento, hashValidacao, pdf, cadeia);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(texto.toString() + " - Erro ao pr� assinar digitalmente o texto. " + e.getMessage());
		}
	}

	private RefAssinaturaExterna preCalcularHashPdfParaAssinatura(DocumentoParaAssinaturaAdapter adapter, Long idDocEletronico, String hashValidacao, byte[] pdf, String[] cadeiaStr)
			throws ServiceException {
		try {
			ValidadorCertificado vc = ValidadorCertificado.build(cadeiaStr);
			vc.validarCertificado();

			X509Certificate[] cadeia = (X509Certificate[]) vc.getCadeiaCompleta().toArray(new X509Certificate[0]);
			CRL[] crls = vc.getCrl();
			AssinadorPorPartes app = getAssinadorPorPartes();
			String adaptedId = adapter.getAdaptedId();
			ContextoAssinatura ca = new ContextoAssinatura(adaptedId);
			String montaMensagemDoRodape = AssinaturaDigitalServiceImpl.getRodapeAssinaturaDigital(hashValidacao);
			byte[] dadosParaAssinar = app.preAssinar(cadeia, crls, pdf, montaMensagemDoRodape, ca);
			byte[] hashParaAssinar = app.prepararHashParaAssinaturaExterna(dadosParaAssinar);

			String id = salvarObjetosParaPosAssinatura(ca);
			getSession().setAttribute(SEQUENCIAL_DOCUMENTO, idDocEletronico);
			getSession().setAttribute(HASH_VALIDACAO, hashValidacao);
			return new RefAssinaturaExterna(id, new String(Base64.encodeBase64(hashParaAssinar)));
			
		} catch (ValidacaoCertificadoException e) {
			throw new ServiceException("Erro ao validar assinador.", e);
		} catch (AssinaturaExternaException e) {
			throw new ServiceException("Erro ao pr� assinar pdf.", e);
		}
	}

	public void posAssinarTexto(TextoDto texto, String idContexto, String assinatura) throws ServiceException {
		try {
			DocumentoParaAssinaturaAdapter adapt = DocumentoParaAssinaturaAdapter.adapt(texto);
			ResultadoAssinatura ra = posAssinarDocumento(adapt, idContexto, assinatura);
			AssinarTextoHandler ath = new AssinarTextoHandler();
			TextoWrapper montaWrapperTexto = montaWrapperTexto(texto);
			ath.salvarDocumentoPdf(montaWrapperTexto, ra);
		} catch (RespostaException e) {
			throw new ServiceException("Erro ao salvar o documento assinado.");
		}
	}

	public ResultadoAssinatura posAssinarDocumento(DocumentoParaAssinaturaAdapter documento, String idContexto, String assinatura) throws ServiceException {
		try {
			AssinadorPorPartes app = getAssinadorPorPartes();
			ContextoAssinatura ca = recuperarObjetosParaPosAssinatura(documento, idContexto);
			
			byte[] pdfAssinado = app.posAssinar(ca, assinatura);
			byte[] assinaturaPdf = app.extrairAssinatura(pdfAssinado);

			byte[] carimbo = app.carimbar(assinaturaPdf);
			String subjectDN = ca.getPdfPKCS7().getSigningCertificate().getSubjectDN().getName();
			
			ResultadoAssinatura ra = new ResultadoAssinaturaImpl(pdfAssinado, assinaturaPdf, carimbo, subjectDN);
			
			log.info("Resultado da assinatura: pdfAssinado=" + pdfAssinado.length + " bytes, assinatura=" + assinaturaPdf.length + " bytes, carimbo="+ carimbo.length + " bytes");
			return ra;
		} catch (CarimbadorException e) {
			throw new ServiceException("Erro ao recuperar dados de carimbo de tempo.", e);
		} catch (AssinaturaExternaException e) {
			throw new ServiceException("Erro ao p�s assinar pdf.", e);
		}
	}

	private AssinadorPorPartes getAssinadorPorPartes() {
		// Passando false. Por enquanto n�o vamos adicionar o timestamp no pdf.
		// Melhor aguardar para faz�-lo tamb�m no Assinador Desktop.
		return new SHA256DetachedAssinadorPorPartes(false);
	}
	
	private TextoWrapper montaWrapperTexto(TextoDto texto) {
		TipoDocumentoTexto tipoDocumentoTexto = null;
		tipoDocumentoTexto = textoService.recuperarTipoDocumentoTextoPorId(TipoDocumentoTexto.COD_TIPO_DOCUMENTO_TEXTO_PADRAO);
		Long sequencialDoDocumento;
		String hashValidacao;
		
		if (getSession().getAttribute(SEQUENCIAL_DOCUMENTO) == null) {
			sequencialDoDocumento = textoService.recuperarSequencialDoDocumentoEletronico(texto);
			hashValidacao = AssinaturaDigitalServiceImpl.gerarHashValidacao();
		} else {
			sequencialDoDocumento = (Long) getSession().getAttribute(SEQUENCIAL_DOCUMENTO);
			hashValidacao = (String) getSession().getAttribute(HASH_VALIDACAO);
		}
		 
		TextoWrapper textoWrapper = new TextoWrapper(appContext, tipoDocumentoTexto, texto, getInserirTimbre(), sequencialDoDocumento, hashValidacao, getUsuario().getId(),
				getObservacao(), FlagTipoAssinatura.MOVEL);
		return textoWrapper;
	}

	private String salvarObjetosParaPosAssinatura(ContextoAssinatura ca) {
		getSession().setAttribute(ca.getIdContexto(), ca);
		return ca.getIdContexto();
	}

	private ContextoAssinatura recuperarObjetosParaPosAssinatura(DocumentoParaAssinaturaAdapter documento, String id) throws ServiceException {
		ContextoAssinatura ca = (ContextoAssinatura) getSession().getAttribute(id);
		if (ca != null) {
			getSession().removeAttribute(id);
			if (!ca.getIdDocumento().equals(documento.getAdaptedId())) {
				throw new ServiceException("Texto inv�lido para o Contexto de Assinatura.");
			}
			return ca;
		} else {
			throw new ServiceException("Contexto de Assinatura n�o encontrado na sess�o.");
		}
	}

	private byte[] recuperarPDF(TextoDto textoDto) {
		long start = System.currentTimeMillis();
		try {
			Texto texto = textoService.recuperarTextoPorId(textoDto.getId());
			DadosMontagemTexto<Long> dadosMontagem;
			if (texto.getTipoTexto().equals(TipoTexto.ACORDAO)) {
				dadosMontagem = dadosMontagemTextoBuilder.montaDadosMontagemTexto(texto, false, montaArquivoDeEmentaAcordao(texto));

			} else if (texto.getTipoTexto().equals(TipoTexto.DECISAO_SOBRE_REPERCURSAO_GERAL)) {
				dadosMontagem = dadosMontagemTextoBuilder.montaDadosMontagemTexto(texto, false, montaArquivoDeEmentaDecisaoSobreRepercussaoGeral(texto));

			} else {
				dadosMontagem = dadosMontagemTextoBuilder.montaDadosMontagemTexto(texto, false);
			}

			// Preparando par�metros para gera��o do PDF...
			final ByteArrayOutputStream conteudo = new ByteArrayOutputStream();
			TextoOutputStrategy<Long> outputStrategy = new ByteArrayOutputStrategy<Long>(new ByteArrayPersister<Long>() {
				public void persistByteArray(Long textoId, byte[] data) throws TextoOutputException, IOException {
					conteudo.write(data);
				}
			});
			// Executando rotina de gera��o de PDF...
			long startConversao = System.currentTimeMillis();
			logger.warn("Tempo para recuperacao dos dados: [" + (startConversao - start) + "] milisegundos.");
			openOfficeMontadorTextoService.criarTextoPDF(dadosMontagem, outputStrategy, true);
			long endConversao = System.currentTimeMillis();
			logger.warn("Tempo para conversao: [" + (endConversao - startConversao) + "] milisegundos.");

			byte[] pdfComAutor = PDFUtil.getInstancia().inserirAutor(new ByteArrayInputStream(conteudo.toByteArray()),
					getMinistroGabineteUsuarioLogado().getNome());

			if (getInserirTimbre()) {
				ByteArrayInputStream conteudoStream = new ByteArrayInputStream(pdfComAutor);
				return PDFUtil.getInstancia().inserirCabecalhoArquivoPDF(conteudoStream);
			}
			return pdfComAutor;
		} catch (MontadorTextoServiceException e) {
			logger.error("Erro ao montar o PDF para assinatura", e);
			throw new RuntimeException(e);
		} catch (ServiceException e) {
			logger.error("Erro ao montar o PDF para assinatura", e);
			throw new RuntimeException(e);
		} catch (IOException e) {
			logger.error("Erro ao montar o PDF para assinatura", e);
			throw new RuntimeException(e);
		} catch (DocumentException e) {
			logger.error("Erro ao montar o PDF para assinatura", e);
			throw new RuntimeException(e);
		} catch (JDOMException e) {
			logger.error("Erro ao montar o PDF para assinatura", e);
			throw new RuntimeException(e);
		} finally {
			long end = System.currentTimeMillis();
			logger.info("Tempo total para geracao do PDF: [" + (end - start) + "] milisegundos.");
		}
	}

	private byte[] montaArquivoDeEmentaAcordao(Texto texto) throws ServiceException, FileNotFoundException, JDOMException, IOException,
			MontadorTextoServiceException {
		Texto acordao = texto;
		Texto ementa = textoService.recuperarEmenta(acordao, acordao.getMinistro());
		return concatenarArquivos(ementa, acordao);
	}

	private byte[] montaArquivoDeEmentaDecisaoSobreRepercussaoGeral(Texto texto) throws ServiceException, FileNotFoundException, JDOMException, IOException,
			MontadorTextoServiceException {
		Texto decisao = texto;
		Texto ementa = textoService.recuperarEmentaRepercussaoGeral(decisao, decisao.getMinistro());
		return concatenarArquivos(ementa, decisao);
	}

	/**
	 * Concatena dois arquivos RTF, retornando um �nico arquivo ODT. Permite adicionar
	 * uma quebra de p�gina entre os dois arquivos.
	 * 
	 * @param ementa
	 *            o primeiro texto a ser concatenado
	 * @param acordao
	 *            o segundo texto a ser concatenado
	 * 
	 * @return o array de bytes do arquvios concatenado
	 */
	private byte[] concatenarArquivos(Texto ementa, Texto acordao) throws JDOMException, IOException, MontadorTextoServiceException, FileNotFoundException {
		File ementaAsOdt = converterArquivoParaOdt(ementa);
		File acordaoAsOdt = converterArquivoParaOdt(acordao);

		File resultado = openOfficeMontadorTextoService.concatenaArquivosOdt(ementaAsOdt, acordaoAsOdt, false);

		return IOUtils.toByteArray(new FileInputStream(resultado));
	}

	/**
	 * Converte o conte�do (RTF) de um dado texto em um arquivo(File) ODT.
	 * 
	 * @param texto
	 *            o texto de entrada
	 * 
	 * @return o arquivo ODT
	 */
	private File converterArquivoParaOdt(Texto texto) throws MontadorTextoServiceException, IOException, FileNotFoundException {
		InputStream odtAsInputStream = openOfficeMontadorTextoService.converteArquivo(getTextoSource(texto), ExtensaoEnum.RTF, ExtensaoEnum.ODT);
		File odtAsFile = File.createTempFile(texto.getIdentificacao(), ".odt");
		FileOutputStream fos = new FileOutputStream(odtAsFile);
		IOUtils.copy(odtAsInputStream, fos);
		return odtAsFile;
	}

	/**
	 * Retorna o <code>TextoSource</code> para o conte�do de um dado texto.
	 * 
	 * @param texto	
	 *            o texto de entrada
	 * 
	 * @return o <code>TextoSource</code>
	 */
	private TextoSource getTextoSource(final Texto texto) {
		return new TextoSource() {
			@Override
			public byte[] getByteArray() throws IOException, MontadorTextoServiceException {
				return texto.getArquivoEletronico().getConteudo();
			}
		};
	}

	private boolean getInserirTimbre() {
		try {
			return configuracaoSistemaService.isGerarTimbreAssinatura();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			return false;
		}		
	}

	private String getObservacao() {
		return ""; // Por padr�o, � vazia
	}

	public RefAssinaturaExterna preAssinarComunicacao(ComunicacaoDto comunicacao, String[] cadeia) throws ServiceException {
		Long idDocumentoComunicacao = comunicacao.getIdDocumentoComunicacao();
		Long sequencialDoDocumento = documentoComunicacaoService.recuperarSequencialDoDocumentoEletronico(idDocumentoComunicacao);
		String hashValidacao = AssinaturaDigitalServiceImpl.gerarHashValidacao();
		
		try {
			byte[] recuperarPDF = recuperarPDF(comunicacao);
			String montaMensagemDoRodape = AssinaturaDigitalServiceImpl.getRodapeAssinaturaDigital(hashValidacao);
			byte[] pdf = AssinaturaDigitalServiceImpl.adicionarRodapePdf(recuperarPDF, montaMensagemDoRodape);
			DocumentoParaAssinaturaAdapter adapt = DocumentoParaAssinaturaAdapter.adapt(comunicacao);
			RefAssinaturaExterna preCalcularHashPdfParaAssinatura = preCalcularHashPdfParaAssinatura(adapt, sequencialDoDocumento, hashValidacao, pdf, cadeia);
			return preCalcularHashPdfParaAssinatura;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(comunicacao.toString() + " - Erro ao pr� assinar digitalmente a comunica��o. " + e.getMessage());
		}
	}

	public byte[] recuperarPDF(ComunicacaoDto comunicacao) throws ServiceException {
		long start = System.currentTimeMillis();
		try {
			Long idDocumentoComunicacao = comunicacao.getIdDocumentoComunicacao();
			DocumentoComunicacao documentoComunicacao = documentoComunicacaoService.recuperarPorId(idDocumentoComunicacao);
			byte[] arquivo = documentoComunicacao.getDocumentoEletronico().getArquivo();
			return arquivo;
		} catch (ServiceException e) {
			logger.error("Erro ao recuperar o PDF para assinatura", e);
			throw new ServiceException(e);
		} finally {
			long end = System.currentTimeMillis();
			logger.info("Tempo total para recupera��o do PDF: [" + (end - start) + "] milisegundos.");
		}
	}

	public void posAssinarComunicacao(ComunicacaoDto comunicacao, String idContexto, String assinatura) throws ServiceException {
		try {
			ResultadoAssinatura ra = posAssinarDocumento(DocumentoParaAssinaturaAdapter.adapt(comunicacao), idContexto, assinatura);

			AssinarComunicacaoHandler ach = new AssinarComunicacaoHandler();
			ach.salvarDocumentoPdf(montaWrapperComunicacao(comunicacao), ra);
		} catch (RespostaException e) {
			throw new ServiceException("Erro ao salvar o documento assinado.");
		}
	}

	private ComunicacaoWrapper montaWrapperComunicacao(ComunicacaoDto comunicacao) {
		return new ComunicacaoWrapper(appContext, comunicacao, getUsuario().getId(), FlagTipoAssinatura.MOVEL, AssinaturaDigitalServiceImpl.gerarHashValidacao());
	}

	
	private HttpSession getSession() {
		ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		return sra.getRequest().getSession();
	}
	
}
