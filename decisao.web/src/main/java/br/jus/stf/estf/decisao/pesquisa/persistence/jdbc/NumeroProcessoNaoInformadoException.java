package br.jus.stf.estf.decisao.pesquisa.persistence.jdbc;

public class NumeroProcessoNaoInformadoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6347557254455460239L;

	public NumeroProcessoNaoInformadoException() {
		super();
	}

	public NumeroProcessoNaoInformadoException(String message, Throwable cause) {
		super(message, cause);
	}

	public NumeroProcessoNaoInformadoException(String message) {
		super(message);
	}

	public NumeroProcessoNaoInformadoException(Throwable cause) {
		super(cause);
	}

}
