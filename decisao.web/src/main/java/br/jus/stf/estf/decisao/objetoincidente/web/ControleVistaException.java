package br.jus.stf.estf.decisao.objetoincidente.web;

public class ControleVistaException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ControleVistaException(String message) {
		super(message);
	}


}
