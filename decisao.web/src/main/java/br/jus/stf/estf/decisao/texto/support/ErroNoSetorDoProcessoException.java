package br.jus.stf.estf.decisao.texto.support;

public class ErroNoSetorDoProcessoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4430634118967222215L;

	public ErroNoSetorDoProcessoException() {
	}

	public ErroNoSetorDoProcessoException(String message) {
		super(message);
	}

	public ErroNoSetorDoProcessoException(Throwable cause) {
		super(cause);
	}

	public ErroNoSetorDoProcessoException(String message,
			Throwable cause) {
		super(message, cause);
	}

}
