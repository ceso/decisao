package br.jus.stf.estf.decisao.objetoincidente.support;

import java.util.Collection;

import br.gov.stf.estf.processostf.model.util.IConsultaDeAgendamento;

//import br.gov.stf.estf.processostf.model.util.IConsultaDeAgendamentoComTipoJulgamento;

public class ConsultaDeAgendamentoVO implements IConsultaDeAgendamento {

	private Long codigoDoMinistro;
	private Long sequencialObjetoIncidente;
	private Collection<Long> sequenciaisObjetosIncidentes;

	public Long getCodigoDoMinistro() {
		return codigoDoMinistro;
	}

	public void setCodigoDoMinistro(Long codigoDoMinistro) {
		this.codigoDoMinistro = codigoDoMinistro;
	}

	public Long getSequencialObjetoIncidente() {
		return sequencialObjetoIncidente;
	}

	public void setSequencialObjetoIncidente(Long sequencialObjetoIncidente) {
		this.sequencialObjetoIncidente = sequencialObjetoIncidente;
	}

	@Override
	public Collection<Long> getSequenciaisObjetosIncidentes() {
		return sequenciaisObjetosIncidentes;
	}

	public void setSequenciaisObjetosIncidentes(Collection<Long> sequenciaisObjetosIncidentes) {
		this.sequenciaisObjetosIncidentes = sequenciaisObjetosIncidentes;
	}

}
