package br.jus.stf.estf.decisao.support.action.handlers;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indica que a a��o s� deve ser apresentada se os 
 * textos forem todos favoritos ou todos n�o-favoritos.
 * 
 * @author Hertony.Morais
 * @since 31.03.2015
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CheckVisualizacaoAntecipadaSelecionados {
	public enum BooleanMode {SIM, NAO}
	
	BooleanMode value();
}