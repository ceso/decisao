/**
 * 
 */
package br.jus.stf.estf.decisao.texto.web;

import org.springframework.beans.factory.annotation.Autowired;

import br.gov.stf.estf.entidade.documento.tipofase.FaseTexto;
import br.gov.stf.estf.entidade.documento.tipofase.TipoTransicaoFaseTexto;
import br.jus.stf.estf.decisao.pesquisa.domain.TextoDto;
import br.jus.stf.estf.decisao.support.action.handlers.CheckMinisterId;
import br.jus.stf.estf.decisao.support.action.handlers.RequiresResources;
import br.jus.stf.estf.decisao.support.action.handlers.RequiresResources.Mode;
import br.jus.stf.estf.decisao.support.action.handlers.Restrict;
import br.jus.stf.estf.decisao.support.action.handlers.States;
import br.jus.stf.estf.decisao.support.action.support.Action;
import br.jus.stf.estf.decisao.support.action.support.ActionIdentification;
import br.jus.stf.estf.decisao.texto.service.TextoService;

/**
 * @author Paulo.Estevao
 * @since 04.10.2010
 */
@Action(id="desfazerJuntadaActionFacesBean", name="Desfazer Juntada", view = "/acoes/texto/transicao/executar.xhtml", height = 215, width = 500)
@Restrict({ActionIdentification.DESFAZER_JUNTADA})
@States({ FaseTexto.JUNTADO })
@RequiresResources(Mode.Many)
@CheckMinisterId
public class DesfazerJuntadaActionFacesBean extends AbstractAlterarFaseDoTextoActionFacesBean<TextoDto> {

	@Autowired
	private TextoService textoService;
	
	@Override
	protected void doExecute(TextoDto texto) throws Exception {
		try {
			textoService.desfazerJuntada(texto, textosProcessados, getObservacao(), getResponsavel());
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	protected String getErrorTitle() {
		return "N�o foi poss�vel juntar os textos abaixo:";
	}

	@Override
	protected TipoTransicaoFaseTexto getDestino() {
		return TipoTransicaoFaseTexto.DESFAZER_JUNTADA;
	}

		
}
