package br.jus.stf.estf.decisao.mobile.assinatura.support;

import java.io.Serializable;

public class VersaoDto implements Serializable {

	private static final long serialVersionUID = -8514063874232220184L;

	private String versao;
	private String ambiente;

	public VersaoDto(String versao, String ambiente) {
		this.versao = versao;
		this.ambiente = ambiente;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

}
