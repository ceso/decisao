/**
 * 
 */
package br.jus.stf.estf.decisao.texto.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.gov.stf.estf.documento.model.service.exception.TextoNaoPodeSerAlteradoException;
import br.gov.stf.estf.entidade.documento.ControleVoto;
import br.gov.stf.estf.entidade.documento.TipoTexto;
import br.gov.stf.estf.entidade.ministro.Ministro;
import br.gov.stf.estf.entidade.processostf.Processo;
import br.gov.stf.estf.ministro.model.service.MinistroService;
import br.gov.stf.framework.model.service.ServiceException;
import br.jus.stf.estf.decisao.objetoincidente.service.ObjetoIncidenteService;
import br.jus.stf.estf.decisao.pesquisa.domain.ObjetoIncidenteDto;
import br.jus.stf.estf.decisao.pesquisa.domain.TextoDto;
import br.jus.stf.estf.decisao.support.action.support.ActionSupport;
import br.jus.stf.estf.decisao.texto.service.TextoService;
import br.jus.stf.estf.decisao.texto.support.ProcessoInvalidoParaListaDeTextosException;

/**
 * @author Paulo.Estevao
 * @since 21.07.2010
 */
public abstract class AbstractCriarEditarListaTextosIguaisActionFacesBean<T> extends ActionSupport<TextoDto> {
	
	@Qualifier("textoServiceLocal")
	@Autowired
	private TextoService textoService;
	@Autowired
	private MinistroService ministroService;
	@Autowired
	private ObjetoIncidenteService objetoIncidenteService;
	
	private String identificacaoProcesso;
	
	private Collection<ObjetoIncidenteDto> processosParaSelecao;
	private Collection<ObjetoIncidenteDto> processosValidos;
	private Collection<ObjetoIncidenteDto> processosComEmentaGerada;
	private Collection<String> mensagemDeProcessosComEmentaGerada;
	
	private String errorTitle;
	
	@Override
	public void load() {
		try {
			if(getTextoSelecionado().isTextosIguais()) {
				carregaListaDeTextosIguais(getTextoSelecionado());
			}
			textoService.validaTextoParaAlteracao(getTextoSelecionado());
		} catch(ServiceException e) {
			addError(e.getMessage());
		} catch (TextoNaoPodeSerAlteradoException e) {
			addError(e.getMessage());
		}
		
		if(hasMessages()) {
			setErrorTitle("Erro ao carregar lista de textos iguais");
			sendToErrors();
		}
	}
	
	private void carregaListaDeTextosIguais(TextoDto texto) throws ServiceException {
		List<ObjetoIncidenteDto> objetosIncidente = textoService.recuperarProcessosListaTextosIguais(texto);
		for (ObjetoIncidenteDto oi : objetosIncidente) {
			getProcessosValidos().add(oi);
			getProcessosParaSelecao().add(oi);
		}
	}
	
	public void validateAndExecute() {
		if (getMensagemDeProcessosComEmentaGerada().isEmpty()) {
			execute(false);
		} else {
			for (String processo : getMensagemDeProcessosComEmentaGerada()) {
				addInformation(processo);
			}
			sendToInformations();
			getDefinition().setFacet("listaDeProcessosComEmentaGerada");
		}
	}
	
	public void excluirProcessosSelecionados() {
		Collection<ObjetoIncidenteDto> processos = getProcessosParaSelecao();
		Collection<ObjetoIncidenteDto> processosParaRetirar = new ArrayList<ObjetoIncidenteDto>();
		for (ObjetoIncidenteDto processo : processos) {
			if (processo.isSelected()) {
				processosParaRetirar.add(processo);
				getProcessosValidos().remove(processo);
				getProcessosComEmentaGerada().remove(processo);
			}
		}
		processos.removeAll(processosParaRetirar);
	}
	
	public void execute() {
		execute(false);
	}
	
	public void sobrescreverEmenta() {
		execute(true);
	}
		
	private void execute(boolean sobrescreverEmenta) {
		cleanMessages();
		try {
			if(sobrescreverEmenta) {
				getProcessosValidos().addAll(getProcessosComEmentaGerada());
				getProcessosComEmentaGerada().clear();
			}
			textoService.criarEditarListaTextosIguais(getTextoSelecionado(), getProcessosValidos(), getMinistro(), getUsuario(), sobrescreverEmenta);
		} catch (Exception e) {
			addError(e.getMessage());
		}

		setRefresh(true);
		
		if (!hasMessages()) {
			sendToConfirmation();
		} else {
			setErrorTitle("Erro ao gravar lista de textos.");
			sendToErrors();
		}
	}

	public void incluirProcessoSelecionado(ObjetoIncidenteDto objetoIncidenteSelecionado) {
		try {
			if (objetoIncidenteSelecionado == null) {
				throw new ProcessoInvalidoParaListaDeTextosException("Selecione um processo para inclus�o na lista!");
			}
			if (isProcessoNaLista(objetoIncidenteSelecionado)) {
				addInformation("O processo selecionado j� faz parte da lista!");
			} else {
				insereProcessoSelecionado(objetoIncidenteSelecionado, getTextoSelecionado());
			}
		} catch (ProcessoInvalidoParaListaDeTextosException e) {
			addInformation(e.getMessage());
		} catch (Exception e) {
			addError(e.getMessage());
		}		
		identificacaoProcesso = null;
	}
	
	public void selectAll() {
		boolean check = !allChecked();
		for (ObjetoIncidenteDto processo : processosValidos) {
			processo.setSelected(check);
		}
	}
	
	private boolean allChecked() {
    	for (ObjetoIncidenteDto dto : processosValidos) {
    		if (!dto.isSelected()) {
    			return false;
    		}
    	}
    	return true;
    }
	
	private boolean isProcessoNaLista(ObjetoIncidenteDto objetoIncidente) {
		if (objetoIncidente != null) {
			return isColecoesContemProcesso(objetoIncidente);
		}
		throw new RuntimeException("Ocorreu um erro ao recuperar o processo informado! Por favor, tente novamente!");
	}

	private boolean isColecoesContemProcesso(ObjetoIncidenteDto objetoIncidente) {
		return getProcessosParaSelecao().contains(objetoIncidente);
	}
	
	
	
	private void insereProcessoSelecionado(ObjetoIncidenteDto objetoIncidente, TextoDto texto) throws ServiceException, ProcessoInvalidoParaListaDeTextosException {
		Ministro ministroDoGabinete = getMinistro();
		verificaTextoRegistrado(texto, ministroDoGabinete, objetoIncidente);
		verificaControleDeVotos(texto, ministroDoGabinete, objetoIncidente);
		adicionaProcessoNaTabela(objetoIncidente);
	}
	
	private void verificaTextoRegistrado(TextoDto texto, Ministro ministroDoGabinete, ObjetoIncidenteDto objetoIncidente)
			throws ServiceException, ProcessoInvalidoParaListaDeTextosException {
		if ((texto.getTipoTexto().equals(TipoTexto.ACORDAO) || texto.getTipoTexto().equals(TipoTexto.RELATORIO) || texto.getTipoTexto().equals(TipoTexto.VOTO))
				&& textoService.existeTextoRegistradoParaProcesso(objetoIncidente, texto.getTipoTexto(), ministroDoGabinete) ) {
			throw new ProcessoInvalidoParaListaDeTextosException(montaMensagemDeErroDeTextoRegistrado(objetoIncidente, texto));
		}
	}
	
	private String montaMensagemDeErroDeTextoRegistrado(ObjetoIncidenteDto objetoIncidente, TextoDto texto) {
		StringBuilder sb = new StringBuilder();
		sb.append(objetoIncidente.getIdentificacao());
		sb.append(": Texto j� registrado para este processo.");
		return sb.toString();
	}
	
	private void verificaControleDeVotos(TextoDto texto, Ministro ministroDoGabinete, ObjetoIncidenteDto objetoIncidente)
			throws ServiceException, ProcessoInvalidoParaListaDeTextosException {
		if (precisaValidarControleDeVotos(ministroDoGabinete, objetoIncidente)) {
			validaControleDeVotos(texto, ministroDoGabinete, objetoIncidente);
		}
	}
	
	private void validaControleDeVotos(TextoDto texto, Ministro ministroDoGabinete, ObjetoIncidenteDto objetoIncidente)
			throws ServiceException, ProcessoInvalidoParaListaDeTextosException {
		ControleVoto controleDeVoto = textoService.consultaControleDeVotoDoProcesso(texto.getTipoTexto(), ministroDoGabinete, objetoIncidente);
		if (controleDeVoto == null && !TipoTexto.DESPACHO.equals(texto.getTipoTexto()) && !TipoTexto.DECISAO_MONOCRATICA.equals(texto.getTipoTexto())) {
			throw new ProcessoInvalidoParaListaDeTextosException(montaMensagemDeProcessoDeOutroRelator(objetoIncidente));
		}
	}
	
	private String montaMensagemDeProcessoDeOutroRelator(ObjetoIncidenteDto objetoIncidente) {
		StringBuilder sb = new StringBuilder();
		sb.append("O processo ");
		sb.append(objetoIncidenteService.recuperarObjetoIncidentePorId(objetoIncidente.getId()).getPrincipal().getIdentificacaoCompleta());
		sb.append(" pertence a outro relator.");
		return sb.toString();
	}
	
	private boolean precisaValidarControleDeVotos(Ministro ministroDoGabinete, ObjetoIncidenteDto objetoIncidente)
		throws ServiceException {
	return !(isMinistroDoSetorRelatorDoProcesso(ministroDoGabinete, objetoIncidente) || getMinistroService()
			.isMinistroTemRelatoriaDaPresidencia(ministroDoGabinete, (Processo) objetoIncidenteService.recuperarObjetoIncidentePorId(objetoIncidente.getId()).getPrincipal()));
	}
	
	private void adicionaProcessoNaTabela(ObjetoIncidenteDto objetoIncidente) throws ServiceException {
		if (TipoTexto.EMENTA.equals(getTextoSelecionado().getTipoTexto())) {
			TextoDto ementaGerada = textoService.recuperaEmentaGeradaParaProcesso(getTextoSelecionado(), objetoIncidente);
			if (ementaGerada != null) {
				getProcessosComEmentaGerada().add(objetoIncidente);
				adicionaMensagemDeEmentaGerada(objetoIncidente, ementaGerada);
			} else {
				getProcessosValidos().add(objetoIncidente);
			}
		} else {
			getProcessosValidos().add(objetoIncidente);
		}
		getProcessosParaSelecao().add(objetoIncidente);
	}
	
	private void adicionaMensagemDeEmentaGerada(ObjetoIncidenteDto objetoIncidente, TextoDto ementaGerada) {
		getMensagemDeProcessosComEmentaGerada().add(objetoIncidente.getIdentificacao() + " - " + ementaGerada.getMinistro());
	}
	
	private boolean isMinistroDoSetorRelatorDoProcesso(Ministro ministroDoGabinete, ObjetoIncidenteDto objetoIncidente)
			throws ServiceException {
		return getMinistroService().isMinistroRelatorDoProcesso(ministroDoGabinete, (Processo) objetoIncidenteService.recuperarObjetoIncidentePorId(objetoIncidente.getId()).getPrincipal());
	}
	
	private MinistroService getMinistroService() {
		return ministroService;
	}
	
	public Collection<ObjetoIncidenteDto> getProcessosParaSelecao() {
		if (processosParaSelecao == null) {
			processosParaSelecao = new ArrayList<ObjetoIncidenteDto>();
		}
		return processosParaSelecao;
	}
	
	public Collection<ObjetoIncidenteDto> getProcessosValidos() {
		if (processosValidos == null) {
			processosValidos = new ArrayList<ObjetoIncidenteDto>();
		}
		return processosValidos;
	}
	
	public Collection<ObjetoIncidenteDto> getProcessosComEmentaGerada() {
		if (processosComEmentaGerada == null) {
			processosComEmentaGerada = new ArrayList<ObjetoIncidenteDto>();
		}
		return processosComEmentaGerada;
	}
	
	public Collection<String> getMensagemDeProcessosComEmentaGerada() {
		if (mensagemDeProcessosComEmentaGerada == null) {
			mensagemDeProcessosComEmentaGerada = new ArrayList<String>();
		}
		return mensagemDeProcessosComEmentaGerada;
	}
	
	public TextoDto getTextoSelecionado() {
		if (getResources() != null && getResources().size() == 1) {
			return (TextoDto) getResources().toArray()[0];
		}
		throw new IllegalArgumentException("N�o � poss�vel recuperar o texto selecionado!");
	}
	
	public String getIdentificacaoProcesso() {
		return identificacaoProcesso;
	}

	public void setIdentificacaoProcesso(String identificacaoProcesso) {
		this.identificacaoProcesso = identificacaoProcesso;
	}
	
	private void setErrorTitle(String error) {
		errorTitle = error;
	}
	
	@Override
	public String getErrorTitle() {
		return errorTitle;
	}
}
