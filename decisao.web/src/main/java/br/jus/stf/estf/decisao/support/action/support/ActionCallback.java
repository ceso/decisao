package br.jus.stf.estf.decisao.support.action.support;


/**
 * Interface callback usada pelo m�todo {@link ActionSupport#execute(ActionCallback)}.
 * 
 * <p>Usada para rodar a��es dentro de um contexto protegido, com tratamento de 
 * erros, envio de mensagens e navega��o de fluxo de tela.
 * 
 * @author Rodrigo.Barreiros
 * @since 24.05.2010
 *
 * @param <T> o tipo de recurso
 */
public interface ActionCallback<T> {
    
    /**
     * Chamado pelo m�todo {@link ActionSupport#execute(ActionCallback)} para executar
     * a��es em um contexto protegido.
     * 
     * @param resource o recurso que ser� utilizado pela a��o
     * @throws Exception caso ocorra algum problema
     */
    void doInAction(T resource) throws Exception;

}
