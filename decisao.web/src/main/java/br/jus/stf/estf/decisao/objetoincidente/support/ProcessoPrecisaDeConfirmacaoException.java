package br.jus.stf.estf.decisao.objetoincidente.support;

public class ProcessoPrecisaDeConfirmacaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProcessoPrecisaDeConfirmacaoException() {
	}

	public ProcessoPrecisaDeConfirmacaoException(String message) {
		super(message);
	}

	public ProcessoPrecisaDeConfirmacaoException(Throwable cause) {
		super(cause);
	}

	public ProcessoPrecisaDeConfirmacaoException(String message, Throwable cause) {
		super(message, cause);
	}

}
