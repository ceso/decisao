package br.jus.stf.estf.decisao.texto.support;

public class ManterListaDeTextosException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3852132020901229822L;

	public ManterListaDeTextosException() {
	}

	public ManterListaDeTextosException(String message) {
		super(message);
	}

	public ManterListaDeTextosException(Throwable cause) {
		super(cause);
	}

	public ManterListaDeTextosException(String message, Throwable cause) {
		super(message, cause);
	}

	
}
