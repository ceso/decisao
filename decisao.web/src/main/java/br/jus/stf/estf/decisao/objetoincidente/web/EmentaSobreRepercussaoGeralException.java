package br.jus.stf.estf.decisao.objetoincidente.web;

public class EmentaSobreRepercussaoGeralException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1406279266864536885L;

	public EmentaSobreRepercussaoGeralException(String message) {
		super(message);
	}

}
