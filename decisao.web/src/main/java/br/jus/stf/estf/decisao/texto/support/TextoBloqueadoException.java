/**
 * 
 */
package br.jus.stf.estf.decisao.texto.support;

/**
 * @author Paulo.Estevao
 * @since 06.04.2011
 */
public class TextoBloqueadoException extends Exception {

	private static final long serialVersionUID = -3713125695861008863L;

	public TextoBloqueadoException(String message) {
		super(message);
	}

}
