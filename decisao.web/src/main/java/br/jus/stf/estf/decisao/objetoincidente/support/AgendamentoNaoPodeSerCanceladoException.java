package br.jus.stf.estf.decisao.objetoincidente.support;

public class AgendamentoNaoPodeSerCanceladoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4429949815945658961L;

	public AgendamentoNaoPodeSerCanceladoException(String message) {
		super(message);
	}

}
