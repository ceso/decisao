package br.jus.stf.estf.decisao.documento.support.assinador.timestamp;
public class ServidorCarimbo {
	private String nome;
	private String url;
	
	public ServidorCarimbo(String nome, String url) {
		super();
		this.nome = nome;
		this.url = url;
	}	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
