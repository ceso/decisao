package br.jus.stf.estf.decisao.texto.service;

/**
 * Indica que um texto j� foi juntado.
 * 
 * @author Rodrigo Barreiros
 * @see 22.07.2010
 */
public class TextoJaJuntadoException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constroi a exce��o montando a mensagem em fun��o do flag de disponibiliza��o
	 * das pe�as juntadas.
	 * 
	 * @param disponibilizarNaInternet flag de disponibiliza��o
	 */
	public TextoJaJuntadoException(boolean disponibilizarNaInternet) {
		super(getMessage(disponibilizarNaInternet));
	}

	/**
	 * Montagem a mensagem de erro indicando qual a situa��o atual.
	 * 
	 * @param disponibilizarNaInternet flag de disponibiliza��o
	 * @return a mensagem montada
	 */
	private static String getMessage(boolean disponibilizarNaInternet) {
		String mensagem = "O texto j� estava juntado ao processo.";
		if (disponibilizarNaInternet) {
			mensagem += " Apenas a disponibiliza��o na internet foi realizada!";
		} else {
			mensagem += " Nenhuma altera��o foi realizada.";
		}
		return mensagem;
	}

}
