package br.jus.stf.estf.decisao.texto.support;

public class ProcessoInvalidoParaListaDeTextosException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5344659478991579739L;

	public ProcessoInvalidoParaListaDeTextosException() {
	}

	public ProcessoInvalidoParaListaDeTextosException(String message) {
		super(message);
	}

	public ProcessoInvalidoParaListaDeTextosException(Throwable cause) {
		super(cause);
	}

	public ProcessoInvalidoParaListaDeTextosException(String message,
			Throwable cause) {
		super(message, cause);
	}

}