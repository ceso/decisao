package br.jus.stf.estf.decisao.texto.support;

public class DeslocamentoNaoEncontradoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7973841870291649246L;
	public DeslocamentoNaoEncontradoException(String message) {
		super(message);
	}


}
