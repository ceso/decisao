package br.jus.stf.estf.decisao.support.action.handlers;

import java.util.Map;
import java.util.Set;

/**
 * Abstra��o para tratar a��es e determinar se uma a��o pode ou n�o ser exibida
 * dado o contexto de execu��o.
 * 
 * @author Rodrigo.Barreiros
 * @since 24.05.2010
 * 
 * @param <A> o tipo da anota��o vinculada ao handler
 */
public interface ActionConditionHandler<A> {
    
	/**
	 * Verifica se uma dada a��o pode ou n�o ser exibida ao usu�rio, dada a configura��o 
	 * do handler (annotation), os recursos selecionados, o tipo de recurso e 
	 * outras op��es que poder�o ser utilizadas para decis�o.
	 * 
	 * @param <T> o tipo de recurso
	 * @param annotation a anota��o utilizada para configurar e indicar que esse
	 * handler deve tratar uma determinada a��o
	 * @param resources o recursos selecionada na tela
	 * @param resourceClass o tipo de recurso selecionado
	 * @param options outras op��es utilizadas para decis�o
	 * 
	 * @return true, se a a��o deve ser listada, false, caso contr�rio
	 */
	<T> boolean matches(A annotation, Set<T> resources, Class<T> resourceClass, Map<?, ?> options);
    
    /**
     * Retorna a anota��o utilizada para configurar e indicar que esse
	 * handler deve tratar uma determinada a��o.
	 * 
     * @return a anota��o associada ao handler
     */
    Class<A> getAnnotation();

}
