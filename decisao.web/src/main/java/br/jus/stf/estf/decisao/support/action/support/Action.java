package br.jus.stf.estf.decisao.support.action.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indica que a classe anotada � uma A��o.
 * 
 * @author Rodrigo.Barreiros
 * @since 24.05.2010
 * 
 * @see ActionController
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Action {
    
    /**
     * O identificador da a��o. A a��o pode ser acessada na camada de apresenta��o usando
     * esse identificador
     * @return id da a��o
     */
    String id();
    
    
    /**
     * O nome da a��o. � esse nome que ser� apresentado no menu de a��es
     * @return o nome da a��o
     */
    String name();
    
    
    /**
     * A p�gina facelets que ser� utilizada para entrada de informa��es 
     * necess�rias � execu��o da a��o
     * @return
     */
    String view() default "";
    
    
    /**
     * Indica que p�gina (facet) deve ser exibida em um dado momento 
     * da execu��o da a��o. Muitas a��es utilizam v�rias p�ginas,
     * como em um wizard e precisa controlar p�gina deve ser
     * apresentada e quando.
     * @return a p�gina que ser� exibida
     */
    String facet() default "principal";
    
    /**
     * A p�gina da a��o � exibida em uma popup. Indica o tamanho da popup.
     * @return o tamanho da popup
     */
    int height() default 180;
    
    
    /**
     * A p�gina da a��o � exibida em uma popup. Indica a largura da popup.
     * @return a largura da popup
     */
    int width() default 500;
    
    /**
     * Indica se a a��es exibir� algum relat�rio.
     * @return true, se exibir, false, caso contr�rio
     */
    boolean report() default false;

}
