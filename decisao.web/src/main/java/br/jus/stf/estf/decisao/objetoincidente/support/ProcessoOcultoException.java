/**
 * 
 */
package br.jus.stf.estf.decisao.objetoincidente.support;

/**
 * @author Paulo.Estevao
 * @since 20.10.2011
 */
public class ProcessoOcultoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3262717595621580958L;

	public ProcessoOcultoException (String message) {
		super(message);
	}
}
