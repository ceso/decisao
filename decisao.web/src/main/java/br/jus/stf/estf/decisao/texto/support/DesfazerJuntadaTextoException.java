/**
 * 
 */
package br.jus.stf.estf.decisao.texto.support;

/**
 * @author Paulo.Estevao
 * @since 04.10.2010
 */
public class DesfazerJuntadaTextoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DesfazerJuntadaTextoException() {
		super("Erro ao desfazer juntada de pe�as.");
	}
}
