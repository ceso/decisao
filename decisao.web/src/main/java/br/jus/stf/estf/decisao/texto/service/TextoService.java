package br.jus.stf.estf.decisao.texto.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import br.gov.stf.estf.documento.model.exception.TextoException;
import br.gov.stf.estf.documento.model.exception.TextoInvalidoParaPecaException;
import br.gov.stf.estf.documento.model.service.exception.NaoExisteDocumentoAssinadoException;
import br.gov.stf.estf.documento.model.service.exception.TextoNaoPodeSerAlteradoException;
import br.gov.stf.estf.documento.model.service.exception.TransicaoDeFaseInvalidaException;
import br.gov.stf.estf.documento.model.util.AssinaturaDto;
import br.gov.stf.estf.entidade.documento.ArquivoEletronicoView;
import br.gov.stf.estf.entidade.documento.ControleVoto;
import br.gov.stf.estf.entidade.documento.ListaTextos;
import br.gov.stf.estf.entidade.documento.Texto;
import br.gov.stf.estf.entidade.documento.Texto.TipoRestricao;
import br.gov.stf.estf.entidade.documento.TipoDocumentoTexto;
import br.gov.stf.estf.entidade.documento.TipoTexto;
import br.gov.stf.estf.entidade.documento.tipofase.TipoTransicaoFaseTexto;
import br.gov.stf.estf.entidade.ministro.Ministro;
import br.gov.stf.estf.entidade.processostf.Classe;
import br.gov.stf.estf.entidade.processostf.IncidenteJulgamento;
import br.gov.stf.estf.entidade.processostf.ObjetoIncidente;
import br.gov.stf.estf.entidade.processostf.TipoIncidenteJulgamento;
import br.gov.stf.estf.entidade.processostf.TipoJulgamento;
import br.gov.stf.estf.entidade.usuario.Responsavel;
import br.gov.stf.estf.entidade.usuario.Usuario;
import br.gov.stf.estf.processostf.model.service.exception.DuplicacaoChaveAntigaException;
import br.gov.stf.framework.model.service.ServiceException;
import br.jus.stf.estf.decisao.objetoincidente.support.ProcessoOcultoException;
import br.jus.stf.estf.decisao.pesquisa.domain.ObjetoIncidenteDto;
import br.jus.stf.estf.decisao.pesquisa.domain.TextoDto;
import br.jus.stf.estf.decisao.support.security.Principal;
import br.jus.stf.estf.decisao.texto.support.ConsultaDadosDoTextoDto;
import br.jus.stf.estf.decisao.texto.support.SetorInativoException;
import br.jus.stf.estf.decisao.texto.support.TextoBloqueadoException;
import br.jus.stf.estf.decisao.texto.support.TextoComSituacaoDaPublicacaoVO;
import br.jus.stf.estf.decisao.texto.support.TextoNaoPodeSerRestritoException;

/**
 * Fornece servi�os de neg�cio relacionados a Textos.
 * 
 * <p>Deve ser o ponto �nico de acesso a servi�os que implementam regras 
 * de neg�cio que envolvem textos.
 * 
 * @author Rodrigo.Barreiros
 * @since 15.04.2010
 */
public interface TextoService {

	/**
	 * Altera a fase de um texto, aplicando todas as regras pertinentes.
	 * 
	 * @param texto o identificador do texto
	 * @param tipoTransicao o tipo de transi��o
	 * @param textosProcessados Ids dos textos que j� foram processados
	 * @param observacao Observa��o da transi��o de fase
	 * @param responsavel Respons�vel pela fase
	 * @throws TransicaoDeFaseInvalidaException lan�ada caso
	 * o tipo de transi��o seja inv�lida
	 * @throws TextoBloqueadoException 
	 * @throws ProcessoOcultoException 
	 */
	void alterarFase(TextoDto texto, TipoTransicaoFaseTexto tipoTransicao, Set<Long> textosProcessados, String observacao, Responsavel responsavel)
			throws TransicaoDeFaseInvalidaException, TextoBloqueadoException, ProcessoOcultoException;
	
	/**
	 * Libera um dado texto para publica��o.
	 * 
	 * @param texto o identificador do texto
	 * @param texto O texto que ser� liberado
	 * @param textosProcessados Ids dos textos que j� foram processados
	 * @param liberarRtj Indica se quer que o texto seja liberado para RTJ
	 * @param usuario TODO
	 * @param observacao Observa��o da transi��o de fase
	 * @throws TransicaoDeFaseInvalidaException lan�ada caso
	 * o texto n�o possa ser transitado para publica��o
	 * @return Cole��o de mensagens dos textos processados para ser mostrada ao cliente.
	 * @throws TransicaoDeFaseInvalidaException
	 * @throws TextoBloqueadoException 
	 * @throws ProcessoOcultoException 
	 * @throws SetorInativoException 
	 */
	Collection<String> liberarParaPublicacao(TextoDto texto, Set<Long> textosProcessados, boolean liberarRtj, Principal usuario, String observacao, Responsavel responsavel) throws TransicaoDeFaseInvalidaException, TextoBloqueadoException, ProcessoOcultoException, SetorInativoException;

	/**
	 * Recupera a lista de textos relacionados a um dado objeto incidente e a
	 * um dado ministro. Alguns restri��es:
	 * - O Texto deve ser, efetivamente, do Ministro e
	 * - Somente Textos que n�o sejam de Decis�o.
	 * 
	 * @param objetoIncidente o objeto incidente do texto
	 * @param ministro o ministro solicitado
	 * @return a lista de textos
	 */
	List<TextoDto> recuperarTextos(ObjetoIncidente<?> objetoIncidente, Ministro ministro, Principal principal) throws ServiceException;

	/**
	 * Recupera a lista de textos relacionados a um dado objeto incidente.
	 * Alguns restri��es:
	 * - Somente Textos associados a outro Ministro;
	 * - Somente Textos P�blicos e
	 * - Somente Textos que n�o sejam de Decis�o.
	 * 
	 * @param objetoIncidente o objeto incidente do texto
	 * @return a lista de textos
	 */
	List<TextoDto> recuperarTextos(ObjetoIncidente<?> objetoIncidente);
	
	/**
	 * Recupera a lista de textos relacionados a um dado objeto incidente.
	 * @param objetoIncidente o objeto incidente do texto
	 * @param adicionarControleDeVotos
	 * @return a lista de textos
	 */
	List<TextoDto> recuperarTextos(ObjetoIncidente<?> objetoIncidente, boolean adicionarControleDeVotos);

	/**
	 * Pesquisa listas de texto dado o nome ou parte do nome da lista.
	 * 
	 * @param nome o nome da lista
	 * @return a lista de listas
	 */
	List<ListaTextos> pesquisarListasTextos(String string);

	/**
	 * Recupera os textos iguais ao texto relacionado a um dao identificador
	 * 
	 * @param id o identificador do texto
	 * @return a lista de textos iguais
	 */
	List<Texto> recuperarTextosIguais(Long id);

	/**
	 * Recupera o Texto dado o identificador.
	 * 
	 * @param id o identificador do texto
	 * @return o texto
	 */
	Texto recuperarTextoPorId(Long id);

	/**
	 * Cancela a assinatura de um determinado texto.
	 * @param texto Identificador do texto
	 * @param transicaoDeFase A transi��o de fase que est� ocorrendo 
	 * @param textosProcessados Os textos que j� foram processados por essa a��o.
	 * @param observacao Observa��o inserida pelo usu�rio que justifica o cancelamento da assinatura.
	 * @throws TextoBloqueadoException 
	 * @throws ProcessoOcultoException 
	 */
	void cancelarAssinatura(TextoDto texto, TipoTransicaoFaseTexto transicaoDeFase, Set<Long> textosProcessados, String observacao, Responsavel responsavel) throws TextoBloqueadoException, ProcessoOcultoException;

	/**
	 * Verifica se o texto est� bloqueado para edi��o por algum usu�rio.
	 * @param texto
	 * @throws ServiceException
	 * @throws TextoBloqueadoException 
	 */
	void verificaTextoBloqueado(Texto texto) throws ServiceException, TextoBloqueadoException;

	/**
	 * Verifica se o textoDto est� bloqueado para edi��o por algum usu�rio.
	 * @param texto
	 * @throws ServiceException
	 * @throws TextoBloqueadoException 
	 */
	void verificaTextoBloqueado(TextoDto texto) throws ServiceException, TextoBloqueadoException;

	/**
	 * Recupera o ArquivoEletronicoView pelo id.
	 * @param id
	 * @return
	 */
	ArquivoEletronicoView recuperarArquivoEletronicoViewPeloId(Long id);
	
	/**
	 * Recupera a ementa de um determinado texto.
	 * @param texto
	 * @param ministro
	 * @return
	 * @throws ServiceException 
	 */
	Texto recuperarEmenta(Texto texto, Ministro ministro) throws ServiceException;
	
	Texto recuperarEmenta(TextoDto textoDto, Ministro ministro) throws ServiceException;

	Texto recuperarDecisaoRepercussaoGeral(Texto texto, Ministro ministro) throws ServiceException;
	
	Texto recuperarEmentaRepercussaoGeral(Texto texto, Ministro ministro) throws ServiceException;
	
	Texto recuperarEmentaRepercussaoGeral(TextoDto textoDto, Ministro ministro) throws ServiceException;

	
	/**
	 * Recupera a lista de textos iguais que ser� levada em considera��o para a transi��o de fase.
	 * O m�todo avalia quais textos ser�o afetados, segundo as regras de transi��o de fase, que s�o as
	 * seguintes:
	 * 1)Se for uma transi��o progressiva, vai trazer todos os registros que estejam entre a fase
	 * do texto base e a fase de destino da transi��o
	 * 2)Se for uma transi��o regressiva, retornar� todos os registros que estejam em uma fase
	 * menor que a do texto base e maior que a do destino.
	 * @param textoDto
	 * @param tipoTransicao
	 * @return
	 * @throws TransicaoDeFaseInvalidaException Quando uma transi��o Liberar para publica��o � enviada em uma lista com textos n�o assinados.
	 * @throws ProcessoOcultoException 
	 */
	List<Texto> pesquisarTextosIguaisParaTransicaoFase(TextoDto textoDto, TipoTransicaoFaseTexto tipoTransicao)
			throws TransicaoDeFaseInvalidaException, ProcessoOcultoException;

	/**
	 * Recupera o sequencial de um novo Documento Eletronico, sem inserir um novo registro 
	 * @param texto
	 * @return
	 */
	Long recuperarSequencialDoDocumentoEletronico(TextoDto texto);

	/**
	 * Verifica se os textos possuem alguma restri��o 
	 * @param textos Lista de textos
	 * @param idUsuario Identificador do usu�rio
	 * @param idSetor Identificador do setor
	 * @return
	 */
	public boolean verificarRestricaoTextos(Collection<Texto> textos, String idUsuario, Long idSetor);
	
	/**
	 * Pesquisa os tipos de documento texto que s�o dispon�veis para um determinado setor
	 * @param codSetor
	 * @return
	 */
	List<TipoDocumentoTexto> pesquisarTiposDocumentoTextoPorSetor(long codSetor);

	/**
	 * Recupera um tipo de documento texto pelo Id
	 * @param idTipoDocumentoTexto
	 * @return
	 */
	TipoDocumentoTexto recuperarTipoDocumentoTextoPorId(Long idTipoDocumentoTexto);

	TextoDto pesquisarEmentaParaTextoIgual(ConsultaDadosDoTextoDto consulta);

	boolean isTextoJaRegistradoParaProcesso(ConsultaDadosDoTextoDto consulta);

	List<TextoDto> pesquisar(ConsultaDadosDoTextoDto consulta);

	/**
	 * Criar textos replicados a partir de um texto para os objetosIncidente e
	 * para o ministro passados.
	 * @param texto texto base para replica��o.
	 * @param objetosIncidente objetosIncidente que receber�o os textos replicados.
	 * @param ministro ministro para o qual os textos ser�o criados.
	 */
	void criarListaTextosReplicados(TextoDto texto, Collection<ObjetoIncidenteDto> objetosIncidente, Ministro ministro);

	/**
	 * Verifica se j� existe um texto para o objetoIncidente, tipoTexto e ministro
	 * @param objetoIncidente
	 * @param tipoTexto
	 * @param ministroDoGabinete
	 * @return
	 * @throws ServiceException
	 */
	boolean existeTextoRegistradoParaProcesso(ObjetoIncidenteDto objetoIncidente, TipoTexto tipoTexto,
			Ministro ministroDoGabinete) throws ServiceException;
	
	ControleVoto consultaControleDeVotoDoProcesso(TipoTexto tipoTexto, Ministro ministroDoGabinete,
			ObjetoIncidenteDto objetoIncidente) throws ServiceException;
	
	TextoDto recuperarEmenta(ObjetoIncidenteDto objetoIncidente) throws ServiceException;


	TextoDto recuperarAcordao(ObjetoIncidenteDto objetoIncidente) throws ServiceException;

	/**
	 * Recupera os tipos de texto que devem ser apresentados para um determinado objeto incidente.
	 * @param objetoIncidente
	 * @param ministro
	 * @return
	 */
	List<TipoTexto> recuperarTipoTextoPadrao(ObjetoIncidente<?> objetoIncidente, Ministro ministro);

	/**
	 * Recupera os tipos de incidentes cadastrados para sele��o
	 * @param Classe Processual 
	 * @return Tipos de Incidente de Julgamento ordenados pela descri��o
	 */
	List<TipoIncidenteJulgamento> recuperarTiposDeIncidentes(Classe classeProcessual);

	/**
	 * Recupera a proxima sequencia do tipo de incidente de julgamento.
	 * @param idObjetoIncidente O sequencial do objeto incidente
	 * @param idTipoIncidenteJulgamento O identificador do tipo de incidente de julgamento
	 * @return
	 */
	Integer recuperarProximaSequenciaCadeia(Long idObjetoIncidente, Long idTipoIncidenteJulgamento);
	
	/**
	 * Cria um novo incidente de julgamento
	 * @param idObjetoIncidente O Id do Objeto Incidente
	 * @param idTipoIncidenteJulgamento O tipo de incidente de julgamento a ser criado
	 * @param sequenciaCadeia A sequencia da cadeia
	 * @return
	 * @throws DuplicacaoChaveAntigaException 
	 */
	IncidenteJulgamento inserirIncidenteJulgamento(Long idObjetoIncidente, Long idTipoIncidenteJulgamento, Integer sequenciaCadeia) throws DuplicacaoChaveAntigaException;

	/**
	 * M�todo que verifica se o novo texto pode ser criado.
	 * @param texto
	 */
	void validarNovoTexto(Texto texto);

	void validaExclusaoTexto(TextoDto texto) throws TextoException, ServiceException;

	void excluirTextos(Collection<TextoDto> textos) throws ServiceException;

	List<ObjetoIncidenteDto> recuperarProcessosListaTextosIguais(TextoDto texto) throws ServiceException;

	TextoDto recuperaEmentaGeradaParaProcesso(TextoDto texto, ObjetoIncidenteDto objetoIncidente) throws ServiceException;

	void verificaTextoLiberadoParaPublicacao(TextoDto texto) throws ServiceException, TextoNaoPodeSerAlteradoException;
	
	/**
	 * Verifica se um texto pode ser alterado. Em caso negativo, lan�a uma exce��o do tipo TextoNaoPodeSerAlteradoException
	 * @param texto Texto a ser alterado
	 * @throws ServiceException
	 * @throws TextoNaoPodeSerAlteradoException
	 */
	void validaTextoParaAlteracao(TextoDto texto) throws ServiceException, TextoNaoPodeSerAlteradoException;
	
	/**
	 * Retorna uma lista contendo os textos iguais, excluindo o texto base
	 * @param texto Texto base para pesquisa de textos iguais
	 * @return Lista de textos iguais ao texto base
	 * @throws ServiceException
	 */
	List<TextoDto> pesquisarTextosIguais(TextoDto texto) throws ServiceException;

	/**
	 * Cria ou editar (caso j� exista) lista de textos iguais
	 * @param texto Texto base para lista
	 * @param processosValidos Processos da lista de textos iguais
	 * @param ministro Ministro dos textos da lista
	 * @param usuarioLogado Usuario de inclusao do texto
	 * @param sobrescreverEmenta Indicativo se os textos de Ementa dos processos de destino devem ser sobrescritos
	 * @throws TransicaoDeFaseInvalidaException
	 */
	void criarEditarListaTextosIguais(TextoDto texto, Collection<ObjetoIncidenteDto> processosValidos, Ministro ministro, 
			Usuario usuarioLogado, boolean sobrescreverEmenta) throws TransicaoDeFaseInvalidaException;
	
	/**
	 * Junta as pe�as relacionadas a um dado texto informando se essas pe�as
	 * devem ser disponibilizadas, ou n�o, para exibi��o na internet.
	 * @param textoDto o texto a que est�o relacionadas as pe�as
	 * @param textosProcessados ids dos textos que j� foram processados
	 * @param disponibilizarNaInternet o flag indicando se as pe�as
	 * @param usuario TODO
	 * @param observacao observa��o inserida pelo usu�rio
	 * @return 
	 * @throws NaoExisteDocumentoAssinadoException
	 * @throws TextoInvalidoParaPecaException
	 * @throws TextoJaJuntadoException
	 * @throws TextoBloqueadoException 
	 * @throws ProcessoOcultoException 
	 */
	Collection<String> juntarPecas(TextoDto textoDto,
			Set<Long> textosProcessados, boolean disponibilizarNaInternet, Principal usuario, String observacao, Responsavel responsavel)
			throws NaoExisteDocumentoAssinadoException,
			TextoInvalidoParaPecaException, TextoJaJuntadoException, TextoBloqueadoException, ProcessoOcultoException;
	
	/**
	 * M�todo que verifica se a situa��o do texto permite que ele seja liberado para publica��o.
	 * @param textos
	 * @return
	 * @throws ServiceException
	 */
	List<TextoComSituacaoDaPublicacaoVO> consultarSituacoesDePublicacaoDosTextos(Collection<TextoDto> textos);

	/**
	 * Suspende a publica��o dos textos enviados.
	 * @param texto
	 * @param textosProcessados
	 * @param observacao
	 * @param usuario
	 * @return
	 * @throws TransicaoDeFaseInvalidaException 
	 * @throws TextoInvalidoParaPecaException 
	 * @throws ProcessoOcultoException 
	 */
	Collection<String> suspenderPublicacao(TextoDto texto, Set<Long> textosProcessados, String observacao, Principal usuario, Responsavel responsavel) throws TransicaoDeFaseInvalidaException, TextoInvalidoParaPecaException, ProcessoOcultoException;

	/**
	 * Pesquisa os textos por objetoIncidente, tipoTexto e ministro 
	 * @param objetoIncidente
	 * @param tipoTexto
	 * @param ministro
	 * @return Lista de Textos
	 * @throws ServiceException 
	 */
	List<Texto> pesquisar(ObjetoIncidenteDto objetoIncidente,
			TipoTexto tipoTexto, Ministro ministro) throws ServiceException;

	/**
	 * Executa a restri��o de acesso ao texto selecionado. Se existem textos iguais ao selecionado, uma mensagem indicando essa
	 * situa��o ser� exibida. O mesmo ocorrer� se o texto selecionado for p�blico.
	 * 
	 * @param textoDto O texto que ser� restringido
	 * @param tipoRestricao O tipo da restri��o
	 * @param siglaUsuario A sigla do usu�rio que solicitou a restri��o
	 * @throws TextoNaoPodeSerRestritoException Quando o texto n�o puder ser restrito (est� em lista de textos iguais ou p�blico)
	 */
	void alterarAcessoDoTexto(TextoDto textoDto, TipoRestricao tipoRestricao, String siglaUsuario) throws TextoNaoPodeSerRestritoException;
	
	/**
	 * Desfaz a juntada das pe�as relacionadas a um dado texto.
	 * 
	 * @param textoDto o texto a que est�o relacionadas as pe�as
	 * @param textosProcessados
	 * @param observacao
	 * @throws ProcessoOcultoException 
	 */
	void desfazerJuntada(TextoDto textoDto, Set<Long> textosProcessados, String observacao, Responsavel responsavel) throws ProcessoOcultoException;

	/**
	 * M�todo que consulta quais s�o os controles de voto registrados para um determinado objeto incidente.
	 * @param objetoIncidente
	 * @return
	 * @throws ServiceException
	 */
	List<ControleVoto> consultarControleDeVotoDoProcesso(ObjetoIncidenteDto objetoIncidente) throws ServiceException;

	/**
	 * Recupera o tipo julgamento para o par tipoRecurso e numSequenciaCadeia
	 * @param seqTipoRecurso
	 * @param sequenciaCadeia
	 * @return
	 */
	TipoJulgamento recuperarTipoJulgamento(Long seqTipoRecurso, Long sequenciaCadeia);
	
	/**
	 * Recuperar um tipo de incidente julgamento pelo id.
	 * @param id
	 * @return
	 */
	TipoIncidenteJulgamento recuperarTipoIncidenteJulgamentoPorId(Long id);

	/**
	 * Retorna uma lista contendo os textos iguais, excluindo o texto base
	 * @param texto Texto base para pesquisa de textos iguais
	 * @param limitarTextosDoMinistro Restringe a pesquisa aos textos do ministro se for true
	 * @return Lista de textos iguais ao texto base
	 * @throws ServiceException
	 */
	List<TextoDto> pesquisarTextosIguais(TextoDto texto,
			boolean limitarTextosDoMinistro) throws ServiceException;

	Long recuperarSequencialDoUltimoDocumentoEletronico();

	void testarAssinaturaTexto(AssinaturaDto assinaturaDto);

	void assinarTextoContingencialmente(AssinaturaDto assinaturaDto);

	void republicarTexto(TextoDto texto);

	boolean isTipoIncidenteJulgamentoPermitidoParaClasse(TipoIncidenteJulgamento tipoIncidenteJulgamento, Classe classeProcessual);

	void validaAcessoTextosRestritos(Principal principal, List<TextoDto> dtos);

	void marcarComoFavoritos(Collection<TextoDto> textos) throws ServiceException ;

	void desmarcarComoFavoritos(Collection<TextoDto> textos) throws ServiceException ;
	
	public List<Texto> recuperarListaTextos(Collection<TextoDto> textos) throws ServiceException;

	public void liberarVisualizacaoAntecipada(Collection<TextoDto> textosDto, boolean liberacaoAntecipada) throws ServiceException;
}
 