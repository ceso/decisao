package br.jus.stf.estf.decisao.objetoincidente.web;

public class TextoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1406279266864536885L;

	public TextoException(String message) {
		super(message);
	}

}
