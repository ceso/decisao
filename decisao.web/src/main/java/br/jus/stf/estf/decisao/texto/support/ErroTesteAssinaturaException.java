/**
 * 
 */
package br.jus.stf.estf.decisao.texto.support;


/**
 * @author Paulo.Estevao
 *
 */
public class ErroTesteAssinaturaException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -428074709673806203L;

	public ErroTesteAssinaturaException(Exception e) {
		super(e);
	}
	
}
