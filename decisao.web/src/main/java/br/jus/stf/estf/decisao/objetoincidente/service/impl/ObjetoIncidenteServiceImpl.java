package br.jus.stf.estf.decisao.objetoincidente.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.gov.stf.estf.documento.model.service.TextoService;
import br.gov.stf.estf.documento.model.util.ConsultaDadosDoTextoVO;
import br.gov.stf.estf.entidade.documento.Texto;
import br.gov.stf.estf.entidade.documento.TipoFaseComunicacao;
import br.gov.stf.estf.entidade.documento.TipoTexto;
import br.gov.stf.estf.entidade.documento.tipofase.FaseTexto;
import br.gov.stf.estf.entidade.julgamento.Colegiado;
import br.gov.stf.estf.entidade.julgamento.Colegiado.TipoColegiadoConstante;
import br.gov.stf.estf.entidade.julgamento.Envolvido;
import br.gov.stf.estf.entidade.julgamento.InformacaoPautaProcesso;
import br.gov.stf.estf.entidade.julgamento.InformacaoPautaProcesso.TipoSituacaoPauta;
import br.gov.stf.estf.entidade.julgamento.JulgamentoProcesso;
import br.gov.stf.estf.entidade.julgamento.ListaJulgamento;
import br.gov.stf.estf.entidade.julgamento.PrevisaoSustentacaoOral;
import br.gov.stf.estf.entidade.julgamento.ProcessoListaJulgamento;
import br.gov.stf.estf.entidade.julgamento.Sessao;
import br.gov.stf.estf.entidade.julgamento.Sessao.TipoAmbienteConstante;
import br.gov.stf.estf.entidade.julgamento.TipoListaJulgamento;
import br.gov.stf.estf.entidade.julgamento.enuns.SituacaoListaJulgamento;
import br.gov.stf.estf.entidade.jurisdicionado.Jurisdicionado;
import br.gov.stf.estf.entidade.localizacao.Setor;
import br.gov.stf.estf.entidade.ministro.Ministro;
import br.gov.stf.estf.entidade.ministro.Ocorrencia;
import br.gov.stf.estf.entidade.processosetor.ProcessoSetor;
import br.gov.stf.estf.entidade.processostf.Agendamento;
import br.gov.stf.estf.entidade.processostf.Agendamento.AgendamentoId;
import br.gov.stf.estf.entidade.processostf.Andamento;
import br.gov.stf.estf.entidade.processostf.Andamento.Andamentos;
import br.gov.stf.estf.entidade.processostf.AndamentoProcesso;
import br.gov.stf.estf.entidade.processostf.Categoria;
import br.gov.stf.estf.entidade.processostf.Classe;
import br.gov.stf.estf.entidade.processostf.DeslocaProcesso;
import br.gov.stf.estf.entidade.processostf.IncidenteJulgamento;
import br.gov.stf.estf.entidade.processostf.ListaProcessos;
import br.gov.stf.estf.entidade.processostf.ModeloComunicacaoEnum;
import br.gov.stf.estf.entidade.processostf.ObjetoIncidente;
import br.gov.stf.estf.entidade.processostf.OrigemAndamentoDecisao;
import br.gov.stf.estf.entidade.processostf.OrigemAndamentoDecisao.ConstanteOrigemDecisao;
import br.gov.stf.estf.entidade.processostf.Parte;
import br.gov.stf.estf.entidade.processostf.PreListaJulgamento;
import br.gov.stf.estf.entidade.processostf.PreListaJulgamentoObjetoIncidente;
import br.gov.stf.estf.entidade.processostf.Processo;
import br.gov.stf.estf.entidade.processostf.ProcessoDependencia;
import br.gov.stf.estf.entidade.processostf.RecursoProcesso;
import br.gov.stf.estf.entidade.processostf.SituacaoMinistroProcesso;
import br.gov.stf.estf.entidade.processostf.TipoMeioProcesso;
import br.gov.stf.estf.entidade.processostf.TipoObjetoIncidente;
import br.gov.stf.estf.entidade.processostf.TipoRecurso;
import br.gov.stf.estf.entidade.processostf.TipoVinculoObjeto;
import br.gov.stf.estf.entidade.processostf.VinculoObjeto;
import br.gov.stf.estf.entidade.processostf.enuns.SituacaoIncidenteJulgadoOuNao;
import br.gov.stf.estf.entidade.publicacao.EstruturaPublicacao;
import br.gov.stf.estf.entidade.publicacao.ProcessoPublicado;
import br.gov.stf.estf.entidade.util.ObjetoIncidenteUtil;
import br.gov.stf.estf.intimacao.model.service.TipoComunicacaoLocalService;
import br.gov.stf.estf.julgamento.model.service.EnvolvidoService;
import br.gov.stf.estf.julgamento.model.service.InformacaoPautaProcessoService;
import br.gov.stf.estf.julgamento.model.service.JulgamentoProcessoService;
import br.gov.stf.estf.julgamento.model.service.ListaJulgamentoService;
import br.gov.stf.estf.julgamento.model.service.PrevisaoSustentacaoOralService;
import br.gov.stf.estf.julgamento.model.service.ProcessoListaJulgamentoService;
import br.gov.stf.estf.julgamento.model.service.SessaoService;
import br.gov.stf.estf.jurisdicionado.model.service.JurisdicionadoService;
import br.gov.stf.estf.localizacao.model.service.SetorService;
import br.gov.stf.estf.ministro.model.service.MinistroPresidenteService;
import br.gov.stf.estf.ministro.model.service.MinistroService;
import br.gov.stf.estf.processosetor.model.service.ProcessoSetorService;
import br.gov.stf.estf.processostf.model.service.AgendamentoService;
import br.gov.stf.estf.processostf.model.service.AndamentoProcessoService;
import br.gov.stf.estf.processostf.model.service.DeslocaProcessoService;
import br.gov.stf.estf.processostf.model.service.ListaProcessosService;
import br.gov.stf.estf.processostf.model.service.OrigemAndamentoDecisaoService;
import br.gov.stf.estf.processostf.model.service.PreListaJulgamentoService;
import br.gov.stf.estf.processostf.model.service.ProcessoDependenciaService;
import br.gov.stf.estf.processostf.model.service.ProcessoService;
import br.gov.stf.estf.processostf.model.service.SituacaoMinistroProcessoService;
import br.gov.stf.estf.processostf.model.service.TipoRecursoService;
import br.gov.stf.estf.processostf.model.service.VinculoObjetoService;
import br.gov.stf.estf.processostf.model.service.exception.AgendamentoNaoDefinidoException;
import br.gov.stf.estf.publicacao.model.service.ConteudoPublicacaoService;
import br.gov.stf.estf.publicacao.model.service.ProcessoPublicadoService;
import br.gov.stf.estf.repercussaogeral.model.service.RepercussaoGeralService;
import br.gov.stf.estf.util.DataUtil;
import br.gov.stf.framework.model.service.ServiceException;
import br.jus.stf.estf.decisao.comunicacao.service.ComunicacaoServicLocalImpl;
import br.jus.stf.estf.decisao.objetoincidente.service.ObjetoIncidenteService;
import br.jus.stf.estf.decisao.objetoincidente.support.AgendamentoNaoPodeSerCanceladoException;
import br.jus.stf.estf.decisao.objetoincidente.support.ConsultaDeAgendamentoVO;
import br.jus.stf.estf.decisao.objetoincidente.support.ConsultaDeConteudoPublicacaoVO;
import br.jus.stf.estf.decisao.objetoincidente.support.DadosAgendamentoDto;
import br.jus.stf.estf.decisao.objetoincidente.support.ProcessoNaoPodeSerAgendadoException;
import br.jus.stf.estf.decisao.objetoincidente.support.ProcessoPautaDeJulgamentoAdapter;
import br.jus.stf.estf.decisao.objetoincidente.support.ProcessoPrecisaDeConfirmacaoException;
import br.jus.stf.estf.decisao.objetoincidente.support.ProcessoTipoRecursoPodePlanarioVirtualException;
import br.jus.stf.estf.decisao.objetoincidente.support.TipoAgendamento;
import br.jus.stf.estf.decisao.objetoincidente.support.TipoColegiadoAgendamento;
import br.jus.stf.estf.decisao.objetoincidente.support.ValidacaoLiberacaoParaJulgamentoException;
import br.jus.stf.estf.decisao.objetoincidente.web.CancelarLiberacaoParaJulgamentoActionFacesBean;
import br.jus.stf.estf.decisao.objetoincidente.web.LiberarParaJulgamentoActionFacesBean.AdvogadoSustentacaoOral;
import br.jus.stf.estf.decisao.objetoincidente.web.LiberarParaJulgamentoActionFacesBean.PrevisaoSustentacaoOralDto;
import br.jus.stf.estf.decisao.pesquisa.domain.ObjetoIncidenteDto;
import br.jus.stf.estf.decisao.support.security.Principal;
import br.jus.stf.estf.decisao.support.service.ConfiguracaoSistemaService;
import br.jus.stf.estf.decisao.support.util.ListaJulgamentoUI;
import br.jus.stf.estf.decisao.support.util.NestedRuntimeException;
import br.jus.stf.estf.decisao.texto.support.DeslocamentoNaoEncontradoException;
import br.jus.stf.estf.decisao.texto.support.ErroAoDeslocarProcessoException;
import br.jus.stf.estf.decisao.texto.support.ErroNoSetorDoProcessoException;
import br.jus.stf.estf.decisao.texto.support.SetorInativoException;

/**
 * Implementa��o default para interface {@link ObjetoIncidenteService}.
 * 
 * @author Rodrigo Barreiros
 * @see 05.05.2010
 */
@Service("objetoIncidenteServiceLocal")
public class ObjetoIncidenteServiceImpl implements ObjetoIncidenteService {	
	private static final String SIGLA_QO = "QO";
	private static final String MSG_PROCESSO_LIBERADO_OUTRO_COLEGIADO = "Processo est� liberado para julgamento em outro colegiado.";
	private static final String MSG_PROCESSO_LIBERADO_OUTRA_MATERIA   = "Processo est� liberado para julgamento em outra mat�ria.";
	private static final String MSG_PROCESSO_OUTRA_SESSAO_PREVISTA    = "Processo est� associado a uma sess�o prevista.";
	private static final String MSG_LISTA_DE_JULGAMENTO_PREVISTA      = "Processo faz parte de lista de julgamento prevista.";
	private static final String MSG_TIPO_RECURSO_PLENARIO_VIRTUAL     = "Somente poder�o ser julgados virtualmente listas com os tipos de recursos: Agravo Regimental (Agr) e Embargo de Declara��o (ED).";
	private static final String MSG_PROCESSO_SEM_RELATORIO            = "O texto de relat�rio ainda n�o foi elaborado e n�o poder� ser liberado para julgamento.";
	private static final String MSG_MIN_NAO_RELATOR                   = "Este Ministro N�O � relator deste processo.";
	private static final String MSG_ERRO_SESSAO_NAO_CARREGADA         = "Erro ao Carregar sess�o. Informe ao STI";
	private static final String MSG_PROCESSO_SEM_EMENTA_RELATORIO_OU_VOTO = "Processo sem ementa, relat�rio ou voto revisados.";
	public static final String  MSG_ERRO_PAUTA_NAO_CONFIRMADA         = "Pauta n�o confirmada! Favor entrar em contato com a Secretaria de Sess�es.";
	public static final String  MSG_ERRO_ALTERAR_LISTA_JULGAMENTO_VIRTUAL_INICIO_SESSAO = "Ap�s in�cio da sess�o de julgamento virtual a lista n�o poder� ser alterada/excluida";
	public static final String MSG_COMUNICAO_REQUISAO_PROCESSO = "Requisi��o de Processo";
	/**
	 * Classes de processo que n�o precisam de relat�rio para serem agendadas
	 * Alterado conforme issue DECISAO-1375 e Regimento Interno
	 * Se��o III
	 * DO REVISOR
     * Art. 23. H� revis�o nos seguintes processos:
     *
     * I - a��o rescis�ria; AR
     * II - revis�o criminal; RvC
     * III - a��o penal origin�ria prevista no art. 5�, I e II; AP
     * IV - recurso ordin�rio criminal previsto no art. 6�, III, RCR
     * V - declara��o de suspens�o de direitos do art. 5�, VI, SD
	 */
	private static Set<String> classesDeProcessoSemRelatorio = new HashSet<String>(Arrays.asList(
			 Classe.SIGLA_ACAO_ORIGINARIA
			,Classe.SIGLA_ACAO_PENAL
			,Classe.SIGLA_ACAO_RESCISORIA
			,Classe.SIGLA_REVISAO_CRIMINAL
			,Classe.SIGLA_RECURSO_ORDINARIO_CRIMINAL
			,Classe.SIGLA_DECLARACAO_DE_SUSPENSAO_DE_DIREITOS
			,Classe.SIGLA_RECURSO_CRIME
			));
	
	private static List<Andamentos> andamentosQueImpedemDeslocamentoAutomatico = Arrays.asList(
			Andamentos.BAIXA_EM_DILIGENCIA,
			Andamentos.REMESSA_EXTERNA_DOS_AUTOS,
			Andamentos.BAIXA_AO_ARQUIVO_DO_STF_GUIA_NO,
			Andamentos.BAIXA_DEFINITIVA_DOS_AUTOS,
			Andamentos.REMESSA_AO_JUIZO_COMPETENTE,
			Andamentos.AUTOS_DEVOLVIDOS_A_ORIGEM);

	@Autowired
	private br.gov.stf.estf.processostf.model.service.ObjetoIncidenteService objetoIncidenteService;

	@Autowired
	private ListaProcessosService listaProcessosService;
	
	@Autowired
	private PreListaJulgamentoService preListaJulgamentoService;
	
	@Autowired
	private ConfiguracaoSistemaService configuracaoSistemaService;

	@Autowired
	private RepercussaoGeralService repercussaoGeralService;

	@Autowired
	private MinistroService ministroService;

	@Autowired
	private ProcessoPublicadoService processoPublicadoService;

	@Autowired
	private AgendamentoService agendamentoService;

	@Autowired
	private TextoService textoService;

	@Autowired
	private MinistroPresidenteService ministroPresidenteService;

	@Autowired
	private ConteudoPublicacaoService conteudoPublicacaoService;

	@Autowired
	private SetorService setorService;

	@Autowired
	private AndamentoProcessoService andamentoProcessoService;
	
	@Autowired
	private DeslocaProcessoService deslocaProcessoService;
	
	@Autowired
	private ProcessoSetorService processoSetorService;
	
	@Autowired
	private TipoRecursoService tipoRecursoService;
	
	@Autowired
	private PrevisaoSustentacaoOralService previsaoSustentacaoOralService;
	
	@Autowired
	private EnvolvidoService envolvidoService;
	
	@Autowired
	private JurisdicionadoService jurisdicionadoService;
	
	@Autowired
	private SituacaoMinistroProcessoService situacaoMinistroProcessoService;
	
	@Autowired
	private InformacaoPautaProcessoService informacaoPautaProcessoService;
	
	@Autowired
	private VinculoObjetoService vinculoObjetoService;
	
	@Autowired
	private ListaJulgamentoService listaJulgamentoService;
	
	@Autowired
	private ProcessoListaJulgamentoService processoListaJulgamentoService;
	
	@Autowired
	private JulgamentoProcessoService julgamentoProcessoService;
	
	@Autowired
	private SessaoService sessaoService;
	
	@Autowired
	private ProcessoDependenciaService processoDependenciaService;
	
	@Autowired
	private ProcessoService processoService;
	
	@Autowired
	private ComunicacaoServicLocalImpl comunicacaoService;
	
	@Autowired
	private TipoComunicacaoLocalService tipoComunicacaoLocalService;
	
	@Autowired
	private OrigemAndamentoDecisaoService origemAndamentoDecisaoService;
	
	private DataUtil dataUtil  = new DataUtil();
	
	/**
	 * @see br.jus.stf.estf.decisao.objetoincidente.service.ObjetoIncidenteService#pesquisarListasIncidentes(java.lang.String)
	 */
	@Override
	public List<ListaProcessos> pesquisarListasIncidentes(String nome) {
		Principal principal = (Principal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		try {
			return principal.getMinistro() != null ? listaProcessosService.pesquisarListaProcessos(nome, true, principal.getMinistro().getSetor().getId()) : null;
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}

	/**
	 * @see br.jus.stf.estf.decisao.objetoincidente.service.ObjetoIncidenteService#recuperarObjetoIncidentePorId(java.lang.Long)
	 */
	@Override
	public ObjetoIncidente<?> recuperarObjetoIncidentePorId(Long id) {
		try {
			return objetoIncidenteService.recuperarPorId(id);
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}

	public String recuperarDescricaoRepercussaoGeral(ObjetoIncidente<?> objetoIncidente) {
		try {
			Processo processoPrincipal = (Processo) objetoIncidente.getPrincipal();
			JulgamentoProcesso julgamento = repercussaoGeralService.recuperarJulgamentoRepercussaGeral(processoPrincipal);
			if (julgamento != null) {
				return " - " + julgamento.getDescricaoSituacaoAtual();
			}
			return "";
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}

	private void validarProcessoParaPauta(Ministro ministroDoGabinete, ObjetoIncidente<?> objetoIncidente) throws ProcessoPrecisaDeConfirmacaoException {
		try {
			verificaRelatoriaDoProcesso(objetoIncidente, ministroDoGabinete);
			verificaSeProcessoEstaEmPauta(objetoIncidente);
			verificaSeProcessoEstaAgendadoPauta(objetoIncidente);
			//Comentado em fun��o do DECISAO-2250
			//verificaNecessidadeDePauta(objetoIncidente);
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}

	}

	/**
	 * Para dado objetoIncidente verifica primeiro se TEM tipo de recurso
	 * e pode ser pautado, ver tabela JUDICIARIO.TIPO_RECURSO.
	 * Se n�o tem tipo de recurso verifica se a classe pode ser pautada,
	 * ver tabela JUDICIARIO.CLASSE.
	 * @param objetoIncidente
	 * @return 
	 * @throws ServiceException
	 */
	private Boolean isProcessoPrecisaDePauta(ObjetoIncidente<?> objetoIncidente) throws ServiceException {
		TipoRecurso tipoRecurso = this.ObjetoIncidenteUtilGetTipoRecurso(objetoIncidente);
		if (tipoRecurso == null) {
			Processo processo = this.objetoIncidenteUtilGetProcesso(objetoIncidente);
			Classe classe = processo.getClasseProcessual();
			return classe.getAdmitePautaJulgamento();
		}
		return tipoRecurso.getAdmitePautaJulgamento();
	}

	private void verificaSeProcessoEstaAgendadoPauta(ObjetoIncidente<?> processo) throws ServiceException
																						,ProcessoPrecisaDeConfirmacaoException {
		List<Agendamento> agendamentos = consultaAgendamentosDoProcesso(processo,Agendamento.COD_MATERIA_AGENDAMENTO_PAUTA);
		if (!agendamentos.isEmpty()) {
			Agendamento agendamento = agendamentos.get(0);
			String mensagem = montaMensagemDeProcessoAgendadoPauta(agendamento);
			throw new ProcessoPrecisaDeConfirmacaoException(mensagem);
		}

	}

	private List<Agendamento> consultaAgendamentosDoProcesso(ObjetoIncidente<?> objetoIncidente, Integer codigoDaMateria)
			throws ServiceException {
		ProcessoPautaDeJulgamentoAdapter consulta = new ProcessoPautaDeJulgamentoAdapter(objetoIncidente);
		consulta.setCodigoDaMateria(codigoDaMateria);
		// S� inclui o tipo de julgamento se for �ndice, pois para pauta
		// sempre � m�rito
		if (codigoDaMateria.equals(Agendamento.COD_MATERIA_AGENDAMENTO_JULGAMENTO)) {
			if (objetoIncidente.getTipoObjetoIncidente().equals(TipoObjetoIncidente.INCIDENTE_JULGAMENTO)) {
				TipoRecurso tipoJulgamento = this.ObjetoIncidenteUtilGetTipoRecurso(objetoIncidente);
				consulta.setTipoJulgamento(tipoJulgamento.getId());
			}
		}
		return agendamentoService.consultaAgendamentosParaPauta(consulta);
	}

	private String montaMensagemDeProcessoAgendadoPauta(Agendamento agendamento) {
		int idAgendamento = agendamento.getId().getCodigoMateria().intValue();
		String capituloDoProcesso = getCapituloDoProcesso(idAgendamento);
		return "Processo j� foi liberado para a pauta "+ capituloDoProcesso;
	}

	private void verificaSeProcessoEstaEmPauta(ObjetoIncidente<?> objetoIncidente) throws ServiceException
																						 ,ProcessoPrecisaDeConfirmacaoException {
		List<ProcessoPublicado> processosPublicados = consultaDadosParaPautaDeJulgamento(objetoIncidente);
		if (!processosPublicados.isEmpty()) {
			ProcessoPublicado processoPublicado = processosPublicados.get(0);
			String montaMensagemDeProcessoEmPauta = montaMensagemDeProcessoEmPauta(processoPublicado);
			throw new ProcessoPrecisaDeConfirmacaoException(montaMensagemDeProcessoEmPauta);
		}
	}

	private String montaMensagemDeProcessoEmPauta(ProcessoPublicado processoPublicado) {
		Integer numeroMateria = processoPublicado.getNumeroMateria();
		Short anoMateria = processoPublicado.getAnoMateria();
		Integer codigoMateria = processoPublicado.getCodigoMateria();
		String capituloDoProcesso = getCapituloDoProcesso(codigoMateria);
		
		String mensgem = String.format("Processo na pauta n� %s/%s %s.", numeroMateria, anoMateria, capituloDoProcesso);
		return mensgem;
	}

	private String getCapituloDoProcesso(Integer codigoDaMateria) {
		if (codigoDaMateria.equals(EstruturaPublicacao.COD_CAPITULO_PLENARIO)) {
			return "do Plen�rio.";
		} else if (codigoDaMateria.equals(EstruturaPublicacao.COD_CAPITULO_PRIMEIRA_TURMA)) {
			return "da 1� Turma.";
		} else if (codigoDaMateria.equals(EstruturaPublicacao.COD_CAPITULO_SEGUNDA_TURMA)) {
			return "da 2� Turma.";
		}
		return "Cap�tulo n�o identificado!";

	}

	private List<ProcessoPublicado> consultaDadosParaPautaDeJulgamento(ObjetoIncidente<?> objetoIncidente)
			throws ServiceException {

		ProcessoPautaDeJulgamentoAdapter consulta = new ProcessoPautaDeJulgamentoAdapter(objetoIncidente);
		consulta.setCodigoDaMateria(EstruturaPublicacao.COD_MATERIA_PAUTA_JULGAMENTO);
		return processoPublicadoService.pesquisarProcessoEmPautaDeJulgamento(consulta);
	}
	
	private void verificaRelatoriaDoProcesso(ObjetoIncidente<?> processo, Ministro ministroDoGabinete)	throws ServiceException
																											 , ProcessoPrecisaDeConfirmacaoException {
		boolean isMinistroRelatorIncluindoPresidencia = this.isMinistroRelatorIncluindoPresidencia(processo, ministroDoGabinete);
		if (!isMinistroRelatorIncluindoPresidencia) {
			throw new ProcessoPrecisaDeConfirmacaoException(MSG_MIN_NAO_RELATOR);
		}
	}

	private boolean isMinistroRelatorIncluindoPresidencia(ObjetoIncidente<?> objetoIncidente, Ministro ministroDoGabinete) throws ServiceException {
		// TODO: Verificar quem � o relator do processo: O relator do m�rito, ou do incidente?

		//Solucao Contorno do problema Javassist  DECISAO-2483
		Hibernate.initialize(objetoIncidente.getPrincipal());
		
		Processo processo = (Processo) objetoIncidente.getPrincipal();
		
		//Solucao Contorno do problema Javassist  DECISAO-2483
		Hibernate.initialize(processo.getMinistroRelatorAtual());
		
		// DECISAO-2654
		Hibernate.initialize(ministroDoGabinete);
		
		boolean isMinistroRelator = isMinistroRelator(processo, ministroDoGabinete);
		boolean ministroTemRelatoriaDaPresidencia = ministroService.isMinistroTemRelatoriaDaPresidencia(ministroDoGabinete, processo);
		return isMinistroRelator || ministroTemRelatoriaDaPresidencia;
	}

	private boolean isMinistroRelator(Processo processo, Ministro ministroDoGabinete) throws ServiceException {
		return ministroService.isMinistroRelatorDoProcesso(ministroDoGabinete, processo);
	}

	public void validarProcessoParaAgendamento(DadosAgendamentoDto dadosAgendamento) throws ProcessoPrecisaDeConfirmacaoException
																						  , ProcessoNaoPodeSerAgendadoException {
		Long idObjetoIncidente          = dadosAgendamento.getObjetoIncidenteDto().getId();
		Ministro ministroDoGabinete     = dadosAgendamento.getMinistro();
		TipoAgendamento tipoAgendamento = dadosAgendamento.getTipoAgendamento();
		boolean pautaDirigida           = dadosAgendamento.isPautaDirigida();

		if (ministroDoGabinete != null) {
			try {
				ministroDoGabinete = ministroService.recuperarPorId(ministroDoGabinete.getId());
			} catch (ServiceException e) {
				throw new NestedRuntimeException(e);
			}
		}
		
		ObjetoIncidente<?> objetoIncidente = recuperarObjetoIncidentePorId(idObjetoIncidente);
		verificaExigenciaDeRelatorioVoto(objetoIncidente, ministroDoGabinete, pautaDirigida);
		
		if (isPauta(tipoAgendamento)) {
			validarProcessoParaPauta(ministroDoGabinete, objetoIncidente);
		} else if (isIndice(tipoAgendamento)) {
			validarDadosParaIndice(ministroDoGabinete, objetoIncidente);
		}
	}

	@Override
	public void verificaExigenciaDeRelatorioVoto(ObjetoIncidente<?> objetoIncidente
												,Ministro ministroDoGabinete
												,boolean isPautaDirigida) throws ProcessoNaoPodeSerAgendadoException {
		try {
			Processo processo = this.objetoIncidenteUtilGetProcesso(objetoIncidente);
			boolean isRelatorioGerado = isRelatorioGerado(objetoIncidente, ministroService.recuperarMinistroRelatorIncidente(objetoIncidente));
			if (isPautaDirigida) {
				verificaVotoGerado(ministroDoGabinete, objetoIncidente, isRelatorioGerado);
			} else {
				if (!isRelatorioGerado && processoExigeRelatorio(processo)) {
					throw new ProcessoNaoPodeSerAgendadoException(MSG_PROCESSO_SEM_RELATORIO);
				}
			}
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}

	private boolean processoExigeRelatorio(ObjetoIncidente<?> objetoIncidente) {
		Processo processo = this.objetoIncidenteUtilGetProcesso(objetoIncidente);
		return !classesDeProcessoSemRelatorio.contains(processo.getClasseProcessual().getId());
	}

	private void verificaVotoGerado(Ministro ministroDoGabinete, ObjetoIncidente<?> processo, boolean isRelatorioGerado)
			throws ServiceException, ProcessoNaoPodeSerAgendadoException {
		boolean isVotoGerado = isVotoGerado(processo, ministroDoGabinete);
		if (!(isRelatorioGerado && isVotoGerado)) {
			defineTextoQueEstaFaltando(isRelatorioGerado, isVotoGerado);
		}
	}

	private void defineTextoQueEstaFaltando(boolean isRelatorioGerado, boolean isVotoGerado)
			throws ProcessoNaoPodeSerAgendadoException {
		if (isRelatorioGerado) {
			if (!isVotoGerado) {
				throw new ProcessoNaoPodeSerAgendadoException(
						"O texto de voto ainda n�o foi elaborado e n�o poder� ser agendado para pauta dirigida.");
			}
		} else {
			if (isVotoGerado) {
				throw new ProcessoNaoPodeSerAgendadoException(
						"O texto de relat�rio ainda n�o foi elaborado e n�o poder� ser agendado para pauta dirigida.");
			} else {
				throw new ProcessoNaoPodeSerAgendadoException(
						"Os textos de relat�rio e voto ainda n�o foram elaborados e n�o poder�o ser agendados para pauta dirigida.");
			}
		}
	}

	private boolean isVotoGerado(ObjetoIncidente<?> processo, Ministro ministroDoGabinete) throws ServiceException {
		List<Texto> textos = consultarVotos(processo, ministroDoGabinete);
		return !textos.isEmpty();
	}

	private List<Texto> consultarVotos(ObjetoIncidente<?> processo, Ministro ministroDoGabinete)
			throws ServiceException {
		ConsultaDadosDoTextoVO consulta = montaDadosBasicosDeConsultaDeTexto(processo, ministroDoGabinete);
		consulta.setTipoDeTexto(TipoTexto.VOTO);
		return textoService.pesquisar(consulta);
	}

	private boolean isRelatorioGerado(ObjetoIncidente<?> processo, Ministro ministroDoGabinete) throws ServiceException {
		List<Texto> textos = consultarRelatorioDoProcesso(processo, ministroDoGabinete);
		return !textos.isEmpty();
	}

	private List<Texto> consultarRelatorioDoProcesso(ObjetoIncidente<?> processo, Ministro ministroDoGabinete)
			throws ServiceException {
		ConsultaDadosDoTextoVO consulta = montaConsultaDeTexto(processo, ministroDoGabinete, TipoTexto.RELATORIO);
		return textoService.pesquisar(consulta);
	}

	private ConsultaDadosDoTextoVO montaConsultaDeTexto(ObjetoIncidente<?> processo, Ministro ministroDoGabinete, TipoTexto tipoTexto)
			throws ServiceException {
		ConsultaDadosDoTextoVO consulta = montaDadosBasicosDeConsultaDeTexto(processo, ministroDoGabinete);
		consulta.setIncluirPresidencia(isMinistroPresidente(ministroDoGabinete));
		consulta.setIncluirVicePresidencia(isMinistroVicePresidente(ministroDoGabinete));
		consulta.setTipoDeTexto(tipoTexto);
		return consulta;
	}
	
	private ConsultaDadosDoTextoVO montaDadosBasicosDeConsultaDeTexto(ObjetoIncidente<?> objetoIncidente,
			Ministro ministroDoGabinete) {
		ConsultaDadosDoTextoVO consulta = new ConsultaDadosDoTextoVO();
		if (ministroDoGabinete !=null) {
			consulta.setCodigoDoMinistro(ministroDoGabinete.getId());
		}
		consulta.setSequencialObjetoIncidente(objetoIncidente.getId());
		return consulta;
	}

	private boolean isIndice(TipoAgendamento tipoAgendamento) {
		return TipoAgendamento.INDICE.equals(tipoAgendamento);
	}

	private boolean isPauta(TipoAgendamento tipoAgendamento) {
		boolean isPauta = TipoAgendamento.PAUTA.equals(tipoAgendamento);
		return isPauta;
	}

	private void validarDadosParaIndice(Ministro ministroDoGabinete, ObjetoIncidente<?> objetoIncidente)
			throws ProcessoPrecisaDeConfirmacaoException {
		try {
			verificaNecessidadeDePautaParaIndice(objetoIncidente);
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}

	private boolean isMinistroVicePresidente(Ministro ministroDoGabinete) throws ServiceException {
		return ministroPresidenteService.getMinistroVicePresidente().getId().getMinistro().getId()
				.equals(ministroDoGabinete.getId());
	}

	private boolean isMinistroPresidente(Ministro ministroDoGabinete) throws ServiceException {
		return ministroPresidenteService.getMinistroPresidente().getId().getMinistro().getId()
				.equals(ministroDoGabinete.getId());
	}

	/*
	 * private void verificaPedidoDeVistas(ObjetoIncidente<?> processo, Ministro
	 * ministroDoGabinete) throws ServiceException {
	 * getMapaDeVistas().put(processo.getId(),
	 * !isMinistroRelatorIncluindoPresidencia(processo, ministroDoGabinete)); }
	 */

	private void verificaNecessidadeDePautaParaIndice(ObjetoIncidente<?> objetoIncidente) throws ServiceException,
																								 ProcessoPrecisaDeConfirmacaoException {
		Boolean isProcessoPrecisaDePauta = isProcessoPrecisaDePauta(objetoIncidente);
		if (isProcessoPrecisaDePauta != null && isProcessoPrecisaDePauta) {
			List<Agendamento> agendamentos = consultaAgendamentosDoProcesso(objetoIncidente,Agendamento.COD_MATERIA_AGENDAMENTO_JULGAMENTO);
			if (agendamentos.isEmpty() && isProcessoPrecisaDePauta && isPautaDeveEstarPublicada(objetoIncidente)) {
				List<ProcessoPublicado> processosPublicados = consultaDadosParaPautaDeJulgamento(objetoIncidente);
				if (processosPublicados.isEmpty()) {
					throw new ProcessoPrecisaDeConfirmacaoException("Este processo tem que ser liberado para pauta.");
				}
			}
		}
	}

	/**
	* Verifica se a pauta deve estar publicada. Isso ocorre em duas situa��es:
	* 1 - A classe do processo n�o � uma ADI 
	* 2 - A classe do processo � uma ADI, e o tipo de julgamento n�o � QO, QO2 e QO3
	* 
	* @param objetoIncidente
	* @return
	*/
	private boolean isPautaDeveEstarPublicada(ObjetoIncidente<?> objetoIncidente) {
		Processo processo = this.objetoIncidenteUtilGetProcesso(objetoIncidente);
		TipoRecurso tipoRecurso = this.ObjetoIncidenteUtilGetTipoRecurso(objetoIncidente);
		return ((!processo.getClasseProcessual().getId().equals(Classe.SIGLA_ACAO_DIRETA_DE_INCONSTITUCIONALIDADE) || (isTipoDeJulgamentoObrigaPublicacao(tipoRecurso))));
	}

	/**
	 * A Quest�o de Ordem obriga a publica��o.
	 * @param tipoJulgamento
	 * @return
	 */
	private boolean isTipoDeJulgamentoObrigaPublicacao(TipoRecurso tipoJulgamento) {
		return tipoJulgamento != null && SIGLA_QO.equals(tipoJulgamento.getSigla());
	}

	public void salvarAgendamentoProcesso(DadosAgendamentoDto dadosAgendamento)
			throws ProcessoNaoPodeSerAgendadoException {
		try {
			ObjetoIncidente<?> processo = recuperarObjetoIncidentePorId(dadosAgendamento.getObjetoIncidenteDto()
					.getId());
			Ministro ministroDoGabinete = dadosAgendamento.getMinistro();
			Integer codigoColegiado = defineCapituloParaVerificacaoDaPauta(dadosAgendamento);
			if (isPauta(dadosAgendamento.getTipoAgendamento())) {
				verificaFechamentoDaPauta(ministroDoGabinete, codigoColegiado);
			}
			gravaAgendamento(dadosAgendamento, processo, codigoColegiado);
			liberarRelatorioParaSalaDeJulgamento(ministroDoGabinete, processo);
			liberarVotosParaSalaDeJulgamento(dadosAgendamento, processo);
			liberarTextosDoMinistroRelator(ministroDoGabinete, processo);
			
			if (dadosAgendamento.getJulgamentoConjunto() != null) {
				for (ObjetoIncidenteDto dto : dadosAgendamento.getJulgamentoConjunto()) {
					ObjetoIncidente<?> oi = recuperarObjetoIncidentePorId(dto.getId());
					gravaAgendamento(dadosAgendamento, oi, codigoColegiado);
					liberarRelatorioParaSalaDeJulgamento(ministroDoGabinete, oi);
					liberarVotosParaSalaDeJulgamento(dadosAgendamento, oi);
					liberarTextosDoMinistroRelator(ministroDoGabinete, oi);
				}
			}
			InformacaoPautaProcesso informacaoPautaProcesso = informacaoPautaProcessoService.recuperar(processo);
			informacaoPautaProcesso = alterarInformacaoPautaProcesso(informacaoPautaProcesso, dadosAgendamento);
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}

	}

	private InformacaoPautaProcesso alterarInformacaoPautaProcesso(
		InformacaoPautaProcesso informacaoPautaProcesso,
		DadosAgendamentoDto dadosAgendamento) throws ServiceException {
		if (informacaoPautaProcesso != null) {
			informacaoPautaProcesso = informacaoPautaProcessoService.recuperarPorId(informacaoPautaProcesso.getId());
		} else {
			ObjetoIncidente<?> objetoIncidente = recuperarObjetoIncidentePorId(dadosAgendamento.getObjetoIncidenteDto()
					.getId());
			informacaoPautaProcesso = new InformacaoPautaProcesso();
			informacaoPautaProcesso.setObjetoIncidente(objetoIncidente);
		}
		
		informacaoPautaProcesso.setObservacao(dadosAgendamento.getObservacao());
		informacaoPautaProcesso.setRepercussaoGeral(dadosAgendamento.isRepercussaoGeral());
		informacaoPautaProcesso.setPautaExtra(dadosAgendamento.isPautaExtra());
		informacaoPautaProcesso.setDataJulgamentoSugerida(dadosAgendamento.getDataJulgamentoSugerida());
		informacaoPautaProcesso.setObservacaoDataSugerida(dadosAgendamento.getObservacaoDataJulgamento());
		informacaoPautaProcesso.setDataLiberacaoGabinete(new Date());
		informacaoPautaProcesso.setSituacaoPauta(TipoSituacaoPauta.P);
		informacaoPautaProcesso.setMateriaRelevante(Boolean.FALSE);
		informacaoPautaProcesso.setAguardaLiberacao(Boolean.FALSE);
		
		informacaoPautaProcesso = informacaoPautaProcessoService.salvar(informacaoPautaProcesso);
		
		informacaoPautaProcesso = gravarSustentacoesOrais(informacaoPautaProcesso, dadosAgendamento.getSustentacoesOrais());
		
		informacaoPautaProcesso = atualizarProcessosVinculados(informacaoPautaProcesso, dadosAgendamento);
		
		return informacaoPautaProcesso;
	}

	/**
	 * @param informacaoPautaProcesso
	 * @param dadosAgendamento
	 * @return
	 * @throws ServiceException
	 */
	private InformacaoPautaProcesso atualizarProcessosVinculados(
			InformacaoPautaProcesso informacaoPautaProcesso,
			DadosAgendamentoDto dadosAgendamento) throws ServiceException {
		List<Long> listasJulgamentoConjunto = obterListasJulgamentoConjunto(dadosAgendamento, informacaoPautaProcesso);
		
		// Remove processos exclu�dos da lista de julgamento conjunto
		if (listasJulgamentoConjunto != null && listasJulgamentoConjunto.size() > 0) {
			informacaoPautaProcesso = removerProcessosListaJulgamentoConjunto(
					informacaoPautaProcesso,
					dadosAgendamento.getJulgamentoConjunto(),
					dadosAgendamento.getPrecedentes(), listasJulgamentoConjunto);
		}
		
		if (dadosAgendamento.getJulgamentoConjunto() != null && dadosAgendamento.getJulgamentoConjunto().size() > 0) {
			if (informacaoPautaProcesso.getSeqListaJulgamentoConjunto() == null) {
				if (listasJulgamentoConjunto.size() > 0) {
					informacaoPautaProcesso.setSeqListaJulgamentoConjunto(listasJulgamentoConjunto.get(0));
				} else {
					informacaoPautaProcesso.setSeqListaJulgamentoConjunto(informacaoPautaProcessoService.recuperarProximaSequenciaListaJulgamentoConjunto());
					listasJulgamentoConjunto.add(informacaoPautaProcesso.getSeqListaJulgamentoConjunto());
				}
				informacaoPautaProcesso = informacaoPautaProcessoService.salvar(informacaoPautaProcesso);
			}

			for (ObjetoIncidenteDto oi : dadosAgendamento.getJulgamentoConjunto()) {
				ObjetoIncidente<?> objetoIncidente = recuperarObjetoIncidentePorId(oi.getId());
				InformacaoPautaProcesso ipp = informacaoPautaProcessoService.recuperar(objetoIncidente);
				if (ipp == null) {
					ipp = new InformacaoPautaProcesso();
					ipp.setObjetoIncidente(objetoIncidente);
				}
				ipp.setDataJulgamentoSugerida(informacaoPautaProcesso.getDataJulgamentoSugerida());
				ipp.setObservacaoDataSugerida(informacaoPautaProcesso.getObservacaoDataSugerida());
				ipp.setDataLiberacaoGabinete(new Date());
				ipp.setSeqListaJulgamentoConjunto(informacaoPautaProcesso.getSeqListaJulgamentoConjunto());
				ipp.setDataJulgamentoSugerida(informacaoPautaProcesso.getDataJulgamentoSugerida());
				ipp.setSituacaoPauta(TipoSituacaoPauta.P);
				ipp.setRepercussaoGeral(Boolean.valueOf(dadosAgendamento.isRepercussaoGeral()));
				ipp.setPautaExtra(Boolean.valueOf(dadosAgendamento.isPautaExtra()));
				ipp.setAguardaLiberacao(Boolean.FALSE);
				ipp.setMateriaRelevante(Boolean.FALSE);
				informacaoPautaProcessoService.salvar(ipp);
			}
		}
		
		// Atualizar lista de processos precedentes
		if ( dadosAgendamento.getPrecedentes() != null ) {
			for (ObjetoIncidenteDto dto : dadosAgendamento.getPrecedentes()) {
				ObjetoIncidente<?> vinculado = recuperarObjetoIncidentePorId(dto.getId());
				List<VinculoObjeto> listaVinculosObjetoGravados = vinculoObjetoService.pesquisarPorVinculado(vinculado, TipoVinculoObjeto.DEPENDE_DO_JULGAMENTO);
				List<ObjetoIncidente<?>> julgamentoConjunto = informacaoPautaProcessoService.recuperarProcessosJulgamentoConjunto(informacaoPautaProcesso.getObjetoIncidente(), true);
				vinculador: for (ObjetoIncidente<?> vinculador : julgamentoConjunto) {
					for (VinculoObjeto vinculo : listaVinculosObjetoGravados) {
						if (vinculo.getObjetoIncidenteVinculador().getId().equals(vinculador.getId())) {
							continue vinculador;
						}
					}
					VinculoObjeto novoVinculo = new VinculoObjeto();
					novoVinculo.setObjetoIncidenteVinculador(vinculador);
					novoVinculo.setObjetoIncidente(vinculado);
					novoVinculo.setTipoVinculoObjeto(TipoVinculoObjeto.DEPENDE_DO_JULGAMENTO);
					vinculoObjetoService.salvar(novoVinculo);
				}
			}
		}
		
		return informacaoPautaProcesso;
	}

	private InformacaoPautaProcesso removerProcessosListaJulgamentoConjunto(
			InformacaoPautaProcesso informacaoPautaProcesso,
			List<ObjetoIncidenteDto> julgamentoConjunto, List<ObjetoIncidenteDto> dependentes, List<Long> listasJulgamentoConjunto) throws ServiceException {
		for (Long listaJulgamentoConjunto : listasJulgamentoConjunto) {
			List<ObjetoIncidente<?>> paraRemover = new ArrayList<ObjetoIncidente<?>>();
			List<ObjetoIncidente<?>> processosJulgamentoConjunto = informacaoPautaProcessoService.recuperarProcessosJulgamentoConjunto(listaJulgamentoConjunto);
			for (ObjetoIncidente<?> oi : processosJulgamentoConjunto) {
				if (!julgamentoConjunto.contains(ObjetoIncidenteDto.valueOf(oi))) {
					paraRemover.add(oi);
				}
			}
			
			if (processosJulgamentoConjunto.size() == paraRemover.size() - 1) {
				processosJulgamentoConjunto.removeAll(paraRemover);
				InformacaoPautaProcesso ipp = informacaoPautaProcessoService.recuperar(processosJulgamentoConjunto.get(0));
				ipp.setSeqListaJulgamentoConjunto(null);
				informacaoPautaProcessoService.alterar(ipp);
			} else {
				List<InformacaoPautaProcesso> ipps = new ArrayList<InformacaoPautaProcesso>();
				for (ObjetoIncidente<?> oi : paraRemover) {
					InformacaoPautaProcesso ipp = informacaoPautaProcessoService.recuperar(oi);
					ipp.setSeqListaJulgamentoConjunto(null);
					ipps.add(ipp);
					List<VinculoObjeto> listaVinculosObjeto = vinculoObjetoService.pesquisarPorVinculador(oi, TipoVinculoObjeto.DEPENDE_DO_JULGAMENTO);
					vinculoObjetoService.excluirTodos(listaVinculosObjeto);
				}
				informacaoPautaProcessoService.alterarTodos(ipps);
			}
		}
		
		return informacaoPautaProcessoService.recuperar(informacaoPautaProcesso.getObjetoIncidente());
	}

	private List<Long> obterListasJulgamentoConjunto(
			DadosAgendamentoDto dadosAgendamento,
			InformacaoPautaProcesso informacaoPautaProcesso)
			throws ServiceException {
		List<Long> listasJulgamentoConjunto = new ArrayList<Long>();
		if (informacaoPautaProcesso.getSeqListaJulgamentoConjunto() != null && informacaoPautaProcesso.getSeqListaJulgamentoConjunto().intValue() > 0) {
			listasJulgamentoConjunto.add(informacaoPautaProcesso.getSeqListaJulgamentoConjunto());
		} else {
			if (dadosAgendamento.getJulgamentoConjunto() != null) {
				for (ObjetoIncidenteDto oi : dadosAgendamento.getJulgamentoConjunto()) {
					InformacaoPautaProcesso ipp = informacaoPautaProcessoService.recuperar(recuperarObjetoIncidentePorId(oi.getId()));
					if (ipp != null
							&& ipp.getSeqListaJulgamentoConjunto() != null
							&& ipp.getSeqListaJulgamentoConjunto().intValue() > 0
							&& !listasJulgamentoConjunto.contains(ipp.getSeqListaJulgamentoConjunto())) {
						listasJulgamentoConjunto.add(ipp.getSeqListaJulgamentoConjunto());
					}
				}
			}
		}
		return listasJulgamentoConjunto;
	}
	
	private void liberarTextosDoMinistroRelator(Ministro ministroDoGabinete, ObjetoIncidente<?> objetoIncidente)
			throws ServiceException {
		// Dem�trius Jub� - Essa funcionalidade n�o estava constando na fun��o
		// agendarProcesso do modFuncoes5, e estava somente na lista de
		// agendamento!
		Processo processo = this.objetoIncidenteUtilGetProcesso(objetoIncidente);
		if (isMinistroRelator(processo, ministroDoGabinete)) {
			List<Texto> textosSemRelatorioEVoto = consultaTextosSemRelatorioVoto(ministroDoGabinete, objetoIncidente);
			liberaTextosParaSalaDeJulgamento(textosSemRelatorioEVoto);
		}
	}

	private List<Texto> consultaTextosSemRelatorioVoto(Ministro ministroDoGabinete, ObjetoIncidente<?> processo)
			throws ServiceException {
		ConsultaDadosDoTextoVO consulta = montaDadosBasicosDeConsultaDeTexto(processo, ministroDoGabinete);
		consulta.setTiposDeTextoParaExcluir(TipoTexto.RELATORIO, TipoTexto.VOTO);
		return textoService.pesquisar(consulta);
	}

	private void liberarRelatorioParaSalaDeJulgamento(Ministro ministroDoGabinete, ObjetoIncidente<?> processo)
			throws ServiceException {
		List<Texto> relatorios = consultarRelatorioDoProcesso(processo, ministroDoGabinete);
		liberaTextosParaSalaDeJulgamento(relatorios);
	}

	private void liberarVotosParaSalaDeJulgamento(DadosAgendamentoDto dadosAgendamento, ObjetoIncidente<?> processo)
			throws ServiceException {
		if (dadosAgendamento.isPautaDirigida() && dadosAgendamento.isLiberarVoto()) {
			List<Texto> votos = consultarVotos(processo, dadosAgendamento.getMinistro());
			liberaTextosParaSalaDeJulgamento(votos);
		}
	}

	private void liberaTextosParaSalaDeJulgamento(List<Texto> textos) throws ServiceException {
		if (textos != null && textos.size() > 0) {
			for (Texto texto : textos) {
				texto.setSalaJulgamento(true);
			}
			textoService.alterarTodos(textos);
		}
	}

	private void gravaAgendamento(DadosAgendamentoDto dadosAgendamento, ObjetoIncidente<?> processo,
			Integer codigoColegiado) throws ServiceException {
		Agendamento agendamento;
		try {
			agendamento = consultaAgendamentoCadastrado(processo, dadosAgendamento.getMinistro());
			agendamento.setObservacaoProcesso(dadosAgendamento.getObservacao());
			agendamentoService.alterar(agendamento);
		} catch (AgendamentoNaoDefinidoException e) {
			agendamento = incluiAgendamentoNovo(dadosAgendamento, processo, codigoColegiado);
			AndamentoProcesso andamentoProcesso = geraAndamentoParaProcesso(dadosAgendamento, processo, codigoColegiado);
			
			andamentoProcesso = andamentoProcessoService.recuperarAndamentoProcesso(andamentoProcesso.getId());
			andamentoProcesso.setObjetoIncidente(processo);
			andamentoProcessoService.persistirAndamentoProcesso(andamentoProcesso);
		}
	}

	private AndamentoProcesso geraAndamentoParaProcesso(DadosAgendamentoDto dados, ObjetoIncidente<?> objetoIncidente,
			Integer codigoColegiado) throws ServiceException {
		Long numeroUltimoAndamento = andamentoProcessoService.recuperarUltimoNumeroSequencia(objetoIncidente);
		numeroUltimoAndamento++;
		Date dataAtual = new Date();
		AndamentoProcesso andamentoProcesso = new AndamentoProcesso();
		andamentoProcesso.setCodigoAndamento(defineCodigoDoAndamento(dados));
		andamentoProcesso.setObjetoIncidente(getProcessoOuRecurso(objetoIncidente));
		andamentoProcesso.setCodigoUsuario(dados.getUsuario().getId());
		andamentoProcesso.setDataAndamento(dataAtual);
		andamentoProcesso.setDataHoraSistema(dataAtual);
		String observacao = defineObservacao(codigoColegiado, dataAtual, objetoIncidente, dados.getSessao());
		andamentoProcesso.setDescricaoObservacaoAndamento(observacao);		
		andamentoProcesso.setNumeroSequencia(numeroUltimoAndamento);
		andamentoProcesso.setSetor(dados.getSetorDoUsuario());
		andamentoProcesso.setOrigemAndamentoDecisao(getOrigemAndamentoDecisao(dados, codigoColegiado));
		return andamentoProcessoService.incluir(andamentoProcesso);
	}

	private OrigemAndamentoDecisao getOrigemAndamentoDecisao(DadosAgendamentoDto dados, Integer codigoColegiado) throws ServiceException {
		String siglaColegiado = TipoColegiadoConstante.valueOfCodigoCapitulo(codigoColegiado).getSigla();;
		String siglaTipoAmbiente = "F"; // V: virtual, F: f�sico
		
		if (dados.getSessao() != null) {
			siglaColegiado = dados.getSessao().getColegiado().getId();
			siglaTipoAmbiente = dados.getSessao().getTipoAmbiente(); 
		}
		
		ConstanteOrigemDecisao constanteOrigemDecisao = ConstanteOrigemDecisao.valueOf(siglaColegiado, siglaTipoAmbiente);
		
		if (constanteOrigemDecisao != null)
			return origemAndamentoDecisaoService.recuperarPorId(constanteOrigemDecisao.getCodigo());
		
		return null;
	}

	private ObjetoIncidente<?> getProcessoOuRecurso(ObjetoIncidente<?> objetoIncidente) {
		ObjetoIncidente<?> processoRecurso = objetoIncidente;
		while(!processoRecurso.getTipoObjetoIncidente().equals(TipoObjetoIncidente.PROCESSO) && !processoRecurso.getTipoObjetoIncidente().equals(TipoObjetoIncidente.RECURSO)) {
			processoRecurso = processoRecurso.getPai();
		}
		
		return processoRecurso;
	}

	private String defineObservacao(Integer codigoColegiado, Date dataAtual, ObjetoIncidente<?> objetoIncidente, Sessao sessao) {
		String observacao = "";

		if (codigoColegiado.equals(EstruturaPublicacao.COD_CAPITULO_PLENARIO)) {
			observacao = "Pleno";
		} else if (codigoColegiado.equals(EstruturaPublicacao.COD_CAPITULO_PRIMEIRA_TURMA)) {
			observacao = "1� Turma";
		} else if (codigoColegiado.equals(EstruturaPublicacao.COD_CAPITULO_SEGUNDA_TURMA)) {
			observacao = "2� Turma";
		}

		String siglaCadeiaIncidente = "";
		if (objetoIncidente instanceof RecursoProcesso) {
			siglaCadeiaIncidente =  ((RecursoProcesso) objetoIncidente).getSiglaCadeiaIncidente();
		} else if (objetoIncidente instanceof IncidenteJulgamento) {
			siglaCadeiaIncidente =  ((IncidenteJulgamento) objetoIncidente).getSiglaCadeiaIncidente();
		}

		observacao += " em " + getDataHoraFormatada(dataAtual) + " - " + siglaCadeiaIncidente;
		
		if(sessao != null ) {
			if(sessao.getTipoAmbiente().equals(Sessao.TipoAmbienteConstante.VIRTUAL.getSigla())) {
				observacao = "Julgamento Virtual: " + siglaCadeiaIncidente + " - Agendado para: "+ getDataHoraFormatada(sessao.getDataPrevistaInicio()).substring(0, 10) + ".";
			}
		}
		return observacao;
	}

	private String getDataHoraFormatada(Date dataAtual) {
		return DateFormat.getDateTimeInstance().format(dataAtual);
	}

	private Long defineCodigoDoAndamento(DadosAgendamentoDto dadosAndamento) {
		if (isPauta(dadosAndamento.getTipoAgendamento())) {
			return AndamentoProcesso.COD_ANDAMENTO_INCLUA_EM_PAUTA_MIN_EXT;
		}
		return AndamentoProcesso.COD_ANDAMENTO_APRESENTACAO_MESA_JULG;
	}

	private Agendamento incluiAgendamentoNovo(DadosAgendamentoDto dadosAgendamento, ObjetoIncidente<?> objetoIncidente,
			Integer codigoColegiado) throws ServiceException {
		Agendamento agendamento;
		agendamento = new Agendamento();
		AgendamentoId agendamentoId = new AgendamentoId();
		agendamentoId.setCodigoCapitulo(codigoColegiado);
		agendamentoId.setCodigoMateria(defineTipoDeAgendamento(dadosAgendamento));
		agendamentoId.setObjetoIncidente(objetoIncidente.getId());
		agendamento.setObjetoIncidente(objetoIncidente);
		agendamento.setId(agendamentoId);
		agendamento.setMinistro(dadosAgendamento.getMinistro());
		agendamento.setVista(isProcessoComVistas(objetoIncidente, dadosAgendamento.getMinistro()));
		agendamento.setObservacaoProcesso(dadosAgendamento.getObservacao());
		agendamento.setJulgado(false);
		agendamento.setDirigida(dadosAgendamento.isPautaDirigida());
		agendamentoService.incluir(agendamento);
		return agendamento;
	}

	private Boolean isProcessoComVistas(ObjetoIncidente<?> objetoIncidente, Ministro ministroDoGabinete)
			throws ServiceException {
		return !isMinistroRelatorIncluindoPresidencia(objetoIncidente, ministroDoGabinete);
	}

	public Integer defineTipoDeAgendamento(DadosAgendamentoDto dadosAgendamento) throws ServiceException {
		TipoAgendamento tipoAgendamento = dadosAgendamento.getTipoAgendamento();
		boolean isPauta = isPauta(tipoAgendamento);
		if (isPauta) {
			return Agendamento.COD_MATERIA_AGENDAMENTO_PAUTA;
		} else {
			return Agendamento.COD_MATERIA_AGENDAMENTO_JULGAMENTO;
		}
	}

	private Agendamento consultaAgendamentoCadastrado(ObjetoIncidente<?> objetoIncidente, Ministro ministroDoGabinete)
			throws ServiceException, AgendamentoNaoDefinidoException {
		ConsultaDeAgendamentoVO consulta = new ConsultaDeAgendamentoVO();
		consulta.setCodigoDoMinistro(ministroDoGabinete.getId());
		consulta.setSequencialObjetoIncidente(objetoIncidente.getId());
		return agendamentoService.consultarAgendamento(consulta);
	}

	private void verificaFechamentoDaPauta(Ministro ministroDoGabinete, Integer codigoColegiado)
			throws ServiceException, ProcessoNaoPodeSerAgendadoException {
		if (isPautaFechada(codigoColegiado)) {
			throw new ProcessoNaoPodeSerAgendadoException(
					"A libera��o de processos para julgamento est� bloqueada para esta data. A Pauta de Julgamento j� foi fechada. Caso tenha alguma d�vida, entre em contato com a Secretaria de Sess�es.");
		}
	}

	private Integer defineCapituloParaVerificacaoDaPauta(DadosAgendamentoDto dadosAgendamento) throws ServiceException {
		if (isGabineteOuPresidencia(dadosAgendamento)) {
			if (dadosAgendamento.getTipoColegiadoAgendamento().equals(TipoColegiadoAgendamento.PLENARIO)) {
				return EstruturaPublicacao.COD_CAPITULO_PLENARIO;
			}
			return defineCodigoDaTurmaDoMinistro(dadosAgendamento.getMinistro(), dadosAgendamento);
		}
		return dadosAgendamento.getSetorDoUsuario().getCodigoCapitulo();
	}

	private boolean isGabineteOuPresidencia(DadosAgendamentoDto dadosAgendamento) throws ServiceException {
		return isSetorDeGabinete(dadosAgendamento)
				|| dadosAgendamento.getSetorDoUsuario().getId().equals(Setor.CODIGO_SETOR_PRESIDENCIA)
				|| dadosAgendamento.getSetorDoUsuario().getId().equals(Setor.CODIGO_SETOR_VICE_PRESIDENCIA);
	}

	private Boolean isSetorDeGabinete(DadosAgendamentoDto dadosAgendamento) throws ServiceException {
		return setorService.isSetorGabinete(dadosAgendamento.getSetorDoUsuario());
	}

	@Override
	public Integer defineCodigoDaTurmaDoMinistro(Ministro ministroDoGabinete, DadosAgendamentoDto dadosAgendamento) {
		//m�todo alterado pois o usu�rio agora poder� liberar o processo para uma turma que n�o � a do ministro.
		if (dadosAgendamento == null){
			if (Ministro.CODIGO_PRIMEIRA_TURMA.equals(ministroDoGabinete.getCodigoTurma())) {
				return EstruturaPublicacao.COD_CAPITULO_PRIMEIRA_TURMA;
			}
			return EstruturaPublicacao.COD_CAPITULO_SEGUNDA_TURMA;			
		}else{
			if (dadosAgendamento.getTipoColegiadoAgendamento().getId().equals("1T")){
				return EstruturaPublicacao.COD_CAPITULO_PRIMEIRA_TURMA;
			}
			return EstruturaPublicacao.COD_CAPITULO_SEGUNDA_TURMA;
		}
	}

	private boolean isPautaFechada(Integer codigoColegiado) throws ServiceException {
		ConsultaDeConteudoPublicacaoVO consulta = montaConsultaDeFechamentoDePauta(codigoColegiado);
		return conteudoPublicacaoService.isPautaDeJulgamentoFechada(consulta);
	}

	private ConsultaDeConteudoPublicacaoVO montaConsultaDeFechamentoDePauta(Integer codigoColegiado) {
		ConsultaDeConteudoPublicacaoVO consulta = new ConsultaDeConteudoPublicacaoVO();
		consulta.setCodigoCapitulo(codigoColegiado);
		consulta.setCodigoMateria(Agendamento.COD_MATERIA_AGENDAMENTO_PAUTA);
		return consulta;
	}

	@Override
	public List<Agendamento> consultarAgendamentos(Set<ObjetoIncidenteDto> processosSelecionados) {
		try {
			ConsultaDeAgendamentoVO consulta = new ConsultaDeAgendamentoVO();
			Collection<Long> sequenciais = new ArrayList<Long>();
			for (ObjetoIncidenteDto objetoIncidente : processosSelecionados) {
				sequenciais.add(objetoIncidente.getId());
			}
			consulta.setSequenciaisObjetosIncidentes(sequenciais);
			return agendamentoService.consultaAgendamentos(consulta);
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}

	}

	@Override
	public boolean isCancelamentoPrecisaDeConfirmacao(Agendamento agendamento, Setor setor)
			throws AgendamentoNaoPodeSerCanceladoException {
		try {
			if (isAgendamentoEmPautaComposta(agendamento)) {
				throw new AgendamentoNaoPodeSerCanceladoException(
						"O processo se encontra em pauta para publica��o e n�o poder� ter o agendamento desfeito.");
			}
			if (isIncluirAndamentoDeRetiradoDeMesa(agendamento, setor) || isIncluirAndamentoDeRetiradoDePauta(agendamento, setor)) {
				return true;
			}
			return false;
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}

	}

	private boolean isAgendamentoEmPautaComposta(Agendamento agendamento) throws ServiceException {
		if (isAgendamentoParaPauta(agendamento)) {
			Integer codigoCapitulo = agendamento.getId().getCodigoCapitulo();
			Integer codigoMateria  = agendamento.getId().getCodigoMateria();
			ObjetoIncidente<?> objetoIncidente = agendamento.getObjetoIncidente();
			List<ProcessoPublicado> processosPublicados = processoPublicadoService.pesquisar(codigoCapitulo, codigoMateria, objetoIncidente, false);
			return (processosPublicados != null && processosPublicados.size() > 0);
		}
		return false;
	}

	boolean isAgendamentoParaIndice(Agendamento agendamento) {
		boolean agendamentoDaMateriaPreIndicie  = this.isAgendamentoDaMateria(agendamento, Agendamento.COD_MATERIA_AGENDAMENTO_INDICE);
		boolean agendamentoDaMateriaIndice      = this.isAgendamentoDaMateria(agendamento, Agendamento.COD_MATERIA_AGENDAMENTO_JULGAMENTO);
		return agendamentoDaMateriaPreIndicie || agendamentoDaMateriaIndice;
	}

	boolean isAgendamentoParaPauta(Agendamento agendamento) {
		boolean agendamentoDaMateria = isAgendamentoDaMateria(agendamento, Agendamento.COD_MATERIA_AGENDAMENTO_PAUTA);
		return agendamentoDaMateria;
	}

	private boolean isAgendamentoDaMateria(Agendamento agendamento, Integer codigoMateria) {
		return codigoMateria.equals(agendamento.getId().getCodigoMateria());
	}

	/**
	 * Verifica se, com a exclus�o do agendamento, o andamento dever� ser retirado de mesa. Segundo 
	 * o MTT, isso deve acontecer quando o Setor for um gabinete, e 
	 * 
	 * @param agendamento
	 * @return
	 * @throws ServiceException
	 */
	boolean isIncluirAndamentoDeRetiradoDeMesa(Agendamento agendamento, Setor setor) throws ServiceException {
		boolean setorGabinete         = setorService.isSetorGabinete(setor);
		boolean agendamentoParaIndice = this.isAgendamentoParaIndice(agendamento);
		return setorGabinete && agendamentoParaIndice;
	}
	
	/**
	 * Verifica se, com a exclus�o do agendamento, o andamento dever� ser retirado de pauta.
	 * 
	 * @param agendamento
	 * @return
	 * @throws ServiceException
	 */
	boolean isIncluirAndamentoDeRetiradoDePauta(Agendamento agendamento, Setor setor) throws ServiceException {
		boolean setorGabinete        = setorService.isSetorGabinete(setor);
		boolean agendamentoParaPauta = this.isAgendamentoParaPauta(agendamento);
		return setorGabinete && agendamentoParaPauta;
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void cancelarAgendamentoDoProcesso(ObjetoIncidenteDto objetoIncidente, Principal usuario, String removerJulgamentoConjunto, String removerListaJulgamento, String tipoAmbiente) throws ProcessoPrecisaDeConfirmacaoException {
		try {
			Agendamento agendamento = consultarAgendamentoDoObjetoIncidente(objetoIncidente);
			Ministro ministro = usuario.getMinistro();
			if(ministro != null){
				this.inserirAndamentoProcessoCancelado(usuario, agendamento, tipoAmbiente);
			}
			/* Remove v�nculo de Processos em Julgamento Conjunto */
			removerProcessoDeJulgamentoConjunto( agendamento, removerJulgamentoConjunto, usuario );
			/* Remove v�nculo de Processos em Lista de Julgamento */
			removerProcessoDeListaJulgamento( agendamento, removerListaJulgamento, usuario );
			removerJulgamentoProcesso( objetoIncidente );
			agendamentoService.excluir(agendamento);
			
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}

	void inserirAndamentoProcessoCancelado(Principal usuario
			                             ,Agendamento agendamento, String tipoAmbiente) throws ServiceException, ProcessoPrecisaDeConfirmacaoException {
		ListaJulgamento listaJulgamento = this.processoEmListaJulgamento(agendamento); 
		if(listaJulgamento != null){
			this.defineAndamentoProcessoCanceladoListaJulgamento(listaJulgamento,agendamento,usuario);
		}else{
			Setor setor = usuario.getMinistro().getSetor();
			boolean incluirAndamentoDeRetiradoDeMesa    = this.isIncluirAndamentoDeRetiradoDeMesa(agendamento, setor);
			boolean isIncluirAndamentoDeRetiradoDePauta = this.isIncluirAndamentoDeRetiradoDePauta(agendamento, setor);
			if (incluirAndamentoDeRetiradoDeMesa) {
				this.inserirAndamentoDeRetiradoDeMesa(agendamento, usuario);
			} else if (isIncluirAndamentoDeRetiradoDePauta) {
				this.inserirAndamentoDeRetiradoDePauta(agendamento, usuario, tipoAmbiente);
			}
		}
	}

	void defineAndamentoProcessoCanceladoListaJulgamento(ListaJulgamento listaJulgamento
														,Agendamento agendamento
														,Principal usuario) throws ServiceException, ProcessoPrecisaDeConfirmacaoException {
		this.validarAlterarListaJulgamentoVirtual(listaJulgamento);
		Long idTipoAndamentoListaJulgamento = this.getAndamentoProcessoCanceladoListaJulgamento(listaJulgamento,agendamento);
		if(ListaJulgamentoUI.TIPO_ANDAMENTO_LIBERACAO_MESA.equals(idTipoAndamentoListaJulgamento)){
			this.inserirAndamentoDeRetiradoDeMesa(agendamento, usuario);
		}else{
			String tipoAmbiente = (listaJulgamento!=null && listaJulgamento.getSessao()!=null)?listaJulgamento.getSessao().getTipoAmbiente():null;
			this.inserirAndamentoDeRetiradoDePauta(agendamento, usuario, tipoAmbiente);
		}
	}

	public Long getAndamentoProcessoCanceladoListaJulgamento(ListaJulgamento listaJulgamento
													        ,Agendamento agendamento){
		
		boolean isProcessoEmListaJulgamentoVirtual = this.isProcessoEmListaJulgamentoVirtual(listaJulgamento);
		Long idTipoAndamentoListaJulgamentoRetorno = ListaJulgamentoUI.TIPO_ANDAMENTO_LIBERACAO_PAUTA;
		if(!isProcessoEmListaJulgamentoVirtual){
			Andamento andamento = listaJulgamento.getAndamentoLiberacao();
			Long idTipoAndamentoDaLista = andamento.getId();
			if(ListaJulgamentoUI.TIPO_ANDAMENTO_LIBERACAO_PAUTA.equals(idTipoAndamentoDaLista)){
				idTipoAndamentoListaJulgamentoRetorno = ListaJulgamentoUI.TIPO_ANDAMENTO_LIBERACAO_PAUTA;
			}else{
				idTipoAndamentoListaJulgamentoRetorno = ListaJulgamentoUI.TIPO_ANDAMENTO_LIBERACAO_MESA;
			}
		}
		return idTipoAndamentoListaJulgamentoRetorno;
	}
	
	/**
	 * Verica se o processo faz parte Julgamento Virtual de Listas
	 * @param agendamento
	 * @return
	 */
	boolean isProcessoEmListaJulgamentoVirtual(ListaJulgamento listaJulgamento){
		boolean isProcessoEmListaJulgamentoVirtual = false;
		if (listaJulgamento != null) {
            Sessao sessao         = listaJulgamento.getSessao();
            String tipoAmbiente   = sessao.getTipoAmbiente();
			boolean isAmbienteVirtual = TipoAmbienteConstante.VIRTUAL.equals(TipoAmbienteConstante.valueOfSigla(tipoAmbiente));
            if(isAmbienteVirtual){
            	isProcessoEmListaJulgamentoVirtual = true;
            }
		}
		return isProcessoEmListaJulgamentoVirtual;
	}
	
	public ListaJulgamento processoEmListaJulgamento(Agendamento agendamento){
		ListaJulgamento listaJulgamento = null;		
		List<ListaJulgamento> listDeListaJulgamento = this.carregarProcessosEmListaJulgamento(agendamento);
		if (listDeListaJulgamento != null && listDeListaJulgamento.size() == 1) {
            listaJulgamento = listDeListaJulgamento.get(0);
		}
		return listaJulgamento;
	}	
	
	void validarAlterarListaJulgamentoVirtual(ListaJulgamento listaJulgamento) throws ProcessoPrecisaDeConfirmacaoException {
		Sessao sessao                 = listaJulgamento.getSessao();
		Date datePrevistaInicioSessao = sessao.getDataPrevistaInicio();
		GregorianCalendar calendarPrevistaInicioSessao = this.date2GregorianCalendar(datePrevistaInicioSessao);
		GregorianCalendar hoje = this.getNow();
		Integer datMaior = hoje.compareTo(calendarPrevistaInicioSessao);
		if(datMaior.equals(0)){
			throw new ProcessoPrecisaDeConfirmacaoException(MSG_ERRO_ALTERAR_LISTA_JULGAMENTO_VIRTUAL_INICIO_SESSAO);
		}else if(datMaior > 0){
			throw new ProcessoPrecisaDeConfirmacaoException(MSG_ERRO_ALTERAR_LISTA_JULGAMENTO_VIRTUAL_INICIO_SESSAO);
		}
	}
	
	//Facilitar Junit
	GregorianCalendar getNow(){
		return dataUtil.getNow();
	}

	//Facilitar Junit
	GregorianCalendar date2GregorianCalendar(Date datePrevistaInicioSessao) throws ProcessoPrecisaDeConfirmacaoException {
		try{
			return dataUtil.date2GregorianCalendar(datePrevistaInicioSessao);	
		}
		catch (ParseException e) {
				throw new ProcessoPrecisaDeConfirmacaoException(e);
		}
	}

	/** Remove o JulgamentoProcesso para o Objeto Incidente
	 *  que ter� o Julgamento cancelado. 
	 *  @param objetoIncidente O processo a ser cancelado o Julgamento
	 *                         Processo. */
	private void removerJulgamentoProcesso( ObjetoIncidenteDto objetoIncidente  ){
		try {
			removerJulgamentoProcesso( objetoIncidenteService.recuperarPorId( objetoIncidente.getId() ) );
		} catch (ServiceException e) {
			throw new NestedRuntimeException( e );
		}
	}
	
	private Sessao recuperarSessaoDoObjetoIncidente(ObjetoIncidente<?> objetoIncidente){
		Sessao retorno = null;
		try {
			retorno = sessaoService.recuperar( objetoIncidente );
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
		return retorno;
	}
	
	
	/** Remove o JulgamentoProcesso para o Objeto Incidente
	 *  que ter� o Julgamento cancelado. 
	 *  @param objetoIncidente O processo a ser cancelado o Julgamento
	 *                         Processo. */
	private void removerJulgamentoProcesso( ObjetoIncidente<?> objetoIncidente  ){
		try {
			Sessao sessao = recuperarSessaoDoObjetoIncidente( objetoIncidente );
			if ( sessao != null ){
				JulgamentoProcesso julgamentoProcesso = julgamentoProcessoService.recuperar( objetoIncidente, sessao );
				if (julgamentoProcesso != null ) {
					julgamentoProcessoService.excluir( julgamentoProcesso );
				}
			}
		} catch (ServiceException e) {
			throw new NestedRuntimeException( e );
		}
	}
	
	/** Remove o v�nculo de Julgamento Conjunto do Processo **/
	private void removerProcessoDeJulgamentoConjunto(Agendamento agendamento, String removerProcessoJulgamentoConjunto, Principal usuario){
		Long idSeqListaJulgamentoConjunto = new Long( 0L );
		List<ObjetoIncidente<?>> procJulgamentoConjuntoTodosMinistros = new ArrayList<ObjetoIncidente<?>>();
		try {
			List<ObjetoIncidente<?>> procJulgConjPorMinistro = carregaProcessosEmJulgamentoConjunto( agendamento, true );
			if ( procJulgConjPorMinistro != null && procJulgConjPorMinistro.size() > 0 ) {
				idSeqListaJulgamentoConjunto = recuperarIdSeqListaJulgamentoConjunto( agendamento.getObjetoIncidente() );
				removerVinculoJulgamentoConjunto( agendamento.getObjetoIncidente() );
				if ( removerProcessoJulgamentoConjunto.equalsIgnoreCase( CancelarLiberacaoParaJulgamentoActionFacesBean.REMOVER_TODOS_PROCESSOS_EM_JULGAMENTO_CONJUNTO ) ) {
					if ( idSeqListaJulgamentoConjunto !=  0L ){
						cancelarAgendamentoDeProcessosEmJulgamentoConjunto( agendamento, usuario, idSeqListaJulgamentoConjunto );
					}
					for ( ObjetoIncidente<?> oi: procJulgConjPorMinistro ) {
						removerVinculoJulgamentoConjunto( oi );
						removerJulgamentoProcesso( oi );
					}
				}
			}
			/* Verifico se a lista possui apenas 1 �nico elemento, caso positivo, eliminar a lista */
			if ( idSeqListaJulgamentoConjunto !=  0L ){
				procJulgamentoConjuntoTodosMinistros = informacaoPautaProcessoService.recuperarProcessosJulgamentoConjunto( idSeqListaJulgamentoConjunto );
				/* Verifica se a lista restante possui somente um elemento, caso positivo, excluir a lista */
				if ( procJulgamentoConjuntoTodosMinistros != null && procJulgamentoConjuntoTodosMinistros.size() == 1 ){
					desfazerListaJulgamentoConjunto( procJulgamentoConjuntoTodosMinistros );
				}
			}
		} catch (Exception e) {
			throw new NestedRuntimeException(e);
		}
	}
	
	/** Remove o Processo de uma Lista de Julgamento **/
	private void removerProcessoDeListaJulgamento(Agendamento agendamento, String removerProcessosListaJulgamento, Principal usuario) throws ProcessoPrecisaDeConfirmacaoException{
		List<ListaJulgamento> listaJulgamento = carregarProcessosEmListaJulgamento( agendamento );
		if ( listaJulgamento != null && listaJulgamento.size() > 0 ) {
			if ( removerProcessosListaJulgamento.equalsIgnoreCase( CancelarLiberacaoParaJulgamentoActionFacesBean.REMOVER_SOMENTE_O_PROCESSO_LISTA_JULGAMENTO ) ){
				removerVinculoListaJulgamento( agendamento, false);
			} else {
				cancelarAgendamentoDeProcessosEmListaJulgamento(agendamento, usuario);
				removerVinculoListaJulgamento( agendamento, true);
			}
		}
		listaJulgamento = carregarProcessosEmListaJulgamento( agendamento );
		if ( listaJulgamento.size() > 0 ){
			/* Varre cada lista, verificando se ela n�o possui Objeto Incidente */
			for ( ListaJulgamento lista: listaJulgamento ) {
				if ( lista.getElementos() != null && lista.getElementos().size() == 0 ){
					desfazerListaJulgamento( lista );
				}
			}
		}
	}
	
	/** Recupera o ID da Lista de Julgamento Conjunto, a qual o Objeto Incidente pertence **/
	private Long recuperarIdSeqListaJulgamentoConjunto(ObjetoIncidente<?> objetoIncidente){
		Long retorno = new Long( 0L );
		try {
			InformacaoPautaProcesso informacaoPautaProcesso = informacaoPautaProcessoService.recuperar( objetoIncidente );
			if ( informacaoPautaProcesso != null && informacaoPautaProcesso.getSeqListaJulgamentoConjunto() != null ){
				retorno = informacaoPautaProcesso.getSeqListaJulgamentoConjunto();
			}
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);		
		}
		return retorno;
	}

	/** Utilizado quando a quantidade de processo na lista � igula a 1 **/
	private void desfazerListaJulgamentoConjunto(List<ObjetoIncidente<?>> listaJulgamentoConjunto){
		try {
			for ( ObjetoIncidente<?> oi: listaJulgamentoConjunto ) {
				InformacaoPautaProcesso informacaoPautaProcesso = informacaoPautaProcessoService.recuperar( oi );
				if ( informacaoPautaProcesso != null ){
					if ( informacaoPautaProcesso.getSeqListaJulgamentoConjunto() != null ){
						informacaoPautaProcesso.setSeqListaJulgamentoConjunto( null );
						informacaoPautaProcessoService.alterar( informacaoPautaProcesso );
					}
				}
			}
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}
	
	/** Desfaz a lista de Julgamento, caso ela possua apenas um processo **/
	private void desfazerListaJulgamento(List<ListaJulgamento> listaJulgamento){
		if ( listaJulgamento != null && listaJulgamento.size() > 0 ){
			for ( ListaJulgamento lista: listaJulgamento ){
				desfazerListaJulgamento( lista );
			}
		}
	}
	
	/** Desfaz a lista de Julgamento, caso ela possua apenas um processo **/
	private void desfazerListaJulgamento(ListaJulgamento listaJulgamento){
		try {
			listaJulgamentoService.excluir( listaJulgamento );
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}
	
	/** Cancela o Agendamento dos Processos com V�nculo de Julgamento Conjunto **/
	private void cancelarAgendamentoDeProcessosEmJulgamentoConjunto(Agendamento agendamento, Principal usuario, Long idSeqListaJulgamentoConjunto) throws ProcessoPrecisaDeConfirmacaoException{
		try {
			List<ObjetoIncidente<?>> procJulgConjPorMinistro = carregaProcessosEmJulgamentoConjunto( agendamento, idSeqListaJulgamentoConjunto, true );
			if ( procJulgConjPorMinistro != null && procJulgConjPorMinistro.size() > 0 ){
				for ( ObjetoIncidente<?> oi: procJulgConjPorMinistro ) {
					ObjetoIncidenteDto objetoIncidenteDto = new ObjetoIncidenteDto();
					objetoIncidenteDto.setId( oi.getId() );
					Agendamento agendamentoRecuperado = consultarAgendamentoDoObjetoIncidente( objetoIncidenteDto );
					this.inserirAndamentoProcessoCancelado(usuario,agendamentoRecuperado, null);
					agendamentoService.excluir( agendamentoRecuperado );
				}
			}
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}
	
	
	/** Cancela o Agendamento dos Processos em lista de Julgamento 
	 * @param agendamento o agendamento do qual ser� carregado os processo em lista de Julgamento
	 * @param usuario para fins de andamento**/
	private void cancelarAgendamentoDeProcessosEmListaJulgamento(Agendamento agendamento, Principal usuario) throws ProcessoPrecisaDeConfirmacaoException{
		try {
			List<ListaJulgamento> listaJulgamento = carregarProcessosEmListaJulgamento( agendamento );
			if ( listaJulgamento != null && listaJulgamento.size() > 0 ) {
				for ( ListaJulgamento lista: listaJulgamento ) {
					if ( lista.getElementos() != null && lista.getElementos().size() > 0 ) {
						String tipoAmbiente = (lista.getSessao() != null)?lista.getSessao().getTipoAmbiente():null;
						for ( ObjetoIncidente<?> oi: lista.getElementos() ) {
							if ( !oi.equals( agendamento.getObjetoIncidente() ) ){
								ObjetoIncidenteDto objetoIncidenteDto = new ObjetoIncidenteDto();
								objetoIncidenteDto.setId( oi.getId() );
								Agendamento agendamentoRecuperado = consultarAgendamentoDoObjetoIncidente( objetoIncidenteDto );
								this.inserirAndamentoProcessoCancelado(usuario,agendamentoRecuperado, tipoAmbiente);
								agendamentoService.excluir( agendamentoRecuperado );
							}
						}
					}
				}
			}
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}
	
	/** Carrega a lista de Julgamento Conjunto, levando em considera��o o Ministro
	 * @param agendamento o agendamento a ser considerado. 
	 * @param somenteProcessosMinistroRelator 
	 * 		Quando TRUE, somente os processos do Ministro Relator ser�o considerados.
	 * 		Quando FALSE, o Ministro Relator n�o � considerado, 
	 * 		Quando NULL, n�o se filtra por Ministro, apenas o fato de que est�o vinculados � considerado **/	
	private List<ObjetoIncidente<?>> carregaProcessosEmJulgamentoConjunto(Agendamento agendamento, Boolean somenteProcessosMinistroRelator){
		List<ObjetoIncidente<?>> retorno = new ArrayList<ObjetoIncidente<?>>();
		ObjetoIncidenteDto objetoIncidenteDto = new ObjetoIncidenteDto();
		objetoIncidenteDto.setId( agendamento.getObjetoIncidente().getId() );
		try {
			List<ObjetoIncidenteDto> processosJulgamentoConjunto = recuperarProcessosJulgamentoConjunto( objetoIncidenteDto );		
			for ( ObjetoIncidenteDto oi: processosJulgamentoConjunto ) {
				ObjetoIncidente<?> oiRelator = recuperarObjetoIncidentePorId( oi.getId() );
				Ministro minRelator = recuperarMinistroRelatorIncidente( oiRelator );
				// TODO verificar regra de Ministro Relator
				if ( somenteProcessosMinistroRelator != null && somenteProcessosMinistroRelator ) {
					if ( agendamento.getMinistroRelator() != null ){
						if ( minRelator.equals( agendamento.getMinistroRelator() ) ){
							retorno.add( oiRelator );
						}
					} else {
						retorno.add( oiRelator );
					}
				} else {
					if ( agendamento.getMinistroRelator() != null && somenteProcessosMinistroRelator != null){
						if ( !minRelator.equals( agendamento.getMinistroRelator() ) ){
							retorno.add( oiRelator );
						}
					} else {
						retorno.add( oiRelator );
					}
				}
			}	
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
		return retorno;
	}
	
	/** Carrega a lista de Julgamento Conjunto, levando em considera��o o Ministro
	 * @param agendamento o agendamento a ser considerado. 
	 * @param idSeqListaJulgamentoConjunto identifica a lista no qual o processo pertence.
	 * @param somenteProcessosMinistroRelator 
	 * 		Quando TRUE, somente os processos do Ministro Relator ser�o considerados.
	 * 		Quando FALSE, o Ministro Relator n�o � considerado, 
	 * 		Quando NULL, n�o se filtra por Ministro, apenas o fato de que est�o vinculados � considerado **/	
	private List<ObjetoIncidente<?>> carregaProcessosEmJulgamentoConjunto(Agendamento agendamento, Long idSeqListaJulgamentoConjunto, Boolean somenteProcessosMinistroRelator){
		List<ObjetoIncidente<?>> retorno = new ArrayList<ObjetoIncidente<?>>();
		try {
			List<ObjetoIncidente<?>> processosJulgamentoConjunto = informacaoPautaProcessoService.recuperarProcessosJulgamentoConjunto( idSeqListaJulgamentoConjunto );		
			for ( ObjetoIncidente<?> oi: processosJulgamentoConjunto ) {
				Ministro minRelator = recuperarMinistroRelatorIncidente( oi );
				// TODO verificar regra de Ministro Relator
				if ( somenteProcessosMinistroRelator != null && somenteProcessosMinistroRelator ) {
					if ( agendamento.getMinistroRelator() != null ){
						if ( minRelator.equals( agendamento.getMinistroRelator() ) ){
							retorno.add( oi );
						}
					} else {
						retorno.add( oi );
					}
				} else {
					if ( agendamento.getMinistroRelator() != null && somenteProcessosMinistroRelator != null){
						if ( !minRelator.equals( agendamento.getMinistroRelator() ) ){
							retorno.add( oi );
						}
					} else {
						retorno.add( oi );
					}
				}
			}	
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
		return retorno;
	}

	
	/** Carrega so Processo que est�o em Lista de Julgamento **/
	List<ListaJulgamento> carregarProcessosEmListaJulgamento(Agendamento agendamento){
		List<ListaJulgamento> retorno = new ArrayList<ListaJulgamento>();
		try {
			ObjetoIncidente<?> objetoIncidente = agendamento.getObjetoIncidente();
			retorno = listaJulgamentoService.pesquisar( objetoIncidente, Boolean.TRUE );
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
		return retorno;
	}

	/** Remove o v�nculo do Julgamento Conjunto **/
	private void removerVinculoJulgamentoConjunto(ObjetoIncidente<?> objetoIncidente){
		try {
			InformacaoPautaProcesso informacaoPautaProcesso = informacaoPautaProcessoService.recuperar( objetoIncidente );
			if ( informacaoPautaProcesso != null ){
				if ( informacaoPautaProcesso.getSeqListaJulgamentoConjunto() != null ){
					informacaoPautaProcesso.setSeqListaJulgamentoConjunto( null );
					informacaoPautaProcessoService.alterar( informacaoPautaProcesso );
				}
			}
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}
	
	/** Remove o v�nculo da Lista de Julgamento 
	 * @param objetoIncidente o objeto incidente a ser desvinculado
	 * @param excluirTodaLista se TRUE, desfaz toda a lista de julgamento, 
	 *                     se FALSE, remove somente o objetoIncidente da lista **/
	private void removerVinculoListaJulgamento(Agendamento agendamento, Boolean excluirTodaLista){
		try {
			List<ListaJulgamento> listaJulgamento = listaJulgamentoService.pesquisar( agendamento.getObjetoIncidente(), Boolean.TRUE );
			List<ListaJulgamento> listaJulgamentoExcluir = new ArrayList<ListaJulgamento>();
			if ( listaJulgamento != null && listaJulgamento.size() > 0 ){
				for ( ListaJulgamento lista: listaJulgamento ) {
					if ( excluirTodaLista ) {
						listaJulgamentoExcluir.add( lista );
					} else {
						lista.getElementos().remove( agendamento.getObjetoIncidente() );
						listaJulgamentoService.alterar( lista );	
					}
				}
				if ( excluirTodaLista && listaJulgamentoExcluir.size() > 0 ) {
					desfazerListaJulgamento( listaJulgamentoExcluir );
				}
			}
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}

	Agendamento consultarAgendamentoDoObjetoIncidente(ObjetoIncidenteDto objetoIncidente) throws ServiceException, ProcessoPrecisaDeConfirmacaoException {
		Agendamento agendamento =  new Agendamento();		
		ConsultaDeAgendamentoVO consulta       = this.setConsultaDeAgendamentoVO(objetoIncidente);
		List<Agendamento> consultaAgendamentos = agendamentoService.consultaAgendamentos(consulta);
		if (consultaAgendamentos.size() > 0){
			agendamento = consultaAgendamentos.get(0);
		}else{
			throw new ProcessoPrecisaDeConfirmacaoException(ObjetoIncidenteServiceImpl.MSG_ERRO_PAUTA_NAO_CONFIRMADA);
		}
		return agendamento;
	}

	// Criado para facilitar testes unitarios. 
	ConsultaDeAgendamentoVO setConsultaDeAgendamentoVO(ObjetoIncidenteDto objetoIncidente) {
		ConsultaDeAgendamentoVO consulta = new ConsultaDeAgendamentoVO();
		consulta.setSequencialObjetoIncidente(objetoIncidente.getId());
		return consulta;
	}
	
	void inserirAndamento(Agendamento agendamento
			           , Principal usuario
			           , Long codAndamento
			           , String tipoAmbiente) throws ServiceException {
		ObjetoIncidente<?> objetoIncidente = agendamento.getObjetoIncidente();
		Long numeroUltimoAndamento = andamentoProcessoService.recuperarUltimoNumeroSequencia(objetoIncidente);
		numeroUltimoAndamento++;
		Date dataAtual = new Date();
		ObjetoIncidente<?> processoOuRecurso = getProcessoOuRecurso(objetoIncidente);
		
		AndamentoProcesso andamentoProcesso = new AndamentoProcesso();		
		andamentoProcesso.setCodigoAndamento(codAndamento);
		andamentoProcesso.setObjetoIncidente(processoOuRecurso);
		andamentoProcesso.setCodigoUsuario(usuario.getUsuario().getId());
		andamentoProcesso.setDataAndamento(dataAtual);
		andamentoProcesso.setDataHoraSistema(dataAtual);
		
		String observacao = defineObservacao(agendamento.getId().getCodigoCapitulo(), dataAtual,objetoIncidente, null);		
		//TODO - Incluir condi��o para inserir julgamento virtual no andamento.
		if(Sessao.TipoAmbienteConstante.VIRTUAL.getSigla().equals(tipoAmbiente)){
			observacao = " Julgamento Virtual - "+observacao;
		}
		andamentoProcesso.setDescricaoObservacaoAndamento(observacao);
		andamentoProcesso.setNumeroSequencia(numeroUltimoAndamento);
		andamentoProcesso.setSetor(usuario.getMinistro().getSetor());
		andamentoProcesso = andamentoProcessoService.incluir(andamentoProcesso);
		
		andamentoProcesso = andamentoProcessoService.recuperarAndamentoProcesso(andamentoProcesso.getId());
		andamentoProcesso.setObjetoIncidente(objetoIncidente);
		andamentoProcessoService.persistirAndamentoProcesso(andamentoProcesso);
	}
	
	void inserirAndamentoDeRetiradoDeMesa(Agendamento agendamento, Principal usuario) throws ServiceException {
		Long codAndamento = AndamentoProcesso.COD_ANDAMENTO_RETIRADO_MESA;
		inserirAndamento(agendamento, usuario, codAndamento, null);
	}	
	
	void inserirAndamentoDeRetiradoDePauta(Agendamento agendamento, Principal usuario, String tipoAmbiente) throws ServiceException {
		Long codAndamento = AndamentoProcesso.COD_ANDAMENTO_RETIRADO_PAUTA;
		inserirAndamento(agendamento, usuario, codAndamento, tipoAmbiente);
	}

	@Override
	public Setor consultarSetorPeloId(Long id) {
		try {
			return setorService.recuperarPorId(id);
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}

	@Override
	public ProcessoSetor recuperarProcessoSetor(ObjetoIncidenteDto dto, Setor setor) throws ServiceException {
		Long idObjetoDTO = dto.getId();
		ObjetoIncidente<?> objetoIncidente = recuperarObjetoIncidentePorId(idObjetoDTO);
		Long idObjetoIncidentePrincipal = objetoIncidente.getPrincipal().getId();
		Long idSetor = setor.getId();
		ProcessoSetor recuperarProcessoSetor = processoSetorService.recuperarProcessoSetor(idObjetoIncidentePrincipal, idSetor);
		return recuperarProcessoSetor;
	}

	@Override
	public List<TipoRecurso> recuperarTiposRecurso(boolean ativos) {
		try {
			return tipoRecursoService.recuperarTiposRecurso(ativos);
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}

	@Override
	public Ministro recuperarMinistroRelatorIncidente(
			ObjetoIncidente<?> objetoIncidente) throws ServiceException {
		return ministroService.recuperarMinistroRelatorIncidente(objetoIncidente);
	}

	@Override
	public InformacaoPautaProcesso gravarSustentacoesOrais(
			InformacaoPautaProcesso informacaoPautaProcesso,
			List<PrevisaoSustentacaoOralDto> sustentacoesOrais) throws ServiceException {
		removerSustentacoesOrais(informacaoPautaProcesso, sustentacoesOrais);
		incluirSustentacoesOrais(informacaoPautaProcesso, sustentacoesOrais);
		return informacaoPautaProcessoService.recuperarPorId(informacaoPautaProcesso.getId());
	}

	private void incluirSustentacoesOrais(
			InformacaoPautaProcesso informacaoPautaProcesso,
			List<PrevisaoSustentacaoOralDto> sustentacoesOrais) throws ServiceException {
		List<PrevisaoSustentacaoOral> paraIncluir = new ArrayList<PrevisaoSustentacaoOral>();
		for (PrevisaoSustentacaoOralDto dto : sustentacoesOrais) {
			if (dto.getId() == null) {
				PrevisaoSustentacaoOral pso = new PrevisaoSustentacaoOral();
				if (dto.getIdEnvolvido() != null) {
					pso.setEnvolvido(envolvidoService.recuperarPorId(dto.getIdEnvolvido()));
				}
				if (dto.getIdAdvogado() != null) {
					pso.setJurisdicionado(jurisdicionadoService.recuperarPorId(dto.getIdAdvogado()));
				}
				pso.setRepresentado(jurisdicionadoService.recuperarPorId(dto.getIdParte()));
				pso.setObservacao(dto.getObservacao());
				pso.setInformacaoPautaProcesso(informacaoPautaProcesso);
				paraIncluir.add(pso);
			}
		}
		previsaoSustentacaoOralService.incluirTodos(paraIncluir);
	}

	private void removerSustentacoesOrais(
			InformacaoPautaProcesso informacaoPautaProcesso,
			List<PrevisaoSustentacaoOralDto> sustentacoesOrais) throws ServiceException {
		List<PrevisaoSustentacaoOral> paraExcluir = new ArrayList<PrevisaoSustentacaoOral>();
		List<PrevisaoSustentacaoOral> gravados = previsaoSustentacaoOralService.pesquisar(informacaoPautaProcesso);
		for (PrevisaoSustentacaoOral pso : gravados) {
			if (isPrevisaoSustentacaoOralRemovida(pso, sustentacoesOrais)) {
				paraExcluir.add(pso);
			}
		}
		previsaoSustentacaoOralService.excluirTodos(paraExcluir);
	}

	private boolean isPrevisaoSustentacaoOralRemovida(
			PrevisaoSustentacaoOral pso,
			List<PrevisaoSustentacaoOralDto> sustentacoesOrais) {
		if (sustentacoesOrais != null) {
			for (PrevisaoSustentacaoOralDto dto : sustentacoesOrais) {
				if (pso.getId().equals(dto.getId())) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public List<AdvogadoSustentacaoOral> pesquisarAdvogados(String sugestaoNome) throws ServiceException {
		List<AdvogadoSustentacaoOral> resultado = new ArrayList<AdvogadoSustentacaoOral>();
		List<Envolvido> envolvidos = envolvidoService.pesquisar(sugestaoNome);
		List<Jurisdicionado> jurisdicionados = jurisdicionadoService.pesquisar(sugestaoNome);
		for (Envolvido envolvido : envolvidos) {
			AdvogadoSustentacaoOral aso = new AdvogadoSustentacaoOral();
			aso.setEnvolvido(envolvido);
			resultado.add(aso);
		}
		for (Jurisdicionado jurisdicionado : jurisdicionados) {
			AdvogadoSustentacaoOral aso = new AdvogadoSustentacaoOral();
			aso.setJurisdicionado(jurisdicionado);
			resultado.add(aso);
		}
		
		Collections.sort(resultado, new Comparator<AdvogadoSustentacaoOral>() {

			@Override
			public int compare(AdvogadoSustentacaoOral o1, AdvogadoSustentacaoOral o2) {
				return o1.getNome().compareToIgnoreCase(o2.getNome());
			}
			
		});
		
		return resultado;
	}

	@Override
	public SituacaoMinistroProcesso recuperarDistribuicaoProcesso(ObjetoIncidente<?> objetoIncidente) throws ServiceException {
		List<SituacaoMinistroProcesso> lista = situacaoMinistroProcessoService.pesquisar(objetoIncidente, Ocorrencia.RELATOR);
		if ( lista != null && lista.size() > 0 ){
			return lista.get(0);
		}
		return null;
	}

	@Override
	public List<ObjetoIncidenteDto> recuperarProcessosJulgamentoConjunto(
			ObjetoIncidenteDto objetoIncidente) throws ServiceException {
		List<ObjetoIncidente<?>> processosJulgamentoConjunto = informacaoPautaProcessoService.recuperarProcessosJulgamentoConjunto(
				recuperarObjetoIncidentePorId(objetoIncidente.getId()));
		List<ObjetoIncidenteDto> lista = new ArrayList<ObjetoIncidenteDto>();
		if (processosJulgamentoConjunto != null) {
			for (ObjetoIncidente<?> oi : processosJulgamentoConjunto) {
				if (oi.getId().equals(objetoIncidente.getId())) {
					continue;
				}
				lista.add(ObjetoIncidenteDto.valueOf(oi));
			}
		}
		return lista;
	}

	@Override
	public void incluirProcessoListaJulgamento(ObjetoIncidente<?> objetoIncidente, ListaJulgamento listaJulgamento, DadosAgendamentoDto dadosAgendamento) throws ServiceException, ValidacaoLiberacaoParaJulgamentoException{
		verificaProcessoEmListaJulgamentoConjunto(objetoIncidente);
		verificaProcessoEmListaJulgamentoPrevista(objetoIncidente);
		verificaAgendamentoProcesso(objetoIncidente,
				TipoColegiadoConstante.valueOfSigla(listaJulgamento.getSessao().getColegiado().getId()),
				Agendamento.COD_MATERIA_AGENDAMENTO_JULGAMENTO);
		
		ProcessoListaJulgamento processoListaJulgamento = new ProcessoListaJulgamento();
		processoListaJulgamento.setListaJulgamento(listaJulgamento);
		processoListaJulgamento.setObjetoIncidente(objetoIncidente);
		processoListaJulgamentoService.salvar(processoListaJulgamento);
		Integer codigoColegiado = defineCapituloParaVerificacaoDaPauta(dadosAgendamento);
		gravaAgendamento(dadosAgendamento, objetoIncidente, codigoColegiado);
	}
	
	
	Andamento getAndamentoEscolhido(TipoAgendamento tipoAgendamento) throws ServiceException {
		Andamento andamento = new Andamento();
		if(TipoAgendamento.INDICE.equals(tipoAgendamento)){			
			//andamento = andamentoService.recuperarPorId(7600L);
			andamento.setId(ListaJulgamentoUI.TIPO_ANDAMENTO_LIBERACAO_MESA);
		}else{			
			//andamento = andamentoService.recuperarPorId(7601L);
			andamento.setId(ListaJulgamentoUI.TIPO_ANDAMENTO_LIBERACAO_PAUTA);
		}		
		return andamento;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public ListaJulgamento liberarListaParaJulgamento(DadosAgendamentoDto dadosAgendamento) throws ServiceException
	                                                                                             , ValidacaoLiberacaoParaJulgamentoException, ProcessoNaoPodeSerAgendadoException {		
		
		Ministro ministro = dadosAgendamento.getMinistro();
		Sessao sessao     = dadosAgendamento.getSessao();
		Integer codigoColegiado     = defineCapituloParaVerificacaoDaPauta(dadosAgendamento);		
		Integer ordemSessao         = recuperarMaiorOrdemSessaoListaJulgamento(sessao) + 1;
		Integer ordemSessaoMinistro = contagemDasSessoesNaoFinalizadas(dadosAgendamento);
		String nomeListaJulgamento  = definirNomeListaJulgamento(ordemSessaoMinistro);
		TipoListaJulgamento tipoListaJulgamento = dadosAgendamento.getTipoListaJulgamento();

		// Criar lista de julgamento
		ListaJulgamento listaJulgamento = null;
		listaJulgamento = new ListaJulgamento();
		listaJulgamento.setMinistro(ministro);
		listaJulgamento.setSessao(sessao);
		listaJulgamento.setNome(nomeListaJulgamento);
		listaJulgamento.setOrdemSessao(ordemSessao);
		listaJulgamento.setOrdemSessaoMinistro(ordemSessaoMinistro);
		listaJulgamento.setJulgado(Boolean.FALSE);
		listaJulgamento.setTipoListaJulgamento(tipoListaJulgamento);
		
		if (dadosAgendamento.getPreListaJulgamento() != null) {
			String cabecalho = dadosAgendamento.getPreListaJulgamento().getCabecalho();
			String nomePreListaJulgamento = dadosAgendamento.getPreListaJulgamento().getNome();
			TipoAgendamento tipoAgendamento = dadosAgendamento.getTipoAgendamento();
			Andamento andamentoEscolhido = this.getAndamentoEscolhido(tipoAgendamento);
			
			if (isPauta(dadosAgendamento.getTipoAgendamento())) {
				verificaFechamentoDaPauta(ministro, codigoColegiado);
			}
			
			listaJulgamento.setCabecalho(cabecalho);
			listaJulgamento.setNomePreLista(nomePreListaJulgamento);
			listaJulgamento.setAndamentoLiberacao(andamentoEscolhido);
		}
		
		// Por default, ap�s a libera��o da lista, o estado dela � EMAIL_NAO_ENVIADO
		listaJulgamento.setSituacaoListaJulgamento(SituacaoListaJulgamento.EMAIL_NAO_ENVIADO);
		
		listaJulgamento = listaJulgamentoService.salvar(listaJulgamento);
		
		List<ObjetoIncidente> objetosIncidentes = new ArrayList<ObjetoIncidente>();
		
		if (dadosAgendamento.getListaIncidentesDto() != null) {
			Long idListaProcesso = dadosAgendamento.getListaIncidentesDto().getId();
			ListaProcessos listaProcessos = listaProcessosService.recuperarPorId(idListaProcesso);
			objetosIncidentes.addAll(listaProcessos.getElementos());
		} else {
			Long idPrelista = dadosAgendamento.getPreListaJulgamento().getId();
			PreListaJulgamento preLista = preListaJulgamentoService.recuperarPorId(idPrelista);
			boolean ordenacaoNumerica = configuracaoSistemaService.isOrdenacaoNumerica();
			
			List<PreListaJulgamentoObjetoIncidente> listPreListaJulgamentoObjetosIncidentes = preLista.getObjetosIncidentes(ordenacaoNumerica);
			for (PreListaJulgamentoObjetoIncidente preListaObjetoIncidente : listPreListaJulgamentoObjetosIncidentes) {
				if (preListaObjetoIncidente.getRevisado()) {
					ObjetoIncidente oi = preListaObjetoIncidente.getObjetoIncidente();
					objetosIncidentes.add(oi);	
				}
			}
		}
		
		for (ObjetoIncidente<?> oi : objetosIncidentes) {
			// Valida��es
			verificaProcessoEmListaJulgamentoConjunto(oi);
			verificaProcessoEmListaJulgamentoPrevista(oi);
			verificaAgendamentoProcesso(oi, TipoColegiadoConstante.valueOfCodigoCapitulo(codigoColegiado), defineTipoDeAgendamento(dadosAgendamento));
			
			// Agendar o processo caso ele n�o esteja agendado
			gravaAgendamento(dadosAgendamento, oi, codigoColegiado);
			PreListaJulgamentoObjetoIncidente relacionamento = preListaJulgamentoService.recuperarPreListaJulgamentoObjetoIncidente(oi);
			
			// Adicionar processo � lista
			ProcessoListaJulgamento processoListaJulgamento = new ProcessoListaJulgamento();
			processoListaJulgamento.setListaJulgamento(listaJulgamento);
			processoListaJulgamento.setObjetoIncidente(oi);
			
			if (relacionamento != null ) {
				processoListaJulgamento.setUsuarioRevisor(relacionamento.getUsuarioRevisor());
				processoListaJulgamento.setDataRevisao(relacionamento.getDataRevisao());
				processoListaJulgamento.setMotivo(relacionamento.getMotivo());
				processoListaJulgamento.setObservacao(relacionamento.getObservacao());
			}
			
			processoListaJulgamento.setOrdemNaLista(objetosIncidentes.indexOf(oi)+1);
			processoListaJulgamentoService.salvar(processoListaJulgamento);
		}
		
		listaJulgamento = listaJulgamentoService.recuperarPorId(listaJulgamento.getId());
		
		return listaJulgamento;
	}
	
	
	/**
	 *  Retorno o n�mero de ordem da lista na sess�o POR ministro. Um mesmo ministro pode submeter
	 *  v�rias listas de processos para julgamento em uma mesma sess�o. Este n�mero de ordem
	 *  ser� incrementado pela aplica��o para cada ministro e n�o coincide com o valor armazenado 
	 *  em NUM_ORDEM_SESSAO, que � incrementado POR lista, independentemente do ministro que a submeteu.
	 *  
	 *  Para listas presenciais
	 *  Se lista do PLENO o numero da lista s� volta para 1 se todas as listas do ministro forem julgadas.
	 *  Se lista do TURMA o numero da lista s� volta para 1 em cada sess�o	 *  
	 *  
	 *  Para listas virtuais, o numero da lista s� volta para 1 em cada sess�o
	 *  
	 * @param dadosAgendamento
	 * @return
	 * @throws ServiceException
	 */
	Integer contagemDasSessoesNaoFinalizadas(DadosAgendamentoDto dadosAgendamento) throws ServiceException{		
		int maiorNumeroSessao = 0;
		Sessao sessaoAgendamento = dadosAgendamento.getSessao();
		String tipoAmbiente      = sessaoAgendamento.getTipoAmbiente();		
		if(TipoAmbienteConstante.VIRTUAL.getSigla().equals(tipoAmbiente)){
			maiorNumeroSessao = this.recuperarNumUltimaListaVirtual(dadosAgendamento,maiorNumeroSessao);			
		}else{
			maiorNumeroSessao = this.recuperarNumUltimaListaPresencial(dadosAgendamento,maiorNumeroSessao);			
		}
		return maiorNumeroSessao + 1;		
	}
	
	/**
	 * Retorna o n�mero da maior lista das sess�es VIRTUAIS
	 */
	int recuperarNumUltimaListaVirtual(DadosAgendamentoDto dadosAgendamento,int maiorNumeroSessao) throws ServiceException {		
		Sessao sessaoAgendada = dadosAgendamento.getSessao();
		maiorNumeroSessao = this.getMaiorNumeroSesso(dadosAgendamento,maiorNumeroSessao, sessaoAgendada);
		return maiorNumeroSessao;
	}

	int recuperarNumUltimaListaPresencial(DadosAgendamentoDto dadosAgendamento,int maiorNumeroSessao) throws ServiceException {
		List<Sessao> sessoesEmAberto = dadosAgendamento.getSessoesEmAberto();
		Sessao sessaoAgendada = dadosAgendamento.getSessao();
		Colegiado ColegiadoAgendamento = sessaoAgendada.getColegiado();
		boolean isListaPlenario = Colegiado.TRIBUNAL_PLENO.equals(ColegiadoAgendamento.getId());		
		
		if (sessoesEmAberto != null && sessoesEmAberto.size() > 0){
			if(isListaPlenario){
				for (Sessao sessao : sessoesEmAberto){
					maiorNumeroSessao = this.getMaiorNumeroSesso(dadosAgendamento,maiorNumeroSessao, sessao);
				}
			}else{
				maiorNumeroSessao = this.getMaiorNumeroSesso(dadosAgendamento,maiorNumeroSessao, sessaoAgendada);
			}
		}
		return maiorNumeroSessao;
	}

	Integer getMaiorNumeroSesso(DadosAgendamentoDto dadto, Integer maiorNumeroSessao, Sessao sessaoAgendada) throws ServiceException {
		int maiorNumeroSessaoAtual = this.recuperarMaiorOrdemSessaoMinistroListaJulgamento(sessaoAgendada, dadto.getMinistro());
		if (maiorNumeroSessaoAtual > maiorNumeroSessao){
			maiorNumeroSessao = maiorNumeroSessaoAtual;
		}
		return maiorNumeroSessao;
	}

	private Integer recuperarMaiorOrdemSessaoListaJulgamento(Sessao sessao) throws ServiceException {
		if(sessao == null){
			throw new ServiceException(MSG_ERRO_SESSAO_NAO_CARREGADA);
		}
		Integer maiorOrdemSessao = listaJulgamentoService.recuperarMaiorOrdemSessao(sessao);
		if (maiorOrdemSessao == null) {
			maiorOrdemSessao = 0;
		}
		return maiorOrdemSessao;
	}
	
	Integer recuperarMaiorOrdemSessaoMinistroListaJulgamento(Sessao sessao, Ministro ministro) throws ServiceException {
		Integer maiorOrdemSessao = listaJulgamentoService.recuperarMaiorOrdemSessaoMinistro(sessao, ministro);
		if (maiorOrdemSessao == null) {
			maiorOrdemSessao = 0;
		}
		return maiorOrdemSessao;
	}

	private String definirNomeListaJulgamento(Integer ordemSessao) {
		return "LISTA" + ordemSessao;
	}

	@Override
	public void verificaProcessoEmListaJulgamentoConjunto(ObjetoIncidente<?> objetoIncidente)
			throws ServiceException, ValidacaoLiberacaoParaJulgamentoException {
		List<ObjetoIncidente<?>> processosJulgamentoConjunto = informacaoPautaProcessoService.recuperarProcessosJulgamentoConjunto(objetoIncidente, false);
		if (processosJulgamentoConjunto != null && processosJulgamentoConjunto.size() > 0) {
			throw new ValidacaoLiberacaoParaJulgamentoException("Processo possui v�nculo de julgamento conjunto com outros processos.");
		}
	}

	@Override
	public void verificaProcessoEmListaJulgamentoPrevista(ObjetoIncidente<?> objetoIncidente)
			throws ServiceException, ValidacaoLiberacaoParaJulgamentoException {
		List<ListaJulgamento> listasJulgamento = listaJulgamentoService.pesquisar(objetoIncidente, true);
		if (listasJulgamento != null && listasJulgamento.size() > 0) {
			throw new ValidacaoLiberacaoParaJulgamentoException(MSG_LISTA_DE_JULGAMENTO_PREVISTA);
		}
	}

	@Override
	public void verificaAgendamentoProcesso(ObjetoIncidente<?> objetoIncidente
			                               ,TipoColegiadoConstante colegiado
			                               ,Integer codigoMateria) throws ServiceException
			                                                             ,ValidacaoLiberacaoParaJulgamentoException {
		this.verificaColegiadoAgendamento(objetoIncidente, colegiado);
		this.verificaMateriaAgendamento(objetoIncidente, codigoMateria);
	}

	@Override
	public void verificaColegiadoAgendamento(ObjetoIncidente<?> objetoIncidente, TipoColegiadoConstante colegiado) throws ValidacaoLiberacaoParaJulgamentoException
	                                                                                                                     ,ServiceException {
		Agendamento agendamento = this.getAgendamentoObjIncidente(objetoIncidente);
		Integer codigoCapitulo = colegiado.getCodigoCapitulo();
		if (agendamento != null && !agendamento.getId().getCodigoCapitulo().equals(codigoCapitulo)) {
			throw new ValidacaoLiberacaoParaJulgamentoException(MSG_PROCESSO_LIBERADO_OUTRO_COLEGIADO);
		}
	}
	
	@Override
	public void verificaMateriaAgendamento(ObjetoIncidente<?> objetoIncidente, Integer codigoMateria) throws ValidacaoLiberacaoParaJulgamentoException
																										   , ServiceException {
		Agendamento agendamento = this.getAgendamentoObjIncidente(objetoIncidente);
		if (agendamento != null && !agendamento.getId().getCodigoMateria().equals(codigoMateria)) {
			throw new ValidacaoLiberacaoParaJulgamentoException(MSG_PROCESSO_LIBERADO_OUTRA_MATERIA);
		}
	}

	public Agendamento getAgendamentoObjIncidente(ObjetoIncidente<?> objetoIncidente) throws ServiceException {
		List<Agendamento> agendamentos = agendamentoService.pesquisar(objetoIncidente);
		Agendamento agendamento = agendamentos != null && agendamentos.size() > 0 ? agendamentos.get(0) : null;
		return agendamento;
	}

	@Override
	public void verificaProcessoEmSessaoPrevista(ObjetoIncidente<?> oi) throws ServiceException, ValidacaoLiberacaoParaJulgamentoException {
		JulgamentoProcesso jp = julgamentoProcessoService.pesquisaSessaoNaoFinalizada(oi, TipoAmbienteConstante.PRESENCIAL);
		if (jp != null) {
			throw new ValidacaoLiberacaoParaJulgamentoException(MSG_PROCESSO_OUTRA_SESSAO_PREVISTA);
		}
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {ErroAoDeslocarProcessoException.class})
	public synchronized boolean deslocarProcesso(Processo processo, Setor setorDeDestino, Principal usuario)
			throws ErroAoDeslocarProcessoException {
		try {
			Setor setorDeOrigem = recuperaSetorDeOrigemDoProcesso(processo);
			
			if (isProcessoEmSetorDiferenteDoUsuario(setorDeOrigem, usuario)) {
				// Deslocar o processo para o gabinete do usu�rio
				DeslocaProcesso ultimoDeslocamento = deslocaProcessoService.recuperarUltimoDeslocamentoProcesso(processo.getSiglaClasseProcessual(), processo.getNumeroProcessual());

				Setor setorUsuarioMinistro = setorService.recuperarPorId(usuario.getMinistro().getSetor().getId());
				if (!setorUsuarioMinistro.getAtivo()) {
					throw new SetorInativoException("O setor " + setorUsuarioMinistro.getNome() + " est� inativo.");
				}

				Long codigoOrgaoOrigem = ultimoDeslocamento.getCodigoOrgaoDestino();
				Long codigoOrgaoDestino = setorUsuarioMinistro.getId();
				Integer tipoOrgaoOrigem = ultimoDeslocamento.getGuia().getTipoOrgaoDestino();
				deslocaProcessoService.deslocarProcesso(processo
						                               ,codigoOrgaoOrigem  // a origem do deslocamento � o destino do �ltimo deslocamento 
						                               ,codigoOrgaoDestino // o destino do deslocamento � o setor do ministro
						                               ,tipoOrgaoOrigem    // o tipo do �rg�o de origem � o tipo do �rg�o de destino na guia do �ltimo deslocamento
						                               ,DeslocaProcesso.TIPO_ORGAO_INTERNO); // o tipo do �rg�o de destino � o um �rg�o interno

				
				if(codigoOrgaoOrigem == 23){
					criarComunicacaoRequisaoProcesso(processo, setorDeDestino, usuario);
				}
				Thread.sleep(1000);
				setorDeOrigem = recuperaSetorDoProcesso(processo);
			}


			if (!setorDeDestino.getAtivo()) {
				throw new SetorInativoException("O setor " + setorDeDestino.getNome() + " est� inativo.");
			}

			if (!setorDeDestino.equals(usuario.getMinistro().getSetor())) {
				// Faz novo deslocamento para o setor desejado
				Long codigoOrgaoOrigem = setorDeOrigem.getId();
				Integer tipoOrgaoOrigem = DeslocaProcesso.TIPO_ORGAO_INTERNO;
				Long codigoOrgaoDestino = setorDeDestino.getId();
				Integer tipoOrgaoDestino = DeslocaProcesso.TIPO_ORGAO_INTERNO;
				deslocaProcessoService.deslocarProcesso(processo
													   ,codigoOrgaoOrigem
													   ,codigoOrgaoDestino
													   ,tipoOrgaoOrigem
													   ,tipoOrgaoDestino);

				Thread.sleep(1000);
			}

			return true;
		} catch (Exception e) {
			throw new ErroAoDeslocarProcessoException(e.getMessage(), e);
		}
	}
	
	/**
	 * @param processo
	 * @param setorDeDestino
	 * @param usuario
	 * @throws ServiceException
	 */
	public void criarComunicacaoRequisaoProcesso(Processo processo, Setor setorDeDestino, Principal usuario) throws ServiceException {
		Long numeroDocumento = tipoComunicacaoLocalService.gerarProximoNumeroComunicacao(123L);
		Collection<Long> objetoIncedentes = Arrays.asList(processo.getId());
		comunicacaoService.criarComunicacaoIntimacao(new Date(), setorDeDestino, usuario.getUsuario().getId(), 582242L, ModeloComunicacaoEnum.NOTIFICACA_REQUISICAO_PROCESSO ,objetoIncedentes, TipoFaseComunicacao.ENVIADO , null, null , numeroDocumento, MSG_COMUNICAO_REQUISAO_PROCESSO, null, null);
	}
	
	@Override
	public boolean validarProcessoParaDeslocamento(Processo processo) throws ServiceException, ErroNoSetorDoProcessoException, DeslocamentoNaoEncontradoException, SetorInativoException, InterruptedException {
		if (!isProcessoEletronico(processo)) {
			return false;
		}
		
		if (isSetorDeOrigemOrgaoExterno(processo) && isPossuiAndamentoImpedeDeslocamentoAutomatico(processo)) {
			return false;
		}
		
		return true;
	}
	
	private boolean isSetorDeOrigemOrgaoExterno(Processo processo) throws ErroNoSetorDoProcessoException, ServiceException, DeslocamentoNaoEncontradoException, SetorInativoException, InterruptedException {
		Setor setorDeOrigem = recuperaSetorDeOrigemDoProcesso(processo);
		return setorDeOrigem == null;
	}

	private boolean isPossuiAndamentoImpedeDeslocamentoAutomatico(Processo processo) throws ServiceException {
		List<AndamentoProcesso> andamentos = andamentoProcessoService.pesquisarAndamentoProcesso(processo.getSiglaClasseProcessual(), processo.getNumeroProcessual());
		
		for (AndamentoProcesso andamento : andamentos) {
			if (!andamento.getLancamentoIndevido() && andamentosQueImpedemDeslocamentoAutomatico.contains(Andamentos.valueOfCodigoAndamento(andamento.getCodigoAndamento()))){
				return true;
			}
		}
		
		return false;
	}

	private Boolean isProcessoEletronico(Processo processo) {
		if ( processo.getTipoMeioProcesso().equals( TipoMeioProcesso.ELETRONICO ) ) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private Setor recuperaSetorDeOrigemDoProcesso(Processo processoDoTexto)
			throws ErroNoSetorDoProcessoException, ServiceException, DeslocamentoNaoEncontradoException, SetorInativoException, InterruptedException {
		return recuperaSetorDoProcesso(processoDoTexto);
	}
	
	private boolean isProcessoEmSetorDiferenteDoUsuario(Setor setorDoProcesso, Principal principal) {
		if (principal == null) {
			throw new IllegalArgumentException(
					"Os dados do usu�rio n�o foram informados para a A��o Liberar para Publica��o!");
		}
		// Se o setor do processo estiver nulo, quer dizer que foi feito um
		// deslocamento para um setor externo.
		// Se o processo estiver deslocado para o NAAR (N�cleo de An�lise de Agravos Regimentais), 
		// deve ser considerado que o processo est� na presid�ncia.
		return setorDoProcesso == null || principal.getMinistro() == null
				|| (!setorDoProcesso.equals(principal.getMinistro().getSetor()) && !isProcessoNAAREUsuarioPresidencia(setorDoProcesso, principal));
	}
	
	private boolean isProcessoNAAREUsuarioPresidencia(Setor setorDoProcesso, Principal principal) {
		return setorDoProcesso.getId().equals(Setor.CODIGO_NUCLEO_ANALISE_AGRAVOS_REGIMENTAIS) && principal.getMinistro().getSetor().getId().equals(Setor.CODIGO_SETOR_PRESIDENCIA);
	}
	
	private Setor recuperaSetorDoProcesso(Processo processoDoTexto) throws ErroNoSetorDoProcessoException
	                                                                      ,ServiceException
	                                                                      ,DeslocamentoNaoEncontradoException {
		DeslocaProcesso deslocaProcesso = deslocaProcessoService.recuperarUltimoDeslocamentoProcesso(
						processoDoTexto.getSiglaClasseProcessual(),
						processoDoTexto.getNumeroProcessual());
		if (deslocaProcesso == null) {
			throw new DeslocamentoNaoEncontradoException(
					"N�o foi poss�vel encontrar o �ltimo deslocamento do processo!");
		}
		if (deslocaProcesso.getCodigoOrgaoDestino() == null) {
			throw new ErroNoSetorDoProcessoException(
					montaMensagemDeProcessoEmSetorDiferente(processoDoTexto));
		}
		return setorService.recuperarPorId(deslocaProcesso.getCodigoOrgaoDestino());
	}
	
	private String montaMensagemDeProcessoEmSetorDiferente(Processo processoDoTexto) {
		return "O processo " + processoDoTexto.getSiglaClasseProcessual() + " " + processoDoTexto.getNumeroProcessual()
				+ " n�o se encontra no setor do usu�rio!";
	}

	@Override
	public Ministro recuperarMinistroRevisorIncidente(ObjetoIncidente<?> objetoIncidente) {
		try {
			return ministroService.recuperarMinistroRevisorIncidente(objetoIncidente.getId());
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}
	
	@Override
	public boolean isProcessoApenso(Processo processo) throws ServiceException {
		return processoDependenciaService.isApenso(processo);
	}
	
	@Override
	public Processo recuperarProcessoApensante(Processo processo) throws ServiceException {
		ProcessoDependencia apensamento = processoDependenciaService.getProcessoVinculador(processo); 
		return processoService.recuperarProcesso(apensamento.getClasseProcessoVinculador(), apensamento.getNumeroProcessoVinculador());
	}

	@Override
	public void validarSituacaoDeJulgamento(ObjetoIncidente<?> oi) throws ProcessoPrecisaDeConfirmacaoException {
		try {
			if (objetoIncidenteService.recuperarSituacaoJulgamentoIncidente(oi.getId()).equals(SituacaoIncidenteJulgadoOuNao.JULGADO)) {
				throw new ProcessoPrecisaDeConfirmacaoException("Esse processo j� foi julgado.");
			}
		} catch (ServiceException e) {
			throw new NestedRuntimeException(e);
		}
	}

	@Override
	public SituacaoIncidenteJulgadoOuNao recuperarSituacaoJulgamentoIncidente(Long id) throws ServiceException {
		return objetoIncidenteService.recuperarSituacaoJulgamentoIncidente(id);
	}

	/**
	 * Se o Objeto Indicente for diferente de Agravo Regimental e Embargo de Declara��o, gera Exception
	 * @param oi
	 */
	@Override
	public void validarProcessosParaJulgamentoVirtual(ObjetoIncidente<?> oi) throws ProcessoTipoRecursoPodePlanarioVirtualException {
		TipoRecurso tipoRecurso = this.ObjetoIncidenteUtilGetTipoRecurso(oi);
		if(tipoRecurso == null){
			throw new ProcessoTipoRecursoPodePlanarioVirtualException(MSG_TIPO_RECURSO_PLENARIO_VIRTUAL);
		}
		Long idTipoRecurso = tipoRecurso.getId();			
		Long idAgravoRegimental   = 4L;
		Long idEmbargoDeclaracao  = 15L;
		Long idSegundoJulgamento  = 82L;
		Long idTerceiroJulgamento = 210L;
		Long idJulgamento = 83L;
		boolean tipoRecursoJulgamentoVirtual = (idAgravoRegimental.equals(idTipoRecurso) || idEmbargoDeclaracao.equals(idTipoRecurso) ||
				idJulgamento.equals(idTipoRecurso) || idSegundoJulgamento.equals(idTipoRecurso) || idTerceiroJulgamento.equals(idTipoRecurso)); 
		if(!tipoRecursoJulgamentoVirtual){
			throw new ProcessoTipoRecursoPodePlanarioVirtualException(MSG_TIPO_RECURSO_PLENARIO_VIRTUAL);
		}
		
		if (!temEmentaRelatorioEVotoRevisados(oi))
			throw new ProcessoTipoRecursoPodePlanarioVirtualException(MSG_PROCESSO_SEM_EMENTA_RELATORIO_OU_VOTO);		
	}

	public boolean temEmentaRelatorioEVotoRevisados(ObjetoIncidente<?> oi) {
		boolean temEmentaRelatorioEVotoRevisados = false;
		List<FaseTexto> listaFasesProibidas = Arrays.asList(FaseTexto.EM_ELABORACAO, FaseTexto.EM_REVISAO, FaseTexto.CANCELADO, FaseTexto.NAO_ELABORADO);
		try {
			// Consulta se o processo tem algum relat�rio
			List<Texto> lista = textoService.pesquisar(oi, TipoTexto.EMENTA, TipoTexto.RELATORIO, TipoTexto.VOTO);
			
			boolean temEmenta = false;
			boolean temRelatorio = false;
			boolean temVoto = false;
			
			for (Texto texto : lista) {
				if (!temEmenta && TipoTexto.EMENTA.equals(texto.getTipoTexto()) && !listaFasesProibidas.contains(texto.getTipoFaseTextoDocumento()))
					temEmenta = true;
				
				if (!temRelatorio && TipoTexto.RELATORIO.equals(texto.getTipoTexto()) && !listaFasesProibidas.contains(texto.getTipoFaseTextoDocumento()))
					temRelatorio = true;
				
				if (!temVoto && TipoTexto.VOTO.equals(texto.getTipoTexto()) && !listaFasesProibidas.contains(texto.getTipoFaseTextoDocumento()))
					temVoto = true;
				
				if (temEmenta && temRelatorio && temVoto) {
					temEmentaRelatorioEVotoRevisados = true;
					break;
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return temEmentaRelatorioEVotoRevisados;
	}

	/***
	 * Metodo criado para facilitar o teste Unitario com o Mockito
	 * @param oi
	 * @return
	 **/
	public TipoRecurso ObjetoIncidenteUtilGetTipoRecurso(ObjetoIncidente<?> oi) {
		TipoRecurso tipoRecurso = ObjetoIncidenteUtil.getTipoRecurso(oi);
		return tipoRecurso;
	}
	
	/***
	 * Metodo criado para facilitar o teste Unitario com o Mockito
	 * @param oi
	 * @return
	 **/	
	public Processo objetoIncidenteUtilGetProcesso(ObjetoIncidente<?> oi) {
		Processo processo = ObjetoIncidenteUtil.getProcesso(oi); 
		return processo;
	}
	
	@Override
	public boolean temOABParaTodosOsRepresentantes(ObjetoIncidente<?> processo) {
		ObjetoIncidente<?> oi = recuperarObjetoIncidentePorId(processo.getId());
		
		if (oi != null && oi.getPrincipal() != null && oi.getPrincipal() instanceof Processo) {
			Hibernate.initialize(((Processo)oi.getPrincipal()).getClasseProcessual());
			
			//Habeas Corpus n�o precisa de representante
			if (Classe.SIGLA_HABEAS_CORPUS.equals(((Processo)oi.getPrincipal()).getClasseProcessual().getDescricao()))
				return true;			
		}
		if (oi != null && oi.getPrincipal() != null && oi.getPrincipal() instanceof Processo) {
		for (Parte parte : oi.getPartesVinculadas())
			if (Categoria.COD_CATEGORIA_ADVOGADO.equals(parte.getCategoria().getId()))
				if (parte.getJurisdicionado() == null || parte.getJurisdicionado().getOab() == null || parte.getJurisdicionado().getOab().isEmpty())
					return false;
		}
		return true;
	}
}
