package br.jus.stf.estf.decisao.mobile.assinatura.support;

public class AssinaturaDocumentoDto extends BasicAssinadorRestDto {

	private static final long serialVersionUID = -2534711568004210652L;

	private int quantidadeTextosAssinados;
	private int quantidadeComunicacoesAssinadas;

	public int getQuantidadeTextosAssinados() {
		return quantidadeTextosAssinados;
	}

	public void setQuantidadeTextosAssinados(int quantidadeTextosAssinados) {
		this.quantidadeTextosAssinados = quantidadeTextosAssinados;
	}

	public int getQuantidadeComunicacoesAssinadas() {
		return quantidadeComunicacoesAssinadas;
	}

	public void setQuantidadeComunicacoesAssinadas(int quantidadeComunicacoesAssinadas) {
		this.quantidadeComunicacoesAssinadas = quantidadeComunicacoesAssinadas;
	}

}
