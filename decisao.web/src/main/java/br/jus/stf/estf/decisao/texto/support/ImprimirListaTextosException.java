/**
 * 
 */
package br.jus.stf.estf.decisao.texto.support;

/**
 * @author Paulo.Estevao
 * @since 01.09.2010
 */
public class ImprimirListaTextosException extends Exception {

	public ImprimirListaTextosException() {
		super();
	}

	public ImprimirListaTextosException(String message, Throwable cause) {
		super(message, cause);
	}

	public ImprimirListaTextosException(String message) {
		super(message);
	}

	public ImprimirListaTextosException(Throwable cause) {
		super(cause);
	}
}
