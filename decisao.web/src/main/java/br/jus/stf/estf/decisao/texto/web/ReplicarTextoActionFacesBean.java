/**
 * 
 */
package br.jus.stf.estf.decisao.texto.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.gov.stf.estf.entidade.documento.ControleVoto;
import br.gov.stf.estf.entidade.documento.Texto;
import br.gov.stf.estf.entidade.documento.TipoTexto;
import br.gov.stf.estf.entidade.documento.tipofase.FaseTexto;
import br.gov.stf.estf.entidade.julgamento.Colegiado;
import br.gov.stf.estf.entidade.julgamento.ListaJulgamento;
import br.gov.stf.estf.entidade.julgamento.Sessao;
import br.gov.stf.estf.entidade.julgamento.Sessao.TipoAmbienteConstante;
import br.gov.stf.estf.entidade.ministro.Ministro;
import br.gov.stf.estf.entidade.processostf.ObjetoIncidente;
import br.gov.stf.estf.entidade.processostf.Processo;
import br.gov.stf.estf.entidade.usuario.Responsavel;
import br.gov.stf.estf.julgamento.model.service.ColegiadoService;
import br.gov.stf.estf.julgamento.model.service.ListaJulgamentoService;
import br.gov.stf.estf.julgamento.model.service.SessaoService;
import br.gov.stf.estf.julgamento.model.util.ListaJulgamentoSearchData;
import br.gov.stf.estf.ministro.model.service.MinistroService;
import br.gov.stf.estf.processostf.model.service.SituacaoMinistroProcessoService;
import br.gov.stf.estf.processostf.model.service.TipoRecursoService;
import br.gov.stf.estf.usuario.model.service.UsuarioService;
import br.gov.stf.framework.model.entity.TipoSexo;
import br.gov.stf.framework.model.service.ServiceException;
import br.gov.stf.framework.util.SearchResult;
import br.jus.stf.estf.decisao.objetoincidente.service.ObjetoIncidenteService;
import br.jus.stf.estf.decisao.pesquisa.domain.ObjetoIncidenteDto;
import br.jus.stf.estf.decisao.pesquisa.domain.TextoDto;
import br.jus.stf.estf.decisao.pesquisa.web.texto.TextoFacesBean;
import br.jus.stf.estf.decisao.support.action.handlers.CheckRestrictions;
import br.jus.stf.estf.decisao.support.action.handlers.RequiresResources;
import br.jus.stf.estf.decisao.support.action.handlers.RequiresResources.Mode;
import br.jus.stf.estf.decisao.support.action.handlers.Restrict;
import br.jus.stf.estf.decisao.support.action.handlers.States;
import br.jus.stf.estf.decisao.support.action.support.Action;
import br.jus.stf.estf.decisao.support.action.support.ActionIdentification;
import br.jus.stf.estf.decisao.support.action.support.ActionSupport;
import br.jus.stf.estf.decisao.texto.service.TextoService;
import br.jus.stf.estf.decisao.texto.support.ManterListaDeTextosException;
import br.jus.stf.estf.decisao.texto.support.ProcessoInvalidoParaListaDeTextosException;


/**
 * @author Paulo.Estevao
 * @since 14.07.2010
 *
 */
@Action(id="replicarTextoActionFacesBean", 
		name="Replicar Texto", 
		view="/acoes/texto/replicar.xhtml",
		height=490)
@Restrict({ActionIdentification.CRIAR_TEXTO_SEMELHANTE})
@RequiresResources(Mode.One)
@CheckRestrictions
@States({ FaseTexto.EM_ELABORACAO, FaseTexto.EM_REVISAO, FaseTexto.REVISADO, FaseTexto.LIBERADO_ASSINATURA, FaseTexto.ASSINADO, FaseTexto.LIBERADO_PUBLICACAO, FaseTexto.PUBLICADO, FaseTexto.JUNTADO})
public class ReplicarTextoActionFacesBean extends
		ActionSupport<TextoDto> {

	private static final String PAGINA_LISTA_PROCESSOS = "listaDeProcessos";
	
	private static final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	
	@Qualifier("textoServiceLocal")
	@Autowired
	private TextoService textoService;
	@Autowired
	private MinistroService ministroService;
	@Autowired
	private TipoRecursoService tipoRecursoService;
	@Autowired
	private SituacaoMinistroProcessoService situacaoMinistroProcessoService;
	@Qualifier("objetoIncidenteServiceLocal")
	@Autowired
	private ObjetoIncidenteService objetoIncidenteService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private ColegiadoService colegiadoService;
	@Autowired
	private SessaoService sessaoService;	
	@Autowired
	private ListaJulgamentoService listaJulgamentoService;
	
	private Long idTipoTexto; 
	private Responsavel responsavel;
	private TextoDto texto;
	private String identificacaoProcesso;
	
	private List<SelectItem> listaColegiado;	
	private List<SelectItem> listaSessoes;
	private String idColegiadoPesquisa;
	private Long idRelatorPesquisa;
	private Long idSessaoPesquisa;
	
	private HashMap<String,List<SelectItem>> mapaRelatoresPorColegiado;
	private HashMap<String,List<SelectItem>> mapaSessoesPorColegiado;
	
	private List<ListaJulgamentoSelecionadaLista> listasJulgamentoPesquisadas;
	
	private Collection<ObjetoIncidenteDto> processosParaSelecao;

	private String errorTitle;
	
	@Override
	public void load() {
		// Define o respons�vel padr�o...
		responsavel = getUsuario();
		
		if(getTiposTexto().contains(getTextoSelecionado().getTipoTexto())) {
			idTipoTexto = getTextoSelecionado().getTipoTexto().getCodigo();
		} else {
			idTipoTexto = TipoTexto.DESPACHO.getCodigo();
		}
		texto = new TextoDto();
		texto.setId(getTextoSelecionado().getId());
		texto.setObservacao(getTextoSelecionado().getObservacao());
		texto.setIdUsuarioInclusao(getUsuario().getId());
		
		// Sempre seta como N�O porque o texto est� na fase EM ELABORA��O
		texto.setLiberacaoAntecipada(false);
		
		if (TextoFacesBean.listaTipoTextoDisponiveis.contains(texto.getTipoTexto()))
			texto.setTipoVoto(getTextoSelecionado().getTipoVoto());
	}
	
	private void adicionaProcessoNaTabela(ObjetoIncidenteDto objetoIncidente) throws ServiceException {
		getProcessosParaSelecao().add(objetoIncidente);
	}

	public void incluirProcessoSelecionado(ObjetoIncidenteDto objetoIncidenteSelecionado) {
		try {
			if (objetoIncidenteSelecionado == null) {
				throw new ProcessoInvalidoParaListaDeTextosException("Selecione um processo para inclus�o na lista!");
			}
			if (isProcessoNaLista(objetoIncidenteSelecionado)) {
				addInformation("O processo selecionado j� faz parte da lista!");
			} else {
				validaTextoProcessoDestino(TipoTexto.valueOf(idTipoTexto), objetoIncidenteSelecionado, getMinistro());
				insereProcessoSelecionado(objetoIncidenteSelecionado, getTextoSelecionado(), TipoTexto.valueOf(idTipoTexto));
			}
		} catch (ProcessoInvalidoParaListaDeTextosException e) {
			addInformation(e.getMessage());
		} catch (Exception e) {
			addError(e.getMessage());
		}	

		identificacaoProcesso = null;
	}

	private void insereProcessoSelecionado(ObjetoIncidenteDto objetoIncidente, TextoDto texto, TipoTexto tipoTextoDestino) throws ServiceException, ProcessoInvalidoParaListaDeTextosException {
		Ministro ministroDoGabinete = getMinistro();
		verificaTextoRegistrado(tipoTextoDestino, ministroDoGabinete, objetoIncidente);
		verificaControleDeVotos(texto, ministroDoGabinete, objetoIncidente, tipoTextoDestino);
		adicionaProcessoNaTabela(objetoIncidente);
	}

	private boolean isProcessoNaLista(ObjetoIncidenteDto objetoIncidente) {
		if (objetoIncidente != null) {
			return isColecoesContemProcesso(objetoIncidente);
		}
		throw new RuntimeException("Ocorreu um erro ao recuperar o processo informado! Por favor, tente novamente!");
	}

	private boolean isColecoesContemProcesso(ObjetoIncidenteDto objetoIncidente) {
		return getProcessosParaSelecao().contains(objetoIncidente);
	}

	private void verificaControleDeVotos(TextoDto texto, Ministro ministroDoGabinete, ObjetoIncidenteDto objetoIncidente, TipoTexto tipoTextoDestino)
			throws ServiceException, ProcessoInvalidoParaListaDeTextosException {
		if (precisaValidarControleDeVotos(ministroDoGabinete, objetoIncidente, tipoTextoDestino)) {
			validaControleDeVotos(texto, ministroDoGabinete, objetoIncidente);
		}
	}

	private boolean precisaValidarControleDeVotos(Ministro ministroDoGabinete, ObjetoIncidenteDto objetoIncidente, TipoTexto tipoTextoDestino)
			throws ServiceException {
		
		return  !TipoTexto.DESPACHO.equals(tipoTextoDestino) && !TipoTexto.DECISAO_MONOCRATICA.equals(tipoTextoDestino) && !TipoTexto.VOTO_VOGAL.equals(tipoTextoDestino) &&
					(!(isMinistroDoSetorRelatorDoProcesso(ministroDoGabinete, objetoIncidente) || getMinistroService()
					.isMinistroTemRelatoriaDaPresidencia(ministroDoGabinete, (Processo) objetoIncidenteService.recuperarObjetoIncidentePorId(objetoIncidente.getId()).getPrincipal())));
	}

	private void validaControleDeVotos(TextoDto texto, Ministro ministroDoGabinete, ObjetoIncidenteDto objetoIncidente)
			throws ServiceException, ProcessoInvalidoParaListaDeTextosException {
		ControleVoto controleDeVoto = textoService.consultaControleDeVotoDoProcesso(getTipoTexto(), ministroDoGabinete, objetoIncidente);
		if (controleDeVoto == null && !TipoTexto.DESPACHO.equals(texto.getTipoTexto()) &&
				!TipoTexto.DECISAO_MONOCRATICA.equals(texto.getTipoTexto()) && !TipoTexto.VOTO_VOGAL.equals(texto.getTipoTexto())) {
			throw new ProcessoInvalidoParaListaDeTextosException(montaMensagemDeProcessoDeOutroRelator(objetoIncidente));
		}
	}

	private void verificaTextoRegistrado(TipoTexto tipoTexto, Ministro ministroDoGabinete, ObjetoIncidenteDto objetoIncidente)
			throws ServiceException, ProcessoInvalidoParaListaDeTextosException {
		if ((tipoTexto.equals(TipoTexto.ACORDAO) || tipoTexto.equals(TipoTexto.EMENTA)
				|| tipoTexto.equals(TipoTexto.RELATORIO) || tipoTexto.equals(TipoTexto.VOTO))
				&& textoService.existeTextoRegistradoParaProcesso(objetoIncidente, tipoTexto, ministroDoGabinete) ) {
			throw new ProcessoInvalidoParaListaDeTextosException(montaMensagemDeErroDeTextoRegistrado(objetoIncidente));
		}
	}

	private String montaMensagemDeProcessoDeOutroRelator(ObjetoIncidenteDto objetoIncidente) {
		StringBuilder sb = new StringBuilder();
		sb.append("O processo ");
		sb.append(objetoIncidenteService.recuperarObjetoIncidentePorId(objetoIncidente.getId()).getPrincipal().getIdentificacaoCompleta());
		sb.append(" pertence a outro relator.");
		return sb.toString();
	}

	private boolean isMinistroDoSetorRelatorDoProcesso(Ministro ministroDoGabinete, ObjetoIncidenteDto objetoIncidente)
			throws ServiceException {
		return getMinistroService().isMinistroRelatorDoProcesso(ministroDoGabinete, (Processo) objetoIncidenteService.recuperarObjetoIncidentePorId(objetoIncidente.getId()).getPrincipal());
	}

	private String montaMensagemDeErroDeTextoRegistrado(ObjetoIncidenteDto objetoIncidente) {
		StringBuilder sb = new StringBuilder();
		sb.append(objetoIncidente.getIdentificacao());
		sb.append(": Texto j� registrado para este processo.");
		return sb.toString();
	}

	public void validateAndExecute() {
		cleanMessages();
		if( processosParaSelecao != null && processosParaSelecao.size() > 0 ) {
			if (responsavel == null) {
				addInformation("O respons�vel deve ser informado.");
			} else {
				texto.setResponsavel(responsavel.getId().toString());
			}
			texto.setTipoTexto(TipoTexto.valueOf(idTipoTexto));
			
			for (ObjetoIncidenteDto processo : processosParaSelecao) {
				try {
					validaTextoProcessoDestino(texto.getTipoTexto(), processo, getMinistro());
				} catch (ManterListaDeTextosException e) {
					addError(e.getMessage());
				}
			}
						
			if (!hasMessages()) {
				execute();
			} else if (hasErrors()) {
				sendToErrors();
			}
		}
	}

	private void execute() {
		cleanMessages();
		try {
			textoService.criarListaTextosReplicados(texto, processosParaSelecao, getMinistro());
		} catch (Exception e) {
			addError(e.getMessage());
		}
		
		setRefresh(true);

		if (!hasMessages()) {
			sendToConfirmation();
		} else {
			setErrorTitle("Erro ao gravar lista de textos.");
			sendToErrors();
		}
	}
	
	private void setErrorTitle(String error) {
		errorTitle = error;		
	}
	
	@Override
	public String getErrorTitle() {
		return errorTitle;
	}

	private void validaTextoProcessoDestino(TipoTexto tipoTexto, ObjetoIncidenteDto objetoIncidente, Ministro ministro) throws ManterListaDeTextosException {
		if (tipoTexto.equals(TipoTexto.ACORDAO) || tipoTexto.equals(TipoTexto.RELATORIO) || tipoTexto.equals(TipoTexto.EMENTA)) {
			for (TextoDto textoObjetoIncidente : textoService.recuperarTextos(objetoIncidenteService.recuperarObjetoIncidentePorId(objetoIncidente.getId()), false)) {
				if (textoObjetoIncidente.getTipoTexto().equals(tipoTexto)) {
					throw new ManterListaDeTextosException("O Texto com o tipo [" + tipoTexto.getDescricao() + "] j� existe no processo " + objetoIncidente.getIdentificacao() + ".");
				}
			}
		} else if (tipoTexto.equals(TipoTexto.VOTO) || tipoTexto.equals(TipoTexto.VOTO_VOGAL)) {
			for (TextoDto textoObjetoIncidente : textoService.recuperarTextos(objetoIncidenteService.recuperarObjetoIncidentePorId(objetoIncidente.getId()), false)) {
				//Se for um voto e for do mesmo ministro, n�o deve deixar que o texto seja replicado (S� pode haver 1)
				if (textoObjetoIncidente.getTipoTexto().equals(tipoTexto) && ministro.getId().equals(textoObjetoIncidente.getIdMinistro())) {
						String mensagem =  String.format("O Texto com o tipo [%s] j� existe no processo %s para %s %s.",tipoTexto.getDescricao(), objetoIncidente.getIdentificacao(), getArtigoDoMinistro(ministro),ministro.getNome() );
						throw new ManterListaDeTextosException(mensagem);
					} 
				}
			}
		} 
		
	/**
	 * Verifica qual o artigo definido deve ser usado. Se for uma ministra, volta "a". Caso contr�rio, volta "o" 
	 * @param ministro
	 * @return
	 */
	private String getArtigoDoMinistro(Ministro ministro) {
		if (ministro.getTipoSexo().equals(TipoSexo.FEMININO)){
			return "a";
		}
		return "o";
	}

	public boolean isTextoDoProcesso(Texto textoGravado, ObjetoIncidente<Processo> objetoIncidente) throws ServiceException {
		return objetoIncidente.getId().equals(textoGravado.getObjetoIncidente().getId());
	}

	public void excluirProcessosSelecionados() {
		Collection<ObjetoIncidenteDto> processos = getProcessosParaSelecao();
		if(processos != null && processos.size()> 0) {
			Collection<ObjetoIncidenteDto> processosParaRetirar = new ArrayList<ObjetoIncidenteDto>();
			for (ObjetoIncidenteDto processo : processos) {
				if (processo.isSelected()) {
					processosParaRetirar.add(processo);
				}
			}
			processos.removeAll(processosParaRetirar);
		}
	}
	
	public void selectAll() {
		boolean check = !allChecked();
		for (ObjetoIncidenteDto processo : processosParaSelecao) {
			processo.setSelected(check);
		}
	}
	
	private boolean allChecked() {
    	for (ObjetoIncidenteDto dto : processosParaSelecao) {
    		if (!dto.isSelected()) {
    			return false;
    		}
    	}
    	return true;
    }

	public void voltarParaListaProcessos() {
		getDefinition().setFacet(PAGINA_LISTA_PROCESSOS);
	}
	
	public Collection<ObjetoIncidenteDto> getProcessosParaSelecao() {
		if (processosParaSelecao == null) {
			processosParaSelecao = new ArrayList<ObjetoIncidenteDto>();
		}
		return processosParaSelecao;
	}
	
	public List<TipoTexto> getTiposTexto() {
		List<TipoTexto> tipos = new ArrayList<TipoTexto>();
		
		tipos.add(TipoTexto.DECISAO_MONOCRATICA);
		tipos.add(TipoTexto.DESPACHO);
		tipos.add(TipoTexto.ACORDAO);
		tipos.add(TipoTexto.RELATORIO);
		tipos.add(TipoTexto.EMENTA);
		tipos.add(TipoTexto.VOTO);
		tipos.add(TipoTexto.VOTO_VOGAL);
		tipos.add(TipoTexto.OFICIO);
		tipos.add(TipoTexto.MINUTA);
		tipos.add(TipoTexto.MEMORIA_DE_CASO);
	
		if( tipos != null && tipos.size() > 0 )
			Collections.sort(tipos);
		
		return tipos;
	}

	public TipoRecursoService getTipoRecursoService() {
		return tipoRecursoService;
	}

	public void setTipoRecursoService(TipoRecursoService tipoRecursoService) {
		this.tipoRecursoService = tipoRecursoService;
	}

	public SituacaoMinistroProcessoService getSituacaoMinistroProcessoService() {
		return situacaoMinistroProcessoService;
	}

	public void setSituacaoMinistroProcessoService(SituacaoMinistroProcessoService situacaoMinistroProcessoService) {
		this.situacaoMinistroProcessoService = situacaoMinistroProcessoService;
	}

	private MinistroService getMinistroService() {
		return ministroService;
	}

	public TextoDto getTextoSelecionado() {
		if (getResources() != null && getResources().size() == 1) {
			return (TextoDto) getResources().toArray()[0];
		}
		return null;
	}

	public String getIdentificacaoDoTexto() {
		try {
			return getTextoSelecionado().toString();
		} catch (IllegalArgumentException e) {
			return "";
		}
	}
	
	public Responsavel getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Responsavel responsavel) {
		this.responsavel = responsavel;
	}

	public Long getIdTipoTexto() {
		return idTipoTexto;
	}

	public void setIdTipoTexto(Long idTipoTexto) {
		this.idTipoTexto = idTipoTexto;
	}

	public TextoDto getTexto() {
		return texto;
	}

	public void setTexto(TextoDto texto) {
		this.texto = texto;
	}

	public String getIdentificacaoProcesso() {
		return identificacaoProcesso;
	}

	public void setIdentificacaoProcesso(String identificacaoProcesso) {
		this.identificacaoProcesso = identificacaoProcesso;
	}

	private TipoTexto getTipoTexto() {
		return TipoTexto.valueOf(idTipoTexto);
	}
	
	public void alterarTipoTexto(ValueChangeEvent event) {
		idTipoTexto = (Long) event.getNewValue();
	}
	
	public List<SelectItem> getListaColegiado(){
		if (listaColegiado != null)
			return listaColegiado;
		listaColegiado = new ArrayList<SelectItem>();
		try{
			SelectItem branco = new SelectItem("0", "");
			List<Colegiado> colegiados = colegiadoService.pesquisar();
			listaColegiado.add(branco);
			for (Colegiado colegiado : colegiados){
				listaColegiado.add(new SelectItem(colegiado.getId(), colegiado.getDescricao()));
			}
		}catch(ServiceException e){
			addError("Problema ao recuperar rela��o de colegiados: " + e.getMessage());
		}
		return listaColegiado;				
	}
	
	public void setListaColegiado(List<SelectItem> lista){
		this.listaColegiado = lista;
	}	
	
	@SuppressWarnings("unchecked")
	public List<SelectItem> getListaRelator() {		
		if (mapaRelatoresPorColegiado == null)
			mapaRelatoresPorColegiado = new HashMap<String, List<SelectItem>>();
		if (mapaRelatoresPorColegiado.get(idColegiadoPesquisa) != null)
			return mapaRelatoresPorColegiado.get(idColegiadoPesquisa);
		
		List<Ministro> relatores = pesquisarRelator(idColegiadoPesquisa);
		List<SelectItem> listaRetorno = new ArrayList<SelectItem>();
		listaRetorno.add(new SelectItem("0", ""));
		for (Ministro relator : relatores){
			listaRetorno.add(new SelectItem(relator.getId(), relator.getNome()));
		}
		Collections.sort(listaRetorno, new Comparator<SelectItem>(){

			@Override
			public int compare(SelectItem s1, SelectItem s2) {
				return s1.getLabel().compareTo(s2.getLabel());
			}

			
			
		});
		mapaRelatoresPorColegiado.put(idColegiadoPesquisa, listaRetorno);
		if(idColegiadoPesquisa.equals("0"))
			mapaRelatoresPorColegiado.put(Colegiado.TRIBUNAL_PLENO, listaRetorno); //a lista para o plen�rio deve incluir todos os ministros, sem necessidade de filtro		
		return listaRetorno;
	}
	
	private List<Ministro> pesquisarRelator(String idColegiado){
		Boolean primeiraTurma = Boolean.FALSE, segundaTurma = Boolean.FALSE, plenario = Boolean.FALSE;
		if (idColegiado == null)
			idColegiado = "0";
		if (idColegiado.equals(Colegiado.PRIMEIRA_TURMA))
			primeiraTurma = Boolean.TRUE;
		else if (idColegiado.equals(Colegiado.SEGUNDA_TURMA))
			segundaTurma = Boolean.TRUE;
		else if (idColegiado.equals(Colegiado.TRIBUNAL_PLENO))
			plenario = Boolean.TRUE;
		
		try{
			List<Ministro> ministros = ministroService.pesquisarMinistros(Boolean.TRUE, Boolean.TRUE, primeiraTurma, segundaTurma, plenario);
			return ministros;
		}catch(ServiceException e){
			addError("Erro ao recuperar rela��o de ministros");
			return new ArrayList<Ministro>();
			
		}
			
	}
	
	public List<SelectItem> getListaSessoes(){		
		if (mapaSessoesPorColegiado == null)
			mapaSessoesPorColegiado = new HashMap<String, List<SelectItem>>();
		if (mapaSessoesPorColegiado.get(idColegiadoPesquisa) != null)
			return mapaSessoesPorColegiado.get(idColegiadoPesquisa);

		String tipoColegiado = idColegiadoPesquisa;
		if (idColegiadoPesquisa.equals("0"))
			tipoColegiado = null;
		
		
		List<SelectItem> listaRetorno = new ArrayList<SelectItem>();
		listaRetorno.add(new SelectItem("0", ""));
		try {
			List<Sessao> sessoes = sessaoService.pesquisarSessao(null, null, TipoAmbienteConstante.VIRTUAL, null, tipoColegiado);						
			List<SelectItem> listaAux = formatarOrdenarSessoesVirtuais(sessoes);
			listaRetorno.addAll(listaAux);
			mapaSessoesPorColegiado.put(idColegiadoPesquisa, listaRetorno);
		} catch (ServiceException e) {
			addError("Erro ao recuperar rela��o de sess�es.");		
		}
								
		return listaRetorno;
	}
	
	private List<SelectItem> formatarOrdenarSessoesVirtuais(List<Sessao> listaSessoes) {
		List<Sessao> listaSessoesTemp = new ArrayList<Sessao>();
		for (Sessao sessao : listaSessoes){
			if (sessao.getTipoJulgamentoVirtual() == null || !sessao.getTipoJulgamentoVirtual().equals(Sessao.TipoJulgamentoVirtual.LISTAS_DE_JULGAMENTO.getId()))
				continue;
			listaSessoesTemp.add(sessao);
		}
		
		Collections.sort(listaSessoesTemp, new Comparator<Sessao>(){
			@Override
			public int compare(Sessao s1, Sessao s2) {
				if (!s1.getAno().equals(s2.getAno()))
					return -s1.getAno().compareTo(s2.getAno());
				if ( s1.getNumero() == null){
					if(s1.getNumero() == s2.getNumero())
						return s1.getColegiado().getDescricao().compareTo(s2.getColegiado().getDescricao()) * (-1);
					else
						return 1;						
				}
				else{
					if (s2.getNumero() == null)
						return -1;
					else{
						if (s1.getNumero().equals(s2.getNumero()))
							return s1.getColegiado().getDescricao().compareTo(s2.getColegiado().getDescricao());												
						return -s1.getNumero().compareTo(s2.getNumero());
					}
				}				
			}			
		});
		
		List<SelectItem> listaSelectSessoes = new ArrayList<SelectItem>();
		for (Sessao sessao : listaSessoesTemp) {						
						
			String colegiado = sessao.getColegiado().getDescricao();
			Long numero = sessao.getNumero();
			String dataInicio = sessao.getDataInicio() == null?  formatter.format(sessao.getDataPrevistaInicio()): formatter.format(sessao.getDataInicio());
			String dataFim    = sessao.getDataFim() == null?  formatter.format(sessao.getDataPrevistaFim()): formatter.format(sessao.getDataFim());
			String label = numero + " Sess�o Virtual (" + colegiado + ") - " +  dataInicio + " a " + dataFim;
			SelectItem e = new SelectItem(sessao.getId(), label);
			listaSelectSessoes.add(e);
		}
		
		return listaSelectSessoes;
	}
	
	public void pesquisarListasJulgamento(){
		
		setListasJulgamentoPesquisadas(new ArrayList<ListaJulgamentoSelecionadaLista>());
		ListaJulgamentoSearchData searchData = new ListaJulgamentoSearchData();
		if (idColegiadoPesquisa != null && !idColegiadoPesquisa.equals("0")){
			if (idColegiadoPesquisa.equals("1T"))
				searchData.setPrimeiraTurma(Boolean.TRUE);
			else if (idColegiadoPesquisa.equals("2T"))
				searchData.setSegundaTurma(Boolean.TRUE);
			else if (idColegiadoPesquisa.equals("TP"))
				searchData.setPlenario(Boolean.TRUE);			
		}
		
		if(idRelatorPesquisa != null && idRelatorPesquisa != 0)
			searchData.setCodigoMinistroRelator(idRelatorPesquisa);
		if (idSessaoPesquisa != null && idSessaoPesquisa != 0)
			searchData.setIdSessao(idSessaoPesquisa);
		
		try{
			SearchResult<ListaJulgamento> pesquisa = listaJulgamentoService.pesquisarListaJulgamentoPlenarioVirtual(searchData);
			List<ListaJulgamento> listasRetornadas = (List<ListaJulgamento>)pesquisa.getResultCollection();						
			if (listasRetornadas == null || listasRetornadas.isEmpty()){
				addInformation("Nenhuma lista encontrada.");
				return;
			}
			for(ListaJulgamento lista : listasRetornadas){
				ListaJulgamentoSelecionadaLista listaPesquisada = new ListaJulgamentoSelecionadaLista();
				listaPesquisada.setListaJulgamento(lista);
				listaPesquisada.setSelected(Boolean.FALSE);
				listaPesquisada.setDescricao(lista.getNome() + " - " + lista.getMinistro().getNome());
				getListasJulgamentoPesquisadas().add(listaPesquisada);
			}
			
		}catch(ServiceException e){
			addError("Erro ao pesquisar as listas de julgamento.");
		}		
	}
	
	public void selectAllListasPesquisados() {
		if (listasJulgamentoPesquisadas == null || listasJulgamentoPesquisadas.isEmpty())
			return;
		boolean check = !allListasPesquisadasChecked();
		for (ListaJulgamentoSelecionadaLista lista : listasJulgamentoPesquisadas) {
			lista.setSelected(check);
		}
	}
	
	private boolean allListasPesquisadasChecked() {
    	for (ListaJulgamentoSelecionadaLista lista : listasJulgamentoPesquisadas) {
    		if (!lista.getSelected()) {
    			return false;
    		}
    	}
    	return true;
    }
	
	public void adicionarListaProcessos(){
		for (ListaJulgamentoSelecionadaLista lista : listasJulgamentoPesquisadas){
			if (!lista.getSelected())
				continue;			
			try{
				ListaJulgamento listaAux = listaJulgamentoService.recuperarPorId(lista.getListaJulgamento().getId());
				for (ObjetoIncidente<?> oi : listaAux.getElementos()){					
					ObjetoIncidenteDto dto = ObjetoIncidenteDto.valueOf(oi);
					incluirProcessoSelecionado(dto);
				}
				setListasJulgamentoPesquisadas(new ArrayList<ListaJulgamentoSelecionadaLista>());
			}catch(ServiceException e){
				addError("Ocorreu um erro ao recuperar os processos das listas de julgamento.");
			}
		}
	}

	
	public String getIdColegiadoPesquisa() {
		if(idColegiadoPesquisa == null)
			idColegiadoPesquisa = "0";
		return idColegiadoPesquisa;
	}

	public void setIdColegiadoPesquisa(String idColegiadoPesquisa) {		
		this.idColegiadoPesquisa = idColegiadoPesquisa;
	}

	public Long getIdRelatorPesquisa() {
		return idRelatorPesquisa;
	}
		
	public void setIdRelatorPesquisa(Long idRelatorPesquisa) {
		this.idRelatorPesquisa = idRelatorPesquisa;
	}

	public Long getIdSessaoPesquisa() {
		return idSessaoPesquisa;
	}

	public void setIdSessaoPesquisa(Long idSessaoPesquisa) {
		this.idSessaoPesquisa = idSessaoPesquisa;
	}
	
	public void setListasSelecionadas(List<SelectItem> lista){
		
	}
	
	public List<SelectItem> getListasSelecionadas(){
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem("0", "lista 1"));
		return l;
	}
	
	public List<ListaJulgamentoSelecionadaLista> getListasJulgamentoPesquisadas() {
		if (listasJulgamentoPesquisadas == null)
			listasJulgamentoPesquisadas = new ArrayList<ListaJulgamentoSelecionadaLista>();
		return listasJulgamentoPesquisadas;
	}

	public void setListasJulgamentoPesquisadas(
			List<ListaJulgamentoSelecionadaLista> listasJulgamentoPesquisadas) {
		this.listasJulgamentoPesquisadas = listasJulgamentoPesquisadas;
	}

	public class ListaJulgamentoSelecionadaLista{
		
		private ListaJulgamento listaJulgamento;
		private Boolean selected;
		private String descricao;
		
		public ListaJulgamento getListaJulgamento() {
			return listaJulgamento;
		}
		public void setListaJulgamento(ListaJulgamento listaJulgamento) {
			this.listaJulgamento = listaJulgamento;
		}
		public Boolean getSelected() {
			return selected;
		}
		public void setSelected(Boolean selected) {
			this.selected = selected;
		}				
		
		public String getDescricao(){
			return descricao;
		}
		
		public void setDescricao(String descricao){
			this.descricao = descricao;
		}
	}
}
