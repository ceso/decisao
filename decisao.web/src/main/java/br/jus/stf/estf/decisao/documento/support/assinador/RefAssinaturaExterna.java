package br.jus.stf.estf.decisao.documento.support.assinador;

import java.io.Serializable;

public final class RefAssinaturaExterna implements Serializable {

	private static final long serialVersionUID = -26678632043156329L;

	private final String id;
	private final String hash;

	public RefAssinaturaExterna(String id, String hash) {
		this.id = id;
		this.hash = hash;
	}

	public String getId() {
		return id;
	}

	public String getHash() {
		return hash;
	}

}
