package br.jus.stf.estf.decisao.support.util;

public enum TipoAmbiente {
	PRODUCAO("Produ��o", "sistemas.stf.jus.br", "www.stf.jus.br", "http://redir.stf.jus.br"), 
	HOMOLOGACAO("Homologa��o", "sistemash.stf.jus.br", "hstf", "http://redirh.stf.jus.br"), 
	DESENVOLVIMENTO("Desenvolvimento", "apld-sspj", "dstf",	"http://smeagold"), 
	INDETERMINADO("Indeterminado", "apld-sspj", "dstf", "http://smeagold"), 
	TESTE("Testes", "sistemast.stf.jus.br", "hstf", "http://redirh.stf.jus.br"),
	PNOVO("PNovo", "sistemas.stf.jus.br", "www.stf.jus.br", "http://redir.stf.jus.br");

	private String descricao;
	private String servidor;
	private String internet;
	private String redir;

	private TipoAmbiente(String descricao, String servidor, String internet, String redir) {
		this.descricao = descricao;
		this.servidor = servidor;
		this.internet = internet;
		this.redir = redir;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getServidor() {
		return this.servidor;
	}

	public String getInternet() {
		return this.internet;
	}

	public String getRedir() {
		return this.redir;
	}

	public String getUrlPecas() {
		return "http://"
				+ this.getServidor()
				+ "/estfvisualizador/jsp/consultarprocessoeletronico/ConsultarProcessoEletronico.jsf?seqobjetoincidente=";
	}

	public String getUrlPecasSupremo() {
		return "http://"
				+ this.getServidor()
				+ "/supremo/supremo.html#/visualizador/false/";
	}
	public String getUrlPecasSupremo(Long id) {
		return "http://"
				+ this.getServidor()
				+ "/supremo/supremo.html#/visualizador/false/"
				+ id.toString();
	}

	public String getUrlPecasSupremoPopout(Long id) {
		return "http://"
				+ this.getServidor()
				+ "/supremo/visualizadorPopout.html#/visualizador/popout/"
				+ id.toString()
				+ "/1";
	}
	
	
	public String getUrlInternet() {
		return "http://" + this.getInternet() + "/portal/processo/verProcessoAndamento.asp?incidente=";
	}

}
