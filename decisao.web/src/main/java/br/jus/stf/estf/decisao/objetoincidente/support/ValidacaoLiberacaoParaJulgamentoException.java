/**
 * 
 */
package br.jus.stf.estf.decisao.objetoincidente.support;

/**
 * @author Paulo.Estevao
 * @since 13.10.2011
 */
public class ValidacaoLiberacaoParaJulgamentoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -783982379329003555L;
	
	public ValidacaoLiberacaoParaJulgamentoException(String message) {
		super(message);
	}
}
