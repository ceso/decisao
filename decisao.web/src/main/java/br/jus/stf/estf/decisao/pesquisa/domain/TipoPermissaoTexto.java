package br.jus.stf.estf.decisao.pesquisa.domain;


/**
 * Classe que define o tipo de permiss�o de um texto.
 * A permiss�o pode ser de Usu�rio ou Grupo.
 *
 */
public enum TipoPermissaoTexto {
	USUARIO, GRUPO;
}