package br.jus.stf.estf.decisao.texto.web;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import br.gov.stf.estf.entidade.documento.Texto;
import br.gov.stf.estf.entidade.documento.TipoTexto;
import br.gov.stf.estf.entidade.documento.tipofase.FaseTexto;
import br.gov.stf.estf.entidade.documento.tipofase.TipoTransicaoFaseTexto;
import br.gov.stf.estf.entidade.processostf.TipoConfidencialidade;
import br.jus.stf.estf.decisao.pesquisa.domain.TextoDto;
import br.jus.stf.estf.decisao.support.action.handlers.CheckIdTipoTexto;
import br.jus.stf.estf.decisao.support.action.handlers.CheckMinisterId;
import br.jus.stf.estf.decisao.support.action.handlers.RequiresResources;
import br.jus.stf.estf.decisao.support.action.handlers.RequiresResources.Mode;
import br.jus.stf.estf.decisao.support.action.handlers.Restrict;
import br.jus.stf.estf.decisao.support.action.handlers.States;
import br.jus.stf.estf.decisao.support.action.support.Action;
import br.jus.stf.estf.decisao.support.action.support.ActionIdentification;
import br.jus.stf.estf.decisao.texto.service.TextoService;
import br.jus.stf.estf.decisao.texto.support.TextoBloqueadoException;

/**
 * Junta as pe�as de cada texto selecionado.
 * 
 * @author Rodrigo Barreiros
 * @since 22.07.2010
 */
@Action(id="juntarPecasActionFacesBean", name="Juntar Pe�as", view="/acoes/texto/juntarPecas.xhtml", height=150, width=500)
@States({FaseTexto.ASSINADO})
@Restrict({ActionIdentification.JUNTAR_PECAS})
@RequiresResources(Mode.Many)
@CheckMinisterId
@CheckIdTipoTexto({TipoTexto.CODIGO_DESPACHO, TipoTexto.CODIGO_DECISAO_MONOCRATICA})
public class JuntarPecasActionFacesBean extends AbstractAlterarFaseDoTextoActionFacesBean<TextoDto> {

	private final String MENSAGEM_VISUALIZACAO_PUBLICA_IMEDIATA = "Esta op��o somente deve ser utilizada quando, "
			+ "para viabilizar o efetivo cumprimento da decis�o, seu conte�do tenha de permanecer com visualiza��o "
			+ "apenas no ambiente interno da Corte. Cumprido o comando, a SEJ disponibilizar� a consulta externa da pe�a.";
	
	@Autowired
	private TextoService textoService;
	
	private boolean disponibilizarNaInternet = true;
	private Boolean existeTextoDeProcessoEmSegredoDeJustica = false;	
	private Boolean existeTextoNaoProcessado;
	private Boolean existeTextoValido;
	
	public void validar() {
		if (!disponibilizarNaInternet) {
			addInformation(MENSAGEM_VISUALIZACAO_PUBLICA_IMEDIATA);
			getDefinition().setFacet("confirmacao");
			cleanMessages();
		} else {
			validateAndExecute();
		}
	}
	
	public void validateAndExecute() {
		for (TextoDto texto : getResources()) {
			adicionaInformacoesDeTextosIguais(texto, getResources());
		}
		
		verificaTextosProcessosSegredoJustica(getResources(), textosIguaisAdicionados);
		
		defineFluxoExecucao();
	}
	
	@Override
	protected void defineFluxoExecucao() {
		// Remove os textos inv�lidos para executar a transi��o.
		getResources().removeAll(textosInvalidos);
		if (getResources().size() > 0) {
			if (hasInformations()) {
				sendToInformations();
			} else if(existeTextoDeProcessoEmSegredoDeJustica) {
				sendToConfirmacaoPeca();
			} else {
				execute();
			}
		} else {
			sendToErrors();
		}
	}
	
	private void verificaTextosProcessosSegredoJustica(Set<TextoDto> listaTextosValidos,
		Set<TextoDto> textosIguaisAdicionados) {
		Set<TextoDto> todosTextos = new HashSet<TextoDto>();
		todosTextos.addAll(listaTextosValidos);
		todosTextos.addAll(textosIguaisAdicionados);
		for (TextoDto textoDto : todosTextos) {
			Texto texto = textoService.recuperarTextoPorId(textoDto.getId());
			if (isTextoProcessoSegredoJustica(texto)) {
				existeTextoDeProcessoEmSegredoDeJustica = true;
			}
		}
	}
	
	private boolean isTextoProcessoSegredoJustica(Texto texto) {
		return TipoConfidencialidade.SEGREDO_JUSTICA.equals(texto.getObjetoIncidente().getTipoConfidencialidade());
	}
	
	public void confirmarJuntada() {
		if (existeTextoDeProcessoEmSegredoDeJustica) {
			sendToConfirmacaoPeca();
		} else {
			execute();
		}
	}

	private void sendToConfirmacaoPeca() {
		getDefinition().setFacet("confirmacaoPeca");
	}
	
	public void confirmarJuntadaAcessoPeca() {
		sendToConfirmacaoTexto();
	}
	
	private void sendToConfirmacaoTexto() {
		getDefinition().setFacet("confirmacaoTexto");
	}
	
	/**
	 * Junta as pe�as de cada texto selecionado.
	 */
	protected void doExecute(TextoDto texto) throws Exception {
		try {
			Collection<String> mensagensDeTextosProcessados = textoService.juntarPecas(texto, textosProcessados, disponibilizarNaInternet, getPrincipal(), getObservacao(), getResponsavel());
			for (String mensagem : mensagensDeTextosProcessados) {
				addInformation(mensagem);
			}
			existeTextoValido = true;
			
		} catch (TextoBloqueadoException e) {
			logger.warn(String.format(MENSAGEM_ERRO_EXECUCAO_ACAO, texto.toString()), e);
			addError(String.format(MENSAGEM_ERRO_EXECUCAO_ACAO + ": %s ", texto.toString(), getMensagemDeErroPadrao(e)));
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void voltar() {
		getDefinition().setFacet("principal");
	}
	
	@Override
	protected String getErrorTitle() {
		return "N�o foi poss�vel juntar os textos abaixo:";
	}
	
	@Override
	public void sendToErrors() {
		getDefinition().setFacet("final");
		getDefinition().setHeight(defineAlturaDaTelaSucessoErro());
		getDefinition().setWidth(500);
		cleanMessages();
	}
	
	private int defineAlturaDaTelaSucessoErro() {
		int tamanho = 250;
		if (existeTextoNaoProcessado != null && existeTextoNaoProcessado && existeTextoValido != null && existeTextoValido){
			//Duplica a altura da tela caso as duas mensagens sejam exibidas.
			tamanho = 350;
		}
		return tamanho;
	}

	@Override
	protected TipoTransicaoFaseTexto getDestino() {
		return TipoTransicaoFaseTexto.JUNTAR;
	}

	public boolean isDisponibilizarNaInternet() {
		return disponibilizarNaInternet;
	}

	public void setDisponibilizarNaInternet(boolean disponibilizarNaInternet) {
		this.disponibilizarNaInternet = disponibilizarNaInternet;
	}
	
	public Boolean getExisteTextoNaoProcessado() {
		return existeTextoNaoProcessado;
	}
	
	public void setExisteTextoNaoProcessado(Boolean existeTextoNaoProcessado) {
		this.existeTextoNaoProcessado = existeTextoNaoProcessado;
	}
	
	public Boolean getExisteTextoValido() {
		return existeTextoValido;
	}
	
	public void setExisteTextoValido(Boolean existeTextoValido) {
		this.existeTextoValido = existeTextoValido;
	}
}
