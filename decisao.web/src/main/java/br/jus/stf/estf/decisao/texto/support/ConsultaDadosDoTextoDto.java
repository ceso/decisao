package br.jus.stf.estf.decisao.texto.support;

import br.gov.stf.estf.documento.model.util.IConsultaDadosDoTexto;
import br.gov.stf.estf.entidade.documento.TipoTexto;

public class ConsultaDadosDoTextoDto implements IConsultaDadosDoTexto {
	private Long codigoDoMinistro;
	private TipoTexto tipoDeTexto;
	private boolean incluirPresidencia;
	private Long sequencialDoArquivoEletronico;
	private Long sequencialObjetoIncidente;

	public Long getSequencialObjetoIncidente() {
		return sequencialObjetoIncidente;
	}

	public void setSequencialObjetoIncidente(Long sequencialObjetoIncidente) {
		this.sequencialObjetoIncidente = sequencialObjetoIncidente;
	}

	private boolean incluirVicePresidencia;
	private TipoTexto[] tiposDeTextoParaExcluir;

	public boolean isIncluirVicePresidencia() {
		return incluirVicePresidencia;
	}

	public void setIncluirVicePresidencia(boolean incluirVicePresidencia) {
		this.incluirVicePresidencia = incluirVicePresidencia;
	}

	public Long getCodigoDoMinistro() {
		return codigoDoMinistro;
	}

	public void setCodigoDoMinistro(Long codigoDoSetor) {
		this.codigoDoMinistro = codigoDoSetor;
	}

	public TipoTexto getTipoDeTexto() {
		return tipoDeTexto;
	}

	public void setTipoDeTexto(TipoTexto tipoDeTexto) {
		this.tipoDeTexto = tipoDeTexto;
	}

	public boolean isIncluirPresidencia() {
		return incluirPresidencia;
	}

	public void setIncluirPresidencia(boolean inserirPresidencia) {
		this.incluirPresidencia = inserirPresidencia;
	}

	public Long getSequencialDoArquivoEletronico() {
		return sequencialDoArquivoEletronico;
	}

	public void setSequencialDoArquivoEletronico(Long sequencialDoArquivoEletronico) {
		this.sequencialDoArquivoEletronico = sequencialDoArquivoEletronico;
	}

	public void setTiposDeTextoParaExcluir(TipoTexto... tiposDeTexto) {
		this.tiposDeTextoParaExcluir = tiposDeTexto;
	}

	public TipoTexto[] getTiposDeTextoParaExcluir() {
		return tiposDeTextoParaExcluir;
	}

}
