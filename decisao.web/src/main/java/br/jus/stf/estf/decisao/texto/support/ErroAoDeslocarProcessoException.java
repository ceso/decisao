/**
 * 
 */
package br.jus.stf.estf.decisao.texto.support;

/**
 * @author Paulo.Estevao
 * @since 27.03.2012
 */
public class ErroAoDeslocarProcessoException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8142655247114055420L;

	public ErroAoDeslocarProcessoException() {
	}

	public ErroAoDeslocarProcessoException(String message) {
		super(message);
	}

	public ErroAoDeslocarProcessoException(Throwable cause) {
		super(cause);
	}

	public ErroAoDeslocarProcessoException(String message,
			Throwable cause) {
		super(message, cause);
	}
}
