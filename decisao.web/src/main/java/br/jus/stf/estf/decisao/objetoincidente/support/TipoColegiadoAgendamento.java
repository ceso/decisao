package br.jus.stf.estf.decisao.objetoincidente.support;

public enum TipoColegiadoAgendamento {

	PLENARIO("P", "Plen�rio"), PT("1T", "Primeira Turma"), ST("2T", "Segunda Turma");

	private String id;
	private String descricao;

	TipoColegiadoAgendamento(String id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public String getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoColegiadoAgendamento getById(String id) {
		for (TipoColegiadoAgendamento tipoColegiadoAgendamento : values()) {
			if (tipoColegiadoAgendamento.getId().equals(id)) {
				return tipoColegiadoAgendamento;
			}
		}
		return null;
	}

}
