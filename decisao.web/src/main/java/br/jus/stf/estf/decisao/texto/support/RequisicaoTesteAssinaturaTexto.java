/**
 * 
 */
package br.jus.stf.estf.decisao.texto.support;

import br.jus.stf.assinadorweb.api.requisicao.RequisicaoJnlpAssinador;

/**
 * @author Paulo.Estevao
 * @since 24.08.2011
 */
public class RequisicaoTesteAssinaturaTexto extends RequisicaoJnlpAssinador<TextoWrapper> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8690687662289682001L;

	public RequisicaoTesteAssinaturaTexto() {
		setExecucaoDeTeste(true);
	}

}
