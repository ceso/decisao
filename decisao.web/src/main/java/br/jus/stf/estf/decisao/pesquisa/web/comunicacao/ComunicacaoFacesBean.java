package br.jus.stf.estf.decisao.pesquisa.web.comunicacao;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.springframework.security.Authentication;
import org.springframework.security.context.SecurityContextHolder;

import br.gov.stf.estf.documento.model.service.DocumentoComunicacaoService;
import br.gov.stf.estf.entidade.documento.DocumentoComunicacao;
import br.gov.stf.estf.entidade.documento.ArquivoProcessoEletronico;
import br.gov.stf.estf.entidade.documento.DocumentoEletronico;
import br.gov.stf.estf.entidade.documento.PecaProcessoEletronicoComunicacao;
import br.gov.stf.estf.entidade.documento.TipoComunicacao;
import br.gov.stf.estf.entidade.documento.TipoPecaProcesso;
import br.gov.stf.framework.model.service.ServiceException;
import br.jus.stf.estf.decisao.pesquisa.domain.ComunicacaoDto;
import br.jus.stf.estf.decisao.pesquisa.domain.Pesquisa;
import br.jus.stf.estf.decisao.pesquisa.service.PesquisaService;
import br.jus.stf.estf.decisao.support.controller.context.FacesBean;
import br.jus.stf.estf.decisao.support.controller.faces.datamodel.PagedList;
import br.jus.stf.estf.decisao.support.security.Principal;
import br.jus.stf.estf.decisao.support.util.GlobalFacesBean;
import br.jus.stf.estf.decisao.support.util.ReportUtils;
import br.jus.stf.estf.decisao.support.util.STFOfficeUtils;
import br.jus.stf.estf.report.ReportOutputStrategy;

/**
 * Bean JSF (Seam Component) para controle e tratamento de eventos de tela associados a 
 * Comunicacao. Usado pelo mecanismo de pesquisa para recupera��o e edi��o de informa��es.
 * 
 * <p>Implementa��o <code>FacesBean</code> para Comunica��es.
 * 
 */
@Name("comunicacaoFacesBean")
@Scope(ScopeType.CONVERSATION)
public class ComunicacaoFacesBean extends STFOfficeUtils implements FacesBean<ComunicacaoDto>, ReportOutputStrategy {

	@In("#{pesquisaService}")
	private PesquisaService pesquisaService;

	@In("#{documentoComunicacaoService}")
	private DocumentoComunicacaoService documentoComunicacaoService;
	
	@In(value = "globalFacesBean", create = true)
	private GlobalFacesBean globalFacesBean;
	
	@In
	private FacesMessages facesMessages;
	
	
	@Logger
	private Log logger;
	private TipoComunicacao tipoComunicacaoSelecionado;
	private String nomeMinistroRelator;
	private String dscNomeDocumento;
	private String usuarioCriacao;
	public Long idTipoComunicacaoSelecionado;
	private Date dataCriacao;
	private boolean refresh;
	private File reportOutputFile;
	private FileOutputStream reportOutputStream;

	/**
	 * @see br.jus.stf.estf.decisao.support.controller.context.FacesBean#search(br.jus.stf.estf.decisao.pesquisa.domain.Pesquisa, int, int)
	 */
	@Override
	public PagedList<ComunicacaoDto> search(Pesquisa pesquisa, int first, int max) {
		pesquisa.setFirstResult(first);
		pesquisa.setMaxResults(max);
		return pesquisaService.pesquisarComunicacoes(pesquisa);
	}

	/**
	 * Carrega informa��es de texto e do conte�do do texto.
	 * 
	 * @see br.jus.stf.estf.decisao.support.controller.context.FacesBean#load(java.lang.Object)
	 */
	@Override
	public ComunicacaoDto load(ComunicacaoDto dto) {
		return dto;
	}
	
	/**
	 * Gera o relat�rio e redireciona para download o texto selecionado na tela de 
	 * listagem de resultados.
	 * @param dto a Comunica��o selecionada.
	 */
	public void gerarPDFComunicacao(ComunicacaoDto dto) {
		try {
			if( dto.getId() != null ){
				DocumentoComunicacao documentoComunicacao = documentoComunicacaoService.recuperarPorId(dto.getIdDocumentoComunicacao());
				if ( documentoComunicacao != null && documentoComunicacao.getDocumentoEletronico().getArquivo() != null ) {
					ReportUtils.report(new ByteArrayInputStream(documentoComunicacao.getDocumentoEletronico().getArquivo()));
				} else {
					facesMessages.add("N�o foi poss�vel gerar o arquivo PDF.", dto);
				}
			}
		} catch (ServiceException e) {
			logger.error(e, dto.getId());
			facesMessages.add(e.getMessage(), dto);
		}
	}
	
	public void gerarPDFVinculado(DocumentoEletronico dto) throws ServiceException {
		if( dto != null && dto.getId() != null ){
				ReportUtils.report(new ByteArrayInputStream(dto.getArquivo()));
			} else {
				facesMessages.add("N�o foi poss�vel gerar o arquivo PDF.", dto);
		}
	}	

	public boolean isRefresh() {
		return refresh;
	}

	public String getUsuarioCriacao() {
		return usuarioCriacao;
	}

	public void setUsuarioCriacao(String usuarioCriacao) {
		this.usuarioCriacao = usuarioCriacao;
	}
	
	public TipoComunicacao getTipoComunicacaoSelecionado() {
		return tipoComunicacaoSelecionado;
	}

	public void setTipoComunicacaoSelecionado(
			TipoComunicacao tipoComunicacaoSelecionado) {
		this.tipoComunicacaoSelecionado = tipoComunicacaoSelecionado;
	}

	public String getNomeMinistroRelator() {
		return nomeMinistroRelator;
	}

	public void setNomeMinistroRelator(String nomeMinistroRelator) {
		this.nomeMinistroRelator = nomeMinistroRelator;
	}

	public String getDscNomeDocumento() {
		return dscNomeDocumento;
	}

	public void setDscNomeDocumento(String dscNomeDocumento) {
		this.dscNomeDocumento = dscNomeDocumento;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	/**
	 * Recupera o usu�rio autenticado. Esse usu�rio � encapsulado em um objeto
	 * Principal que cont�m as credenciais do usu�rio.
	 * 
	 * @return o principal
	 */
	private Principal getPrincipal() {
		return (Principal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	private Authentication getAuthentication() {
		return (Authentication) SecurityContextHolder.getContext().getAuthentication();
	}
	
	@Override
	public OutputStream getOutputStreamFor(String reportName)
			throws IOException {
		reportOutputFile = File.createTempFile("report", ".tmp");
		reportOutputStream = new FileOutputStream(reportOutputFile);
		return reportOutputStream;
	}
}
