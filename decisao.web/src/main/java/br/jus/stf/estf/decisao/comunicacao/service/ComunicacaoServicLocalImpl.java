package br.jus.stf.estf.decisao.comunicacao.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.gov.stf.estf.assinatura.security.ExpedienteAssinadoResult;
import br.gov.stf.estf.assinatura.security.UsuarioAssinatura;
import br.gov.stf.estf.assinatura.service.ComunicacaoDocumentoBase;
import br.gov.stf.estf.assinatura.service.impl.ComunicacaoServiceLocalBaseImpl;
import br.gov.stf.estf.documento.model.util.ComunicacaoDocumentoResult;
import br.gov.stf.estf.entidade.documento.DocumentoComunicacao;
import br.gov.stf.estf.entidade.localizacao.Setor;
import br.gov.stf.estf.intimacao.model.service.exception.ServiceLocalException;
import br.gov.stf.framework.exception.RegraDeNegocioException;

@Service("ComunicacaoServiceLocal")
public class ComunicacaoServicLocalImpl extends ComunicacaoServiceLocalBaseImpl {

	@Override
	public void excluir(
			List<? extends ComunicacaoDocumentoBase> comunicacaoDocumentos)
			throws ServiceLocalException, RegraDeNegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cancelarAssinatura(
			ComunicacaoDocumentoBase comunicacaoDocumento,
			String anotacaoCancelamento, UsuarioAssinatura usuario)
			throws ServiceLocalException, RegraDeNegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cancelarPDF(DocumentoComunicacao docComunicacao,
			String observacao, UsuarioAssinatura usuario,
			boolean gerarFaseCorrecao) throws ServiceLocalException,
			RegraDeNegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cancelarPDF(DocumentoComunicacao docComunicacao,
			String descricaoStatusDocumento, String observacao,
			UsuarioAssinatura usuario, boolean gerarFaseCorrecao)
			throws ServiceLocalException, RegraDeNegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void devolverParaSetorOrigem(
			ComunicacaoDocumentoBase comunicacaoDocumento,
			String anotacaoCancelamento, UsuarioAssinatura usuario)
			throws ServiceLocalException, RegraDeNegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void expedir(
			List<? extends ComunicacaoDocumentoBase> comunicacaoDocumentos,
			UsuarioAssinatura usuario) throws ServiceLocalException,
			RegraDeNegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void encaminharParaDJe(
			List<? extends ComunicacaoDocumentoBase> comunicacaoDocumentos,
			UsuarioAssinatura usuario) throws ServiceLocalException,
			RegraDeNegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finalizar(
			List<? extends ComunicacaoDocumentoBase> comunicacaoDocumentos,
			UsuarioAssinatura usuario) throws ServiceLocalException,
			RegraDeNegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finalizarRevisao(
			List<? extends ComunicacaoDocumentoBase> comunicacaoDocumentos,
			UsuarioAssinatura usuario) throws ServiceLocalException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ComunicacaoDocumentoResult> pesquisarDocumentos(
			Long seqObjetoIncidente, Long codigoModelo, Setor setor,
			UsuarioAssinatura usuario) throws ServiceLocalException,
			RegraDeNegocioException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ExpedienteAssinadoResult> pesquisarComunicacoesAssinadasPorPeriodo(
			Long codigoSetor, String usuario, String dataInicial,
			String dataFinal) throws ServiceLocalException,
			RegraDeNegocioException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ComunicacaoDocumentoResult> pesquisarDocumentosCorrecao(
			Setor setorUsuarioAutenticado, UsuarioAssinatura usuario)
			throws ServiceLocalException, RegraDeNegocioException {
		// TODO Auto-generated method stub
		return null;
	}

	

}
