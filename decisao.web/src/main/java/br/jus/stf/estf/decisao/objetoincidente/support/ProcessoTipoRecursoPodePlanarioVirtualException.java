package br.jus.stf.estf.decisao.objetoincidente.support;

public class ProcessoTipoRecursoPodePlanarioVirtualException extends Exception {

	private static final long serialVersionUID = -1219760112581749151L;

	public ProcessoTipoRecursoPodePlanarioVirtualException() {
	}

	public ProcessoTipoRecursoPodePlanarioVirtualException(String message) {
		super(message);
	}

	public ProcessoTipoRecursoPodePlanarioVirtualException(Throwable cause) {
		super(cause);
	}

	public ProcessoTipoRecursoPodePlanarioVirtualException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
