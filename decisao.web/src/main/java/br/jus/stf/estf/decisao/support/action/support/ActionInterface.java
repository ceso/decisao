package br.jus.stf.estf.decisao.support.action.support;

import java.util.Map;
import java.util.Set;

/**
 * Abstra��o que ser� utilizada para implementa��o das a��es.
 * 
 * @author Rodrigo Barreiros
 * @since 24.05.2010
 *
 * @param <T> o tipo do recurso manipulado pela a��o
 */
public interface ActionInterface<T> {
	
	/**
	 * Recupera as metainforma��es definidas para a a��o na anota��o {@link Action}
	 * 
	 * @return as metainforma��es da a��o
	 */
	public ActionDefinition getDefinition();
    
    /**
     * Seta os recursos que foram selecionados na tela para manipula��o pela a��o.
     * 
     * @param resources os recursos para a a��o
     */
    public void setResources(Set<T> resources);

    /**
     * Recupera os recursos submetidos � a��o.
     * 
     * @return os recursos da a��o
     */
    public Set<T> getResources();
    
    /**
     * Recupera o tipo dos recursos manipulados pela a��o.
     * 
     * @return o tipo dos recursos
     */
    public Class<T> getResourceClass();
    
    /**
     * Chamado assim que a a��o � carregada para execu��o.
     */
    public void load();
     
    /**
     * @param options
     */
    public void setOptions(Map<Object, Object> options);
    
    /**
     * Setar "true" se a action for apresentada em um iframe (default=false)
     * 
     * @param actionFrame
     */
    public void setActionFrame(boolean actionFrame);

}
