package br.jus.stf.estf.decisao.objetoincidente.support;

public class ProcessoNaoPodeSerAgendadoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1219760112581749151L;

	public ProcessoNaoPodeSerAgendadoException() {
	}

	public ProcessoNaoPodeSerAgendadoException(String message) {
		super(message);
	}

	public ProcessoNaoPodeSerAgendadoException(Throwable cause) {
		super(cause);
	}

	public ProcessoNaoPodeSerAgendadoException(String message, Throwable cause) {
		super(message, cause);
	}

}
