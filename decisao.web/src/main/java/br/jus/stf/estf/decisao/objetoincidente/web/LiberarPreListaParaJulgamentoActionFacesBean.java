package br.jus.stf.estf.decisao.objetoincidente.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.model.SelectItem;

import org.jboss.seam.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.gov.stf.estf.entidade.julgamento.Colegiado;
import br.gov.stf.estf.entidade.julgamento.Colegiado.TipoColegiadoConstante;
import br.gov.stf.estf.entidade.julgamento.ListaJulgamento;
import br.gov.stf.estf.entidade.julgamento.Sessao;
import br.gov.stf.estf.entidade.julgamento.Sessao.TipoAmbienteConstante;
import br.gov.stf.estf.entidade.julgamento.Sessao.TipoSessaoConstante;
import br.gov.stf.estf.entidade.julgamento.TipoListaJulgamento;
import br.gov.stf.estf.entidade.localizacao.Setor;
import br.gov.stf.estf.entidade.ministro.Ministro;
import br.gov.stf.estf.entidade.processostf.ObjetoIncidente;
import br.gov.stf.estf.entidade.processostf.PreListaJulgamento;
import br.gov.stf.estf.entidade.processostf.PreListaJulgamentoObjetoIncidente;
import br.gov.stf.estf.entidade.publicacao.Feriado;
import br.gov.stf.estf.entidade.usuario.Usuario;
import br.gov.stf.estf.julgamento.model.service.SessaoService;
import br.gov.stf.estf.julgamento.model.service.TipoListaJulgamentoService;
import br.gov.stf.estf.processostf.model.service.AgendamentoService;
import br.gov.stf.estf.processostf.model.service.PreListaJulgamentoService;
import br.gov.stf.estf.publicacao.model.service.FeriadoService;
import br.gov.stf.estf.util.DataUtil;
import br.gov.stf.framework.model.service.ServiceException;
import br.jus.stf.estf.decisao.objetoincidente.service.ObjetoIncidenteService;
import br.jus.stf.estf.decisao.objetoincidente.support.DadosAgendamentoDto;
import br.jus.stf.estf.decisao.objetoincidente.support.ProcessoNaoPodeSerAgendadoException;
import br.jus.stf.estf.decisao.objetoincidente.support.ProcessoPrecisaDeConfirmacaoException;
import br.jus.stf.estf.decisao.objetoincidente.support.ProcessoTipoRecursoPodePlanarioVirtualException;
import br.jus.stf.estf.decisao.objetoincidente.support.TipoAgendamento;
import br.jus.stf.estf.decisao.objetoincidente.support.TipoAndamento;
import br.jus.stf.estf.decisao.objetoincidente.support.TipoColegiadoAgendamento;
import br.jus.stf.estf.decisao.objetoincidente.support.ValidacaoLiberacaoParaJulgamentoException;
import br.jus.stf.estf.decisao.pesquisa.domain.ObjetoIncidenteDto;
import br.jus.stf.estf.decisao.support.action.handlers.RequiresResources;
import br.jus.stf.estf.decisao.support.action.handlers.RequiresResources.Mode;
import br.jus.stf.estf.decisao.support.action.handlers.Restrict;
import br.jus.stf.estf.decisao.support.action.support.Action;
import br.jus.stf.estf.decisao.support.action.support.ActionCallback;
import br.jus.stf.estf.decisao.support.action.support.ActionIdentification;
import br.jus.stf.estf.decisao.support.action.support.ActionSupport;
import br.jus.stf.estf.decisao.support.util.NestedRuntimeException;

@Action(id = "liberarPreListaParaJulgamentoActionFacesBean", name = "Liberar Pr� Lista para Julgamento", view = "/acoes/objetoincidente/liberarPreListaParaJulgamento.xhtml")
@Restrict({ ActionIdentification.LIBERAR_PARA_JULGAMENTO })
@RequiresResources(Mode.Many)
public class LiberarPreListaParaJulgamentoActionFacesBean extends ActionSupport<PreListaJulgamento> {

	public static final String MSG_ERRO_CARREGAR_SESSOES = "Erro: ao carregar sess�es.";
	public static final String MSG_ERRO_SALVAR_SESSAO    = "Erro: ao salvar sess�es";
	
	private static final String MSG_PROCESSO_COM_REPRESENTANTE_SEM_OAB = "Processo com representante sem OAB.";

	@Autowired
	private PreListaJulgamentoService preListaJulgamentoService;

	@Qualifier("objetoIncidenteServiceLocal")
	@Autowired
	private ObjetoIncidenteService objetoIncidenteService;

	@Autowired
	private SessaoService sessaoService;
	
	@Autowired
	private FeriadoService feriadoService;
	
	@Autowired
	private AgendamentoService agendamentoService;
	
	@Autowired
	private TipoListaJulgamentoService tipoListaJulgamentoService;
	

	private TipoColegiadoConstante colegiadoMinistro;

	private Date dataJulgamento;
	private String idTipoColegiadoAgendamento;
	private String idTipoAmbienteColegiado;
	private String idTipoAndamento;
	private ListaJulgamento listaJulgamento;
	private List<SelectItem> sessoes;
	private List<SelectItem> tipoListaJulgamento;
	private List<Sessao> sessoesEmAberto = new ArrayList<Sessao>();
	private Long idSessao;
	private Long idTipoListaJulgamento;
	private Boolean confirmarListasPendentes = false;
	private Boolean existeListaLiberada      = false;
	private Boolean existeListaNaoLiberada   = false;
	private Boolean sessaoMinistroDiferente  = false;
	private DataUtil dataUtil                = new DataUtil();
	
	private Set<PreListaJulgamento> listasInvalidas = new HashSet<PreListaJulgamento>();
	private Set<PreListaJulgamento> listasParaConfirmacao = new HashSet<PreListaJulgamento>();
	
	private Boolean listaProcessosInseridosIndice;
	
	public Boolean getVerificarListaLiberada(){		
		if (!isListaProcessosInseridosIndice())
			return false;
		setIdTipoAmbienteColegiado(TipoAmbienteConstante.PRESENCIAL.getSigla());
		setIdTipoAndamento(TipoAndamento.MESA.getId());
		
		return false;
	}
	
	public void ambienteJulgamentoSelecionado(){
		this.idTipoAndamento = null;
		this.idTipoColegiadoAgendamento = null;
		
		if(TipoAmbienteConstante.VIRTUAL.getSigla().equals(this.idTipoAmbienteColegiado)){
			setIdTipoAndamento(TipoAndamento.PAUTA.getId());
		}
	}
	
	public void tipoAndamentoSelecionado(){
		this.idTipoColegiadoAgendamento = null;
	}
	
	public void tipoColegiadoSelecionado(){
		boolean ambienteNotNull    = ambienteNotNull();
		boolean andamentoNull      = andamentoNull();
		boolean colegiadoNotNull   = colegiadoNotNull();
		boolean ambientePresencial = TipoAmbienteConstante.PRESENCIAL.getSigla().equals(this.idTipoAmbienteColegiado);
		if (ambienteNotNull && !andamentoNull && colegiadoNotNull && ambientePresencial) {
			this.carregarSessoes();
		}
		carregarTipoListaJulgamento();
	}
	
	public boolean isDesabilitarPainelTipoAndamento(){
		String idTipoAmbienteColegiado = this.getIdTipoAmbienteColegiado();
		boolean colegiadoNaoNulo       = idTipoAmbienteColegiado != null;
		boolean isAmbienteVirtual      = TipoAmbienteConstante.VIRTUAL.getSigla().equals(idTipoAmbienteColegiado);
		return colegiadoNaoNulo && isAmbienteVirtual;
	}

	public boolean colegiadoNotNull() {
		return this.idTipoColegiadoAgendamento != null && !this.idTipoColegiadoAgendamento.isEmpty();
	}

	public boolean ambienteNotNull() {
		return this.idTipoAmbienteColegiado != null && !this.idTipoAmbienteColegiado.isEmpty();
	}

	public boolean andamentoNull() {
		return this.idTipoAndamento == null || this.idTipoAndamento.isEmpty();
	}
	
	public void carregarSessoes() {
		try {
			TipoColegiadoConstante colegiado = null;
			colegiado = defineColegiado(this.idTipoColegiadoAgendamento);
			if(TipoAmbienteConstante.PRESENCIAL.getSigla().equals(this.idTipoAmbienteColegiado)){
				List<Sessao> sessoesPesquisa = sessaoService.pesquisar(colegiado,TipoAmbienteConstante.PRESENCIAL);
				carregarSessoesPesquisadas(sessoesPesquisa);
			}
		} catch (ServiceException e) {
			addError(MSG_ERRO_CARREGAR_SESSOES+ExportarPreListaJulgamentoActionFacesBean.ESPACO_BRANCO+e.getMessage());
			logger.error(MSG_ERRO_CARREGAR_SESSOES, e);
		}catch (Exception e) {
			addError(e.getMessage());
			logger.error(e);
			e.printStackTrace();
		}
	}

	public void carregarSessoesPesquisadas(List<Sessao> sessoesPesquisa) throws ParseException, ServiceException {
		this.sessoes = new ArrayList<SelectItem>();
		GregorianCalendar hoje = this.getNow();
		List<GregorianCalendar> feriados = this.recuperFeriados(hoje);
		for (Sessao sessao : sessoesPesquisa) {
			boolean respeitaPrazoCPC2016 = this.sessaoDentroPrazoCPC(this.idTipoAndamento,sessao,hoje,feriados);
			if(respeitaPrazoCPC2016){
				String label = this.labelSessao(sessao);
				sessoes.add(new SelectItem(sessao.getId(), label));
				sessoesEmAberto.add(sessao);
			}
		}
		sessaoColegiadoEscolhidoDiferenteColegiadoPadraoMinistro();
	}
	
	public void carregarTipoListaJulgamento() {
		this.tipoListaJulgamento = new ArrayList<SelectItem>();
		try {
			List<TipoListaJulgamento> tipoListaJulgamentoAtivas = tipoListaJulgamentoService.recuperarTipoListaJulgamentoAtivas();
			for (TipoListaJulgamento tipoListaJulgamentoItem : tipoListaJulgamentoAtivas) {
				tipoListaJulgamento.add(new SelectItem(tipoListaJulgamentoItem.getId(), tipoListaJulgamentoItem.getDescricao()));
			}
		} catch (ServiceException e) {
			addError("Erro: ao carregar tipos de lista. "+ExportarPreListaJulgamentoActionFacesBean.ESPACO_BRANCO+e.getMessage());
			logger.error("Erro: ao carregar tipos de lista. ", e);
		}catch (Exception e) {
			addError(e.getMessage()); 
			logger.error(e);
			e.printStackTrace();
		}
	}

	void sessaoColegiadoEscolhidoDiferenteColegiadoPadraoMinistro() {
		if (!idTipoColegiadoAgendamento.equalsIgnoreCase("P")) {
			if (!colegiadoMinistro.getSigla().equalsIgnoreCase(idTipoColegiadoAgendamento)) {
				setSessaoMinistroDiferente(true);
			} else {
				setSessaoMinistroDiferente(false);
			}
		} else {
			setSessaoMinistroDiferente(false);
		}
	}

	GregorianCalendar getNow() {
		return dataUtil.getNow();
	}

	/**
	 * Recupera os Feriados do mes atual e proximo m�s
	 * @param calendarHoje
	 * @return
	 * @throws ServiceException
	 */
	public List<GregorianCalendar> recuperFeriados(GregorianCalendar calendarHoje) throws ServiceException {
		List<Feriado> feriados = new ArrayList<Feriado>();
		List<Feriado> listFeriadosMes = new ArrayList<Feriado>();
		
		int mes = calendarHoje.get(Calendar.MONTH);
		mes = mes + 1; //Adcionado 1, pois no Calendar Janeiro � 0 e Dezembo � 11;
		int ano = calendarHoje.get(Calendar.YEAR);
		String mesAno = this.recuperaMesAno(ano, mes);
		listFeriadosMes = feriadoService.recuperar(mesAno);
		feriados.addAll(listFeriadosMes);
		if(mes == Calendar.JULY ){
			mes =  Calendar.SEPTEMBER;
		}else if(mes == Calendar.DECEMBER){
			mes = Calendar.FEBRUARY;
			ano = ano+1;
		}else{
			mes = mes+1;
		}
		mesAno = this.recuperaMesAno(ano, mes);
		listFeriadosMes = feriadoService.recuperar(mesAno);		
		feriados.addAll(listFeriadosMes);
		
		List<GregorianCalendar> feriadosCalendar = this.tipoFeriado2Calendar(feriados);
		
		return feriadosCalendar;
	}
	
	/**
	 * ATENCAO: MES recebe valores de 1 a 12, por�m para o Calendar Janeiro � 0 e Dezembro � 11. 
	 * @param ano
	 * @param mes valor de 1 a 12. 
	 * @return
	 */
	String recuperaMesAno(int ano, int mes){
		String stringMes = null;
		if((mes > 0) && (mes < 10) ){
			stringMes = "0"+String.valueOf(mes);
		}else{
			stringMes = String.valueOf(mes);
		}
		String mesAno = stringMes + String.valueOf(ano);
		
		return mesAno;
	}
	
	List<GregorianCalendar> tipoFeriado2Calendar(List<Feriado> feriados) {
		List<GregorianCalendar> feriadosCalendar = new ArrayList<GregorianCalendar>();
		for (Feriado feriado : feriados) {
			int dia = Integer.valueOf(feriado.getDia());
			int mes =  Integer.valueOf(feriado.getId().substring(0, 2));
			int ano =  Integer.valueOf(feriado.getId().substring(2, feriado.getId().length()));
			GregorianCalendar feriadoCalendar = new GregorianCalendar(ano,mes-1,dia); //Mes decrementa de 1 pois no Calendar Janeiro � 0 e Dezembro � 11
			feriadosCalendar.add(feriadoCalendar);
		}		
		return feriadosCalendar;
	}

	/**
	 * Determina se a sess�o respeita a regra de antecedencia do novo CPC. A data de inicio da sess�o dever� ocorrer 7 dias
	 * uteis apos a libera��o. N�o contando a data da libera��o DECISAO-2231
	 * 
	 * ATEN��O : Se idTipoAndamento = TipoAndamento.MESA retorna TRUE. Pois ser� apresentando em mesa e n�o ir� respeita
	 * o prazo do novo CPC
	 * 
	 * @param idTipoAndamento valores TipoAndamento.MESA ou TipoAndamento.PAUTA
	 * @param sessao que ser� verifica se respeita o CPC
	 * @param hoje
	 * @param feriados lista de Feriados
	 * @return
	 * @throws ParseException
	 */
	boolean sessaoDentroPrazoCPC( String idTipoAndamento
			                    , Sessao sessao
			                    , GregorianCalendar hoje
			                    , List<GregorianCalendar> feriados) throws ParseException {
		Boolean retorno = false;
		if(TipoAndamento.MESA.getId().equals(idTipoAndamento)){
			retorno = true;
		}else{	
			Date dateInicioSessao                  = this.getDataInicioSessao(sessao);
			GregorianCalendar calendarInicioSessao = this.date2GregorianCalendar(dateInicioSessao);
			GregorianCalendar calendarUltimoDia    = this.recuperaProximoDiaRespeitandoPrazoCPC(hoje,feriados);
			int datMaior = calendarInicioSessao.compareTo(calendarUltimoDia);
			return (datMaior >= 0);
		}
		return retorno;
	}


	/**
	 * Recupera o proximo dia util respeitando o prazo do novo CPC
	 * @param hoje
	 * @param feriados
	 * @return
	 */
	public GregorianCalendar recuperaProximoDiaRespeitandoPrazoCPC(GregorianCalendar dataLiberacao, List<GregorianCalendar> feriados) {
		GregorianCalendar hoje = getNewGregorian(dataLiberacao);
		int diasUteis = 0;
		int qtdDiasEntreLiberacaoEJulgamento = 7;
		
		while(diasUteis < qtdDiasEntreLiberacaoEJulgamento){
			hoje.add(Calendar.DAY_OF_MONTH, 1);
			if(isDiaUtil(hoje,feriados))
				diasUteis++;
		}
		
		// Retorna o pr�ximo dia (�til ou n�o) ap�s acumular a quantidade de dias a aguardar 
		hoje.add(Calendar.DAY_OF_MONTH, 1);
		
		return hoje;
	}

	public GregorianCalendar getNewGregorian(Calendar calendarHoje) {
		int dia = calendarHoje.get(Calendar.DAY_OF_MONTH);
		int mes = calendarHoje.get(Calendar.MONTH);
		int ano = calendarHoje.get(Calendar.YEAR);
		GregorianCalendar retorno = new GregorianCalendar(ano, mes, dia);
		return retorno;
	}

	/**
	 * Informa se � um dia util ou n�o. Considerando: finais de semana; feriados informados; f�rias ministros STF;
	 * @param calendarHoje
	 * @param feriados
	 * @return
	 */
	boolean isDiaUtil(GregorianCalendar calendarHoje, List<GregorianCalendar> feriados) {
		int diaDoMes = calendarHoje.get(Calendar.DAY_OF_MONTH);
		int diaSemana = calendarHoje.get(Calendar.DAY_OF_WEEK);
		int mes       = calendarHoje.get(Calendar.MONTH);
		
		// S�bado e Domingo n�o � considerado dia �til
		if ((diaSemana == Calendar.SUNDAY) || (diaSemana == Calendar.SATURDAY))
			return false;
		
		if (feriados != null && feriados.contains(calendarHoje))
			return false;
		
		// Ferias Ministros
		if(mes == Calendar.JANUARY)
			return false;
		
		// O recesso de julho vai de 2 a 31
		if (mes == Calendar.JULY && diaDoMes != 1)
			return false;
		
		// Recesso de 20/12 at� o final do ano
		if (mes == Calendar.DECEMBER && diaDoMes >= 20)
			return false;
		
		return true;
	}

	public SimpleDateFormat getFormatterDatePadrao(boolean mostrarHora) {		
		return dataUtil.getFormatterDatePadrao(mostrarHora);
	}
	
	protected String labelSessao(Sessao sessao) {
		String dataSesso = this.getDescricaoDataSessao(sessao);
		String colegiadoDescricao = TipoColegiadoConstante.valueOfSigla(sessao.getColegiado().getId()).getDescricao();
		String tipo = this.getDescricaoTipoSessao(sessao);
		String label = dataSesso + " - " + colegiadoDescricao + " - " + tipo;
		return label;
	}

	String getDescricaoTipoSessao(Sessao sessao) {
		String tipo = "";
		String ambiente = sessao.getTipoAmbiente();
		if(TipoAmbienteConstante.VIRTUAL.getSigla().equals(ambiente)){
			tipo = "Sess�o Virtual";
		}else{
			tipo = TipoSessaoConstante.valueOfSigla(sessao.getTipoSessao()).getDescricao(); 
		}
		return tipo;
	}
	
	String getDescricaoDataSessao(Sessao sessao) {
		String dataSesso = "";		
		String ambiente = sessao.getTipoAmbiente();
		if(TipoAmbienteConstante.VIRTUAL.getSigla().equals(ambiente)){
			SimpleDateFormat formatter = this.getFormatterDatePadrao(false);
			Date dataInicio = this.getDataInicioSessao(sessao);
			Date dataFim    = this.getDataFimSessao(sessao);
			dataSesso = formatter.format(dataInicio)+" a "+formatter.format(dataFim);
		}else{
			SimpleDateFormat formatter = this.getFormatterDatePadrao(true);
			Date dataInicio = this.getDataInicioSessao(sessao);
			dataSesso = formatter.format(dataInicio);
		}
		return dataSesso;
	}
	
	/**
	 * Retorna a Data inicial da Sessao, pois na sess�o VIRTUAL pode ter apena o inicio
	 * previsto pois a sessao ainda n�o ocorreu.
	 * @param sessao
	 * @return
	 */
	public Date getDataInicioSessao(Sessao sessao) {
		Date dataInicio = null;
		String tipoAmbiente = sessao.getTipoAmbiente();
		if(TipoAmbienteConstante.PRESENCIAL.getSigla().equals(tipoAmbiente)){
			dataInicio = sessao.getDataInicio();
			if(dataInicio == null){
				dataInicio = sessao.getDataPrevistaInicio();
			}
		}else{
			dataInicio = sessao.getDataInicio();
			if(dataInicio == null){
				dataInicio = sessao.getDataPrevistaInicio();
			}
		}		
		return dataInicio;
	}	
	
	/**
	 * Retorna a Data final da Sessao, pois na sess�o VIRTUAL pode ter apena o inicio
	 * previsto pois a sessao ainda n�o ocorreu.
	 * @param sessao
	 * @return
	 */
	public Date getDataFimSessao(Sessao sessao) {
		Date dataInicio = null;
		String tipoAmbiente = sessao.getTipoAmbiente();
		if(TipoAmbienteConstante.PRESENCIAL.getSigla().equals(tipoAmbiente)){
			dataInicio = sessao.getDataFim();
			if(dataInicio == null){
				dataInicio = sessao.getDataPrevistaFim();
			}
		}else{
			dataInicio = sessao.getDataFim();
			if(dataInicio == null){
				dataInicio = sessao.getDataPrevistaFim();
			}
		}		
		return dataInicio;
	}	
	
	protected GregorianCalendar date2GregorianCalendar(Date date) throws ParseException {
		return dataUtil.date2GregorianCalendar(date);
	}

	/**
	 * Define a data e hora do fim da sess�o virtual. Sempre as quintas-feiras as 23:59:00
	 * @param dataPrevistaInicio
	 * @return
	 * @throws ParseException
	 */
	protected Date datSessaoFim(Date dataPrevistaInicio) throws ParseException {
		GregorianCalendar calendarRetorno = this.date2GregorianCalendar(dataPrevistaInicio);
		calendarRetorno.add(Calendar.DAY_OF_MONTH, 6);
		calendarRetorno.set(Calendar.HOUR_OF_DAY, 23); //Dia com 24 horas, se usar HOUR ser� dia AM e PM
		calendarRetorno.set(Calendar.MINUTE, 59);
		calendarRetorno.set(Calendar.SECOND, 59);
		Date dateRetorno = calendarRetorno.getTime();
	    return dateRetorno;
	}
	
	/**
	 * Define qual � data inicial do slote da sessao virtual. Sempre iniciando
	 * na proxima sexta, depois do dateInformado 
	 * @param dateInformado
	 * @return
	 * @throws ParseException
	 */
	protected Date dateSessaoInicio(Date dateInformado) throws ParseException {
		GregorianCalendar calendarInformado = this.date2GregorianCalendar(dateInformado);
	    int diaSemana = calendarInformado.get(Calendar.DAY_OF_WEEK);
	    do {
	        switch (diaSemana) {
		        case Calendar.SUNDAY:
		        	calendarInformado.add(Calendar.DAY_OF_MONTH, 5);
		            break;
		        case Calendar.MONDAY:
		        	calendarInformado.add(Calendar.DAY_OF_MONTH, 4);
		            break;
		        case Calendar.TUESDAY:
		        	calendarInformado.add(Calendar.DAY_OF_MONTH, 3);
		            break;
		        case Calendar.WEDNESDAY:
		        	calendarInformado.add(Calendar.DAY_OF_MONTH, 2);
		            break;
		        case Calendar.THURSDAY:
		        	calendarInformado.add(Calendar.DAY_OF_MONTH, 1);
		            break;
		        case Calendar.FRIDAY:
		        	calendarInformado.add(Calendar.DAY_OF_MONTH, 0);
		            break;
		         case Calendar.SATURDAY:
		        	calendarInformado.add(Calendar.DAY_OF_MONTH, 6);
		            break;
	        }
        } while (!isDiaUtil(calendarInformado, null)); // Enquanto n�o encontrar um dia fora dos recessos (ignora feriados)
        	
        calendarInformado.set(Calendar.HOUR_OF_DAY, 00); //Dia com 24 horas, se usar HOUR ser� dia AM e PM
        calendarInformado.set(Calendar.MINUTE, 00);
        calendarInformado.set(Calendar.SECOND, 00);
        
        return  calendarInformado.getTime();
	}
	
	protected Short getAnoSessaoVirtual(Date dataPrevistaInicio) throws ParseException {
		GregorianCalendar anoCalendar = this.date2GregorianCalendar(dataPrevistaInicio);
		Short ano = (short) anoCalendar.get(Calendar.YEAR);
		return ano;
	}	
	
	protected Sessao criaSessaoVirtual(GregorianCalendar dataLiberacaoLista
			                         , List<GregorianCalendar> feriados
			                         , Colegiado colegiado) throws ParseException, ServiceException {		
				
		GregorianCalendar primeiroDiaPeloCPC = this.recuperaProximoDiaRespeitandoPrazoCPC(dataLiberacaoLista,feriados);
		
		Date dataPrevistaInicio = this.dateSessaoInicio(primeiroDiaPeloCPC.getTime());
		Short ano				= this.getAnoSessaoVirtual(dataPrevistaInicio);
		Date dataPrevistaFim    = this.datSessaoFim(dataPrevistaInicio);
		Long numeroUltimaSessao = sessaoService.recuperarMaiorNumeroSessaoVirtual(colegiado, ano);
		Long numeroSessao       = (numeroUltimaSessao == null) ? 1 : numeroUltimaSessao + 1;
		
		Sessao sessao = new Sessao();
		sessao.setDataInicio(null);
		sessao.setDataPrevistaInicio(dataPrevistaInicio);
		sessao.setAno(ano);
		sessao.setDataFim(null);
		sessao.setDataPrevistaFim(dataPrevistaFim);
		sessao.setTipoAmbiente(TipoAmbienteConstante.VIRTUAL.getSigla());
		sessao.setColegiado(colegiado);
		sessao.setTipoSessao(Sessao.TipoSessaoConstante.ORDINARIA.getSigla());		
		sessao.setNumero(numeroSessao);
		sessao.setTipoJulgamentoVirtual(Sessao.TipoJulgamentoVirtual.LISTAS_DE_JULGAMENTO.getId());
		
		return sessao;
	}

	protected TipoColegiadoConstante defineColegiado(String idTipoColegiadoAgendamento) {
		Ministro ministro = getMinistro();
		Integer codigoDaTurmaDoMinistro = objetoIncidenteService.defineCodigoDaTurmaDoMinistro(ministro, null);
		return getTipoColegiadoConstante(idTipoColegiadoAgendamento,codigoDaTurmaDoMinistro);
	}

	protected TipoColegiadoConstante getTipoColegiadoConstante(String idTipoColegiadoAgendamento, Integer codigoDaTurmaDoMinistro) {
		TipoColegiadoConstante colegiado;
		colegiadoMinistro = colegiado = TipoColegiadoConstante.valueOfCodigoCapitulo(codigoDaTurmaDoMinistro);
		if (TipoColegiadoAgendamento.PLENARIO.getId().equalsIgnoreCase(idTipoColegiadoAgendamento)) {
			colegiado = TipoColegiadoConstante.SESSAO_PLENARIA;
		} else if (TipoColegiadoAgendamento.PT.getId().equalsIgnoreCase(idTipoColegiadoAgendamento)) {
			colegiado = TipoColegiadoConstante.PRIMEIRA_TURMA;
		} else {
			colegiado = TipoColegiadoConstante.SEGUNDA_TURMA;
		}
		return colegiado;
	}

	public void validateAndExecute() {
		for (PreListaJulgamento dto : getResources()) {
			validarLiberacaoListaParaJulgamento(dto);
		}
		if (hasMessages()) {
			sendToInformations();
		} else {
			execute();
		}
	}

	void validarLiberacaoListaParaJulgamento(PreListaJulgamento dto) {
		List<String> mensagensProcessosNaoPodemSerAgendados = new ArrayList<String>();
		List<String> mensagensConfirmacao = new ArrayList<String>();
		try {
			dto = preListaJulgamentoService.recuperarPorId(dto.getId());
			List<PreListaJulgamentoObjetoIncidente> listPreListaJulgamentoObjetoIncidente = dto.getObjetosIncidentes();
			
			for (PreListaJulgamentoObjetoIncidente oi : listPreListaJulgamentoObjetoIncidente) {
				if (oi.getRevisado()) {
					try {
						validaLiberacaoProcessoParaJulgamento(oi.getObjetoIncidente());
					} catch (ValidacaoLiberacaoParaJulgamentoException e) {
						String stringFormatada = stringFormatMensagem(oi);
						mensagensProcessosNaoPodemSerAgendados.add(stringFormatada + e.getMessage() + "\n");
					} catch (ProcessoTipoRecursoPodePlanarioVirtualException e) {
						String stringFormatada = stringFormatMensagem(oi);
						mensagensProcessosNaoPodemSerAgendados.add(stringFormatada + e.getMessage() + "\n");						
					} catch (ProcessoPrecisaDeConfirmacaoException e) {
						String stringFormatada = stringFormatMensagem(oi);
						mensagensConfirmacao.add(stringFormatada + e.getMessage() + "\n");
					} catch (ProcessoNaoPodeSerAgendadoException e) {
						String stringFormatada = stringFormatMensagem(oi);
						mensagensProcessosNaoPodemSerAgendados.add(stringFormatada + e.getMessage() + "\n");
					} catch (ServiceException e) {
						String stringFormatada = stringFormatMensagem(oi);
						mensagensProcessosNaoPodemSerAgendados.add(stringFormatada+ "Erro ao validar liberar para julgamento do processo. "+ e.getMessage() + "\n");
					} catch (ParseException e) {
						String stringFormatada = stringFormatMensagem(oi);
						mensagensProcessosNaoPodemSerAgendados.add(stringFormatada + "Erro de ParseException. "+e.getMessage() + "\n");
					}
				}
			}

			if (mensagensProcessosNaoPodemSerAgendados.size() > 0) {
				for (String mensagem : mensagensProcessosNaoPodemSerAgendados) {
					addError(mensagem);
				}
				listasInvalidas.add(dto);
			} else if (mensagensConfirmacao.size() > 0) {
				for (String mensagem : mensagensConfirmacao) {
					addWarning(mensagem);
				}
				listasParaConfirmacao.add(dto);
			}
		} catch (ServiceException e) {
			addError(String.format(MENSAGEM_ERRO_EXECUCAO_ACAO, dto.getNome())+ ": Erro ao validar libera��o de lista para julgamento.");
			listasInvalidas.add(dto);
		}
	}

	String stringFormatMensagem(PreListaJulgamentoObjetoIncidente oi) {		
		ObjetoIncidenteDto objetoIncidenteDto = this.objetoIndicenteDtoValueOf(oi.getObjetoIncidente());
		String indentificao = objetoIncidenteDto.getIdentificacao();
		String mensagemFinal = String.format(MENSAGEM_ERRO_EXECUCAO_ACAO,indentificao)+ ": ";
		return mensagemFinal;
	}

	void validaLiberacaoProcessoParaJulgamento(ObjetoIncidente<?> oi) throws ServiceException
	                                                                               , ValidacaoLiberacaoParaJulgamentoException
	                                                                               , ProcessoPrecisaDeConfirmacaoException
	                                                                               , ProcessoNaoPodeSerAgendadoException
	                                                                               , ProcessoTipoRecursoPodePlanarioVirtualException
	                                                                               , ParseException {
		TipoColegiadoConstante tipoColegiado = defineColegiado(idTipoColegiadoAgendamento);
		ObjetoIncidenteDto processo = objetoIndicenteDtoValueOf(oi);		
		DadosAgendamentoDto dadosAgendamentoDto = montaDadosDoAgendamento(processo);				
		if(this.isAmbienteVirtual()){
			objetoIncidenteService.validarProcessosParaJulgamentoVirtual(oi);
		}

		Integer defineTipoDeAgendamento = objetoIncidenteService.defineTipoDeAgendamento(dadosAgendamentoDto);
		
		objetoIncidenteService.verificaAgendamentoProcesso(oi,tipoColegiado,defineTipoDeAgendamento);
		objetoIncidenteService.verificaProcessoEmListaJulgamentoConjunto(oi);
		objetoIncidenteService.verificaProcessoEmSessaoPrevista(oi);
		objetoIncidenteService.verificaProcessoEmListaJulgamentoPrevista(oi);
		objetoIncidenteService.validarProcessoParaAgendamento(dadosAgendamentoDto);
		objetoIncidenteService.validarSituacaoDeJulgamento(oi);
		
//		if (!objetoIncidenteService.temOABParaTodosOsRepresentantes(oi))
//			throw new ValidacaoLiberacaoParaJulgamentoException(MSG_PROCESSO_COM_REPRESENTANTE_SEM_OAB);
	}
	
	/**
	 * Metodo criado para facilitar o teste Unitario com o Mockito
	 * @param oi
	 * @return
	 */
	public ObjetoIncidenteDto objetoIndicenteDtoValueOf(ObjetoIncidente<?> oi) {
		ObjetoIncidenteDto processo = ObjetoIncidenteDto.valueOf(oi);
		return processo;
	}

	public void execute() {
		if (existeListaSelecionada()) {
			// Limpa as mensagens mostradas anteriormente.
			cleanMessages();
			// Retira as listas inv�lidas e pendentes de confirma��o dos
			// recursos selecionados.
			getResources().removeAll(listasInvalidas);
			// Se os processos pendentes n�o forem confirmados, devem ser
			// retirados
			// do processamento
			if (!getConfirmarListasPendentes()) {
				getResources().removeAll(listasParaConfirmacao);
			}
			execute(new ActionCallback<PreListaJulgamento>() {
				public void doInAction(PreListaJulgamento preLista) throws Exception {
					
					preLista = preListaJulgamentoService.recuperarPorId(preLista.getId());
					
					/*if (!agendamentoService.pesquisar(preLista.getObjetosIncidentes().get(0).getObjetoIncidente()).isEmpty()){
						addInformation("Os processos contidos na lista liberada j� foram liberados para julgamento.");
					}*/
					
					DadosAgendamentoDto dadosAgendamento = montaDadosDoAgendamento(preLista);
					try {
						ListaJulgamento listaJulgamento = objetoIncidenteService.liberarListaParaJulgamento(dadosAgendamento);
						preListaJulgamentoService.removerProcessosRevisados(preLista);
						getAgruparFacesBean().atualizarColuna(preLista);
						
						String nomePreLista        = preLista.getNome();
						String nomeLista           = listaJulgamento.getNome();
						Date   datePrevisaoInicio  = sessaoService.recuperarPorId(idSessao).getDataPrevistaInicio();
						
						
						if(isAmbienteVirtual()){					
							Date datePrevistaFim   = sessaoService.recuperarPorId(idSessao).getDataPrevistaFim();
							String dataPrevisaoIniciao = getFormatterDatePadrao(false).format(datePrevisaoInicio);
							String dataPrevisaoFim     = getFormatterDatePadrao(false).format(datePrevistaFim);						
							addInformation("A lista de processos [" + nomePreLista
								     + "] foi liberada para julgamento virtual no per�odo de " + dataPrevisaoIniciao
								     + " 00:00 a " + dataPrevisaoFim
								     + " 23:59 com o nome de [" + nomeLista + "].");
						}else{
							String dataPrevisaoIniciao = getFormatterDatePadrao(true).format(datePrevisaoInicio);
							addInformation("A lista de processos [" + nomePreLista
								     + "] foi liberada para julgamento em " + dataPrevisaoIniciao
								     + " com o nome de [" + nomeLista + "].");
						}
						existeListaLiberada = true;
					} catch (ProcessoNaoPodeSerAgendadoException e) {
						addError(e.getMessage());
						existeListaNaoLiberada = true;
					} catch (ServiceException e) {
						throw new NestedRuntimeException(e);
					}
				}
			});

			
			finalizar();
		} else {
			getDefinition().setFacet("nenhumProcessoRevisado");
			getDefinition().setHeight(180);
		}
	}

	private boolean existeListaSelecionada() {
		boolean existeRevisado = false;
		
		try {
			PreListaJulgamento preListaJulgamento = preListaJulgamentoService.recuperarPorId(getResources().iterator().next().getId());
			for (PreListaJulgamentoObjetoIncidente relacionamento : preListaJulgamento.getObjetosIncidentes()) {
				if (Boolean.TRUE.equals(relacionamento.getRevisado())) {
					existeRevisado = true;
					break;
				}
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return existeRevisado;
	}

	public void voltar() {
		getDefinition().setFacet("principal");
	}

	public void finalizar() {
		getDefinition().setFacet("final");
	}
	
	public Sessao recuperaSessao() {
		Sessao sessao = null;
		try{
			sessao = sessaoService.recuperarPorId(idSessao);
		} catch (ServiceException e) {
			String msg = MSG_ERRO_CARREGAR_SESSOES +ExportarPreListaJulgamentoActionFacesBean.ESPACO_BRANCO+e.getMessage();
			addError(msg);
			//logger.error(MSG_ERRO_CARREGAR_SESSOES,e);
			//e.printStackTrace();
		}
		return sessao;
	}

	public boolean isAmbienteVirtual() {
		return TipoAmbienteConstante.VIRTUAL.getSigla().equals(this.idTipoAmbienteColegiado);
	}	


	private void setDadosSessoVirtual() throws ParseException {		
		Sessao sessaoVirtual = null;
		try{
			GregorianCalendar dataLiberacaoLista = this.getNow();
			List<GregorianCalendar> feriados     = this.recuperFeriados(dataLiberacaoLista);
			Colegiado colegiado                  = this.getColegiadoAgendamento();
			sessaoVirtual                        = this.recuperaProximaSessaoVirtual(dataLiberacaoLista,feriados,colegiado);
			if (sessaoVirtual == null){
				sessaoVirtual = this.criaSessaoVirtual(dataLiberacaoLista,feriados,colegiado); 
				sessaoService.salvar(sessaoVirtual);
			}
			this.sessoesEmAberto.add(sessaoVirtual);

		} catch (ServiceException e) {
			String msg = MSG_ERRO_SALVAR_SESSAO +ExportarPreListaJulgamentoActionFacesBean.ESPACO_BRANCO+e.getMessage();
			addError(msg);
			//logger.error(MSG_ERRO_SALVAR_SESSAO,e);
			//e.printStackTrace();
		}
		this.setIdSessao(sessaoVirtual.getId());
	}
	
	private Sessao recuperaProximaSessaoVirtual(GregorianCalendar dataLiberacaoLista
			                                  , List<GregorianCalendar> feriados
			                                  , Colegiado colegiado) throws ServiceException, ParseException{		
		List<Sessao> sessoesAbertas = sessaoService.pesquisarSessoesVirtuaisNaoIniciadas(colegiado);
		Sessao proximaSessao = null;
		for (Sessao sessao : sessoesAbertas){
			Integer tipoJulgamentoVirtual = sessao.getTipoJulgamentoVirtual();
			if (Sessao.TipoJulgamentoVirtual.LISTAS_DE_JULGAMENTO.getId().equals(tipoJulgamentoVirtual)){
				if( this.sessaoDentroPrazoCPC(TipoAndamento.PAUTA.getId(),sessao,dataLiberacaoLista, feriados) ) {
					proximaSessao = sessao;
					break;
				}
			}
		}
		return proximaSessao;
	}

	public Colegiado getColegiadoAgendamento() {
		Colegiado colegiado = new Colegiado();		
		TipoColegiadoConstante tipoColegiadoConstante = defineColegiado(this.idTipoColegiadoAgendamento);
		colegiado.setId(tipoColegiadoConstante.getSigla());
		return colegiado;
	}
	
	protected DadosAgendamentoDto montaDadosDoAgendamentoComum() throws ParseException {
		TipoAgendamento tipoAndamento = this.getTipoAgendamentoEscolhido();
		TipoColegiadoAgendamento tipoColegiadoAgendamento = TipoColegiadoAgendamento.getById(getIdTipoColegiadoAgendamento());
		Ministro ministro   = getMinistro();
		Usuario usuario     = getUsuario();
		Setor setorMinistro = getSetorMinistro();
		TipoListaJulgamento tipoLista = getTipoLista();
		if(this.isAmbienteVirtual()){
			this.setDadosSessoVirtual();
		}
		Sessao sessao = recuperaSessao();		
		DadosAgendamentoDto dadosAgendamento = new DadosAgendamentoDto();
		dadosAgendamento.setMinistro(ministro);
		dadosAgendamento.setTipoAgendamento(tipoAndamento);
		dadosAgendamento.setUsuario(usuario);
		dadosAgendamento.setSetorDoUsuario(setorMinistro);
		dadosAgendamento.setTipoColegiadoAgendamento(tipoColegiadoAgendamento);
		dadosAgendamento.setSessao(sessao);		
		dadosAgendamento.setSessoesEmAberto(this.sessoesEmAberto);	
		dadosAgendamento.setTipoListaJulgamento(tipoLista);
		return dadosAgendamento;
	}

	private TipoListaJulgamento getTipoLista(){
		TipoListaJulgamento tipoListaJulgamento = null;
		try {
			tipoListaJulgamento = tipoListaJulgamentoService.recuperarPorId(idTipoListaJulgamento);
		} catch (ServiceException e) {
			addError("Erro: ao recuperar o tipo de lista. "+ExportarPreListaJulgamentoActionFacesBean.ESPACO_BRANCO+e.getMessage());
			logger.error("Erro: ao recuperar o tipo de lista. ", e);
		}catch (Exception e) {
			addError(e.getMessage()); 
			logger.error(e);
		}
		return tipoListaJulgamento;
	}

	protected DadosAgendamentoDto montaDadosDoAgendamento(ObjetoIncidenteDto processo) throws ParseException {
		DadosAgendamentoDto dadosAgendamento = this.montaDadosDoAgendamentoComum();
		dadosAgendamento.setObjetoIncidenteDto(processo);		
		return dadosAgendamento;
	}
	
	protected DadosAgendamentoDto montaDadosDoAgendamento(PreListaJulgamento lista) throws ParseException {
		DadosAgendamentoDto dadosAgendamento = this.montaDadosDoAgendamentoComum();
		dadosAgendamento.setPreListaJulgamento(lista);
		return dadosAgendamento;
	}
	
	TipoAgendamento getTipoAgendamentoEscolhido() {
		TipoAgendamento tipoAgendamento = TipoAgendamento.INDICE;
		if(TipoAndamento.MESA.getId().equals(this.idTipoAndamento)){
			tipoAgendamento = TipoAgendamento.INDICE;
		}else{
			tipoAgendamento = TipoAgendamento.PAUTA;
		}		
		return tipoAgendamento;
	}

	public TipoColegiadoAgendamento[] getTiposColegiadoAgendamento() {
		return TipoColegiadoAgendamento.values();
	}
	
	public TipoAmbienteConstante[] getTiposAmbienteColegiado() {		
		TipoAmbienteConstante[]	 arrayTipoAmbienteColegiado = TipoAmbienteConstante.values();
		return arrayTipoAmbienteColegiado;
	}
	
	public TipoAndamento[] getTiposAndamento() {		
		TipoAndamento[]	 arrayTiposAndamento = TipoAndamento.values();
		return arrayTiposAndamento;
	}

	public Date getDataJulgamento() {
		return dataJulgamento;
	}

	public void setDataJulgamento(Date dataJulgamento) {
		this.dataJulgamento = dataJulgamento;
	}

	public String getIdTipoColegiadoAgendamento() {
		return idTipoColegiadoAgendamento;
	}

	public void setIdTipoColegiadoAgendamento(String idTipoColegiadoAgendamento) {
		this.idTipoColegiadoAgendamento = idTipoColegiadoAgendamento;
	}
	
	public String getIdTipoAmbienteColegiado() {
		return idTipoAmbienteColegiado;
	}

	public void setIdTipoAmbienteColegiado(String idTipoAmbienteColegiado) {
		this.idTipoAmbienteColegiado = idTipoAmbienteColegiado;
	}
	
	public String getIdTipoAndamento() {
		return idTipoAndamento;
	}

	public void setIdTipoAndamento(String idTipoAndamento) {
		this.idTipoAndamento = idTipoAndamento;
	}	

	public ListaJulgamento getListaJulgamento() {
		return listaJulgamento;
	}

	public List<SelectItem> getSessoes() {
		return sessoes;
	}

	public void setSessoes(List<SelectItem> sessoes) {
		this.sessoes = sessoes;
	}

	public Long getIdSessao() {
		return idSessao;
	}

	public void setIdSessao(Long idSessao) {
		this.idSessao = idSessao;
	}

	public Boolean getConfirmarListasPendentes() {
		return confirmarListasPendentes;
	}

	public void setConfirmarListasPendentes(Boolean confirmarListasPendentes) {
		this.confirmarListasPendentes = confirmarListasPendentes;
	}

	public Set<PreListaJulgamento> getListasParaConfirmacao() {
		return listasParaConfirmacao;
	}

	public void setListasParaConfirmacao(
			Set<PreListaJulgamento> listasParaConfirmacao) {
		this.listasParaConfirmacao = listasParaConfirmacao;
	}

	public Set<PreListaJulgamento> getListasInvalidas() {
		return listasInvalidas;
	}

	public void setListasInvalidas(Set<PreListaJulgamento> listasInvalidas) {
		this.listasInvalidas = listasInvalidas;
	}

	public Boolean getExisteListaLiberada() {
		return existeListaLiberada;
	}

	public void setExisteListaLiberada(Boolean existeListaLiberada) {
		this.existeListaLiberada = existeListaLiberada;
	}

	public Boolean getExisteListaNaoLiberada() {
		return existeListaNaoLiberada;
	}

	public void setExisteListaNaoLiberada(Boolean existeListaNaoLiberada) {
		this.existeListaNaoLiberada = existeListaNaoLiberada;
	}

	public TipoColegiadoConstante getColegiadoMinistro() {
		return colegiadoMinistro;
	}

	public void setColegiadoMinistro(TipoColegiadoConstante colegiadoMinistro) {
		this.colegiadoMinistro = colegiadoMinistro;
	}

	public Boolean getSessaoMinistroDiferente() {
		return sessaoMinistroDiferente;
	}

	public void setSessaoMinistroDiferente(Boolean sessaoMinistroDiferente) {
		this.sessaoMinistroDiferente = sessaoMinistroDiferente;
	}
	
	public RevisarListasFacesBean getAgruparFacesBean() {
		RevisarListasFacesBean revisarListasFacesBean = (RevisarListasFacesBean)Component.getInstance(RevisarListasFacesBean.class, true);
		return revisarListasFacesBean;
	}
	
	public boolean isListaProcessosInseridosIndice(){
		if (listaProcessosInseridosIndice != null)
			return listaProcessosInseridosIndice;
		listaProcessosInseridosIndice = false;
		for (PreListaJulgamento lista : getResources()){
			try {
				lista = preListaJulgamentoService.recuperarPorId(lista.getId());
				ObjetoIncidente<?> oi = lista.getObjetosIncidentes().get(0).getObjetoIncidente();
			
				listaProcessosInseridosIndice = !agendamentoService.pesquisar(oi).isEmpty();									
			} catch (ServiceException e) {
				addError("Erro ao verificar processos da lista.");				
			}
		}
		return listaProcessosInseridosIndice;
	}

	public Long getIdTipoListaJulgamento() {
		return idTipoListaJulgamento;
	}

	public void setIdTipoListaJulgamento(Long idTipoListaJulgamento) {
		this.idTipoListaJulgamento = idTipoListaJulgamento;
	}

	public List<SelectItem> getTipoListaJulgamento() {
		return tipoListaJulgamento;
	}

	public void setTipoListaJulgamento(List<SelectItem> tipoListaJulgamento) {
		this.tipoListaJulgamento = tipoListaJulgamento;
	}
}
