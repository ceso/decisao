package br.jus.stf.estf.decisao.mobile.assinatura.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BasicAssinadorRestDto implements Serializable {

	private static final long serialVersionUID = -8854997370515949215L;
	
	private List<String> warnings = new ArrayList<String>();
	private List<String> errors = new ArrayList<String>();
	private boolean erroImpeditivo = false;

	public List<String> getWarnings() {
		return warnings;
	}

	public void setWarnings(List<String> warnings) {
		this.warnings = warnings;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public boolean isErroImpeditivo() {
		return erroImpeditivo;
	}

	public void setErroImpeditivo(boolean erroImpeditivo) {
		this.erroImpeditivo = erroImpeditivo;
	}

}
