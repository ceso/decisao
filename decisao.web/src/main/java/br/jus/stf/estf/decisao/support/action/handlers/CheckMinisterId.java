package br.jus.stf.estf.decisao.support.action.handlers;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.gov.stf.estf.entidade.documento.Texto.TipoRestricao;




/**
 * Valida se o usu�rio logado e o Texto est�o no mesmo c�digo do ministro.
 * 
 * @author Rodrigo Lisboa
 * @see 13.08.2010
 * 
 * @see TipoRestricao
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CheckMinisterId {
	
}
