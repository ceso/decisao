package br.jus.stf.estf.decisao.texto.support;

public class TextoNaoPodeSerRestritoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3713125695861008863L;


	public TextoNaoPodeSerRestritoException(String message) {
		super(message);
	}


}
