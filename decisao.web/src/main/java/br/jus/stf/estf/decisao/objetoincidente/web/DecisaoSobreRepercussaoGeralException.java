package br.jus.stf.estf.decisao.objetoincidente.web;

public class DecisaoSobreRepercussaoGeralException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1406279266864536885L;

	public DecisaoSobreRepercussaoGeralException(String message) {
		super(message);
	}

}
