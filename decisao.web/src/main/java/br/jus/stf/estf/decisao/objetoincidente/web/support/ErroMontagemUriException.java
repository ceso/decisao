package br.jus.stf.estf.decisao.objetoincidente.web.support;

public class ErroMontagemUriException extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = -5546567089966428875L;

	public ErroMontagemUriException(String message, Throwable cause) {
		super(message, cause);
	}

}
