package br.jus.stf.estf.decisao.objetoincidente.support;

import br.gov.stf.estf.publicacao.model.util.IConsultaConteudoPublicacao;

public class ConsultaDeConteudoPublicacaoVO implements IConsultaConteudoPublicacao {
	private Integer codigoCapitulo;
	private Integer codigoMateria;

	public Integer getCodigoCapitulo() {
		return codigoCapitulo;
	}

	public void setCodigoCapitulo(Integer codigoCapitulo) {
		this.codigoCapitulo = codigoCapitulo;
	}

	public Integer getCodigoMateria() {
		return codigoMateria;
	}

	public void setCodigoMateria(Integer codigoMateria) {
		this.codigoMateria = codigoMateria;
	}

}
