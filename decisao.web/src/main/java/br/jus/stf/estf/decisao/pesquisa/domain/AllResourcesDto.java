package br.jus.stf.estf.decisao.pesquisa.domain;

import br.jus.stf.estf.decisao.support.query.Dto;

public class AllResourcesDto implements Dto {

	@Override
	public boolean isSelected() {
		return false;
	}

	@Override
	public void setSelected(boolean selected) {

	}

	@Override
	public Long getId() {
		return null;
	}

	@Override
	public boolean isFake() {
		return false;
	}

}
