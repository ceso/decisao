package br.jus.stf.estf.decisao.objetoincidente.support;

public enum TipoAndamento {

	//br.gov.stf.estf.entidade.processostf.Andamento.Andamentos
	MESA("7600","Apresentado em Mesa"),
	PAUTA("7601","Inclus�o na pauta");

	private String id;
	private String descricao;

	TipoAndamento(String id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public String getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}	

	public static TipoAndamento getById(String id) {
		for (TipoAndamento tipoAandamento : values()) {
			if (tipoAandamento.getId().equals(id)) {
				return tipoAandamento;
			}
		}
		return null;
	}

}
