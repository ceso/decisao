/**
 * 
 */
package br.jus.stf.estf.decisao.comunicacao.support;

import br.jus.stf.assinadorweb.api.requisicao.RequisicaoJnlpAssinador;

/**
 * @author Paulo.Estevao
 * @since 13.05.2011
 */
public class RequisicaoAssinaturaComunicacao extends
		RequisicaoJnlpAssinador<ComunicacaoWrapper> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4470761877044671869L;

}
