package br.jus.stf.estf.decisao.documento.support;

/**
 * Interface de marcação para representar documentos
 * (textos ou comunicações).
 * 
 * @author Tomas.Godoi
 *
 */
public interface Documento {

}
