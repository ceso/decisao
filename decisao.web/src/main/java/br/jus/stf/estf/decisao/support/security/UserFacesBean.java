package br.jus.stf.estf.decisao.support.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.context.FacesContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import org.jboss.seam.web.Session;
import org.springframework.security.context.SecurityContextHolder;

import br.gov.stf.estf.entidade.localizacao.Setor;
import br.gov.stf.estf.entidade.ministro.Ministro;
import br.gov.stf.estf.entidade.usuario.PerfilUsuarioSetor;
import br.gov.stf.estf.entidade.usuario.Usuario;
import br.gov.stf.estf.localizacao.model.service.SetorService;
import br.gov.stf.estf.ministro.model.service.MinistroService;
import br.gov.stf.estf.usuario.model.service.PerfilUsuarioSetorService;
import br.gov.stf.estf.usuario.model.service.UsuarioService;
import br.gov.stf.framework.model.service.ServiceException;
import br.jus.stf.estf.decisao.support.action.support.ActionIdentification;
import br.jus.stf.estf.decisao.support.service.ConfiguracaoSistemaService;

/**
 * Faces Bean utilizado para recupera��o de informa��es e disponibiliza��o
 * de servi�os relacionados a usu�rios.
 * 
 * @author Rodrigo Barreiros
 * @since 31.05.2010
 */
@Name("userFacesBean")
@Scope(ScopeType.SESSION)
public class UserFacesBean {
    
	private static final String PATTERN_USUARIO_COM_NUMERO = "([a-zA-Z.]*)([0-9])";

	@In("#{ministroService}")
	private MinistroService ministroService;
	
	@In("#{setorService}")
	private SetorService setorService;
	
	@In("#{perfilUsuarioSetorService}")
	private PerfilUsuarioSetorService perfilUsuarioSetorService;

	@In("#{usuarioService}")
	private UsuarioService usuarioService;
	
	@In("#{configuracaoSistemaServiceLocal}")
	private ConfiguracaoSistemaService configuracaoSistemaService;
	
	@In
	private FacesContext facesContext;
	
	@In
	private Session session;
	
	@In("#{permissionChecker}")
	private PermissionChecker permissionChecker;
	
	@Logger
	private Log logger;
	
	private boolean isConfiguracaoTextoRestritoLoaded = false;
	
	/**
	 * Retorna o principal autenticado no sistema.
	 * 
	 * @return o principal de autentica��o
	 */
	public Principal getPrincipal() {
		Principal principal = (Principal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (!isConfiguracaoTextoRestritoLoaded) {
			try {
				principal.setSetorRestringeTextoAoResponsavel(configuracaoSistemaService.isTextoRestritoResponsavel());
				isConfiguracaoTextoRestritoLoaded = true;
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return principal; 
	}
	
	/**
	 * Altera o setor do usu�rio dentre a lista de setores poss�veis
	 * 
	 * @throws ServiceException caso ocorra algum problema inesperado
	 */
	public void changeSetor() throws ServiceException {
		if (getPrincipal().getIdSetor() != null) {
			getPrincipal().setGruposEgabDoUsuario(usuarioService.recuperarGrupoUsuario(getPrincipal().getUsuario()));
			getPrincipal().setMinistro(ministroService.recuperarMinistro(setorService.recuperarPorId(getPrincipal().getIdSetor())));
			getPrincipal().setSetorRestringeTextoAoResponsavel(configuracaoSistemaService.isTextoRestritoResponsavel());
		} else {
			getPrincipal().setGruposEgabDoUsuario(null);
			getPrincipal().setMinistro(null);
			getPrincipal().setSetorRestringeTextoAoResponsavel(false);
		}
	}
	
	/**
	 * Recupera todos os setores poss�veis.
	 * 
	 * @return a lista de setores
	 * @throws ServiceException caso ocorra algum problema inesperado
	 */
	public List<Setor> getAllSetores() throws ServiceException {
		return setorService.pesquisarGabinetesComPresidenciaEVice();
	}
	
	/**
     * Pesquisa os usu�rios dado o nome do usu�rio.
     * 
     * 
     * @param suggest o nome do usu�rio
     * 
     * @return a lista de objetos associados ao processo
     */
    public List<Usuario> search(Object suggest) {
    	List<Usuario> sugestoes = null;
		try {
			Set<Usuario> usuarios = new HashSet<Usuario>();
			Long idSetor = getPrincipal().getMinistro() == null ? null : getPrincipal().getMinistro().getSetor().getId();
			String nome = suggest.toString();
			List<Usuario> listaDeUsuarios = idSetor == null ? new ArrayList<Usuario>() : usuarioService.pesquisaUsuario(null, nome, true, idSetor);
			for (Usuario usuario : listaDeUsuarios) {
				//Verifica se o login contem algum numero. Se tiver, n�o pode ser usado
				if (!usuario.getId().matches(PATTERN_USUARIO_COM_NUMERO)){
					usuarios.add(usuario);
				}
			}
			List<PerfilUsuarioSetor> listaPerfilUsuarioSetor = idSetor == null ? new ArrayList<PerfilUsuarioSetor>() : perfilUsuarioSetorService.pesquisarUsuarioDoSetor(nome, idSetor);
			for (PerfilUsuarioSetor perfilUsuarioSetor : listaPerfilUsuarioSetor) {
				usuarios.add(perfilUsuarioSetor.getUsuario());
			}
			
			ArrayList<Usuario> resultado = new ArrayList<Usuario>(usuarios);
			Collections.sort(resultado, new UsuarioComparator());
			return resultado;

		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
		}
    	
    	return sugestoes;
	}
	
    /**
	 * Verifica se o usu�rio corrente possui permiss�o para acessar todos os gabinetes
	 * @return
	 */
	public boolean isAcessaTodosGabinetes() {
		return permissionChecker.hasPermission(getPrincipal(), ActionIdentification.ACESSAR_TODOS_OS_GABINETES);
	}
	
	/**
	 * Verifica se o usu�rio corrente possui permiss�o para criar novo texto
	 * @return
	 */
	public boolean isUsuarioPodeCriarTexto() {
		return permissionChecker.hasPermission(getPrincipal(), ActionIdentification.NOVO_TEXTO);
	}
	
	public boolean isUsuarioPodeAssinar() {
		return permissionChecker.hasPermission(getPrincipal(), ActionIdentification.ASSINAR_DIGITALMENTE);
	}
	
	public boolean isUsuarioPodeFavoritar() {
		return permissionChecker.hasPermission(getPrincipal(), ActionIdentification.DEFINIR_TEXTO_COMO_FAVORITO);
	}
	
	public boolean isUsuarioPodeCategorizarProcesso() {
		return permissionChecker.hasPermission(getPrincipal(), ActionIdentification.CATEGORIZAR_PROCESSOS);
	}
	
	public boolean isUsuarioPodeRevisarListas() {
		return permissionChecker.hasPermission(getPrincipal(), ActionIdentification.LIBERAR_PARA_JULGAMENTO);
	}
	
	public boolean isUsuarioNoGabinete(Long idGabinete) {
		Ministro ministro = getPrincipal().getMinistro();
		if (ministro != null) {
			return ministro.getSetor().getId().equals(idGabinete);
		} else {
			return false;
		}
	}
	
	public boolean isPodeEditarObservacaoProcesso() {
		return permissionChecker.hasPermission(getPrincipal(), ActionIdentification.EDITAR_OBSERVACAO_PROCESSO);
	}
	
	public boolean isEmAlgumGabinete() {
		Principal principal = (Principal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return principal.getMinistro() != null;
	}
	
	/**
	 * Verifica se existe um usu�rio logado.
	 * 
	 * @return true, se logado, false, caso contr�rio
	 */
	public boolean isLogged() {
		return getPrincipal() != null;
	}
	
	public Boolean getTextoRestritoResponsavel() {
		return getPrincipal().isSetorRestringeTextoAoResponsavel();
	}
	
	private class UsuarioComparator implements Comparator<Usuario>{
		@Override
		public int compare(Usuario o1, Usuario o2) {
			if (o1 != null ){
				if (o2 != null){
					return o1.getNome().compareTo(o2.getNome());
				}else{
					return -1;
				}
			}else if (o2 != null){
				return 1;
			}
			return 0;
			
		};

	}
	
}	
